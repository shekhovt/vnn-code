import torch
import vnn
import math
from vnn.modelzoo import *

all_checks = True

print('Zero check')
accuracy_eps = 1e-3
########### Tests ######################

# for net_class in [LeNet, MNIST_MLP, MNIST_MLP_Deep, LeNetPadd, LeNetPaddAvg, CIFAR_S3]:
# for net_class in [MNIST_FC, MNIST_MLP, MNIST_MLP_Deep, LeNet, LeNetPadd, LeNetPaddAvg, CIFAR_S3, LeNet5]:
for net_class in [CIFAR_S3]:
# for net_class in [LeNet, LeNetPadd, LeNetPaddAvg, CIFAR_S3]:
    kwargs = dict(eps=1e-8, input_var = 0, activation= 'LReLU', pooling = 'max')
    net = net_class(**kwargs)
    if __name__ == '__main__':
        print(net.__class__.__name__)
    torch.manual_seed(1)
    net.eval()
    batch_size = 32
    if net_class in [LeNet, MNIST_MLP, MNIST_MLP_Deep, LeNetPadd, LeNetPaddAvg, LeNet5]:
        x = torch.Tensor(batch_size, 1, 28, 28)
    elif net_class in [MNIST_FC]:
        x = torch.Tensor(batch_size, 28 * 28)
    else:
        x = torch.Tensor(batch_size, 3, 32, 32)
    
    if net.a.is_cuda:  # todo: hide this
        x = x.cuda(net.a.get_device())
    
    x.uniform_()

    ## test tiny input noise
    xn = RandomVar(x, x.new_ones(x.size()) * 0.0001)
    net.forward(xn, method='AP2')

    # exit(0)

    o1 = net(x)  # this defaults to method = net.inference
    
    #test inference types:
    o1a = net.forward(x, method='AP1') # use AP1 inference explicitly
    assert (o1.data - o1a.data).norm() < accuracy_eps  # check the outputs are indeed equal
    net.forward(x, method='AP2')
    net.forward(x, method='sample')
    
    if False:
        # test for memory leaks
        y = net.forward(x, method='sample').detach()
        M1 = torch.cuda.memory_allocated()
        for i in range(5):
            y += net.forward(x, method='sample').detach()
        M2 = torch.cuda.memory_allocated()
        assert(M2 == M1)
    
        net.set_normalization('Weight', ssb='SSB1', ScaleNormal= True)  # insert normalization
        net.forward(x, method='AP1').detach()
        net.eval()
        net.train()
        y = net.forward(x, method='sample').detach()
        M1 = torch.cuda.memory_allocated()
        for i in range(5):
            net.forward(x, method='sample').detach()
            print(torch.cuda.memory_allocated())
        M2 = torch.cuda.memory_allocated()
        assert (M2 == M1)
        
        continue
    
    O1 = [0]*len(net)
    L1 = [0]*len(net)
    B1 = [0]*len(net)
    y = x
    for i,l in enumerate(net):
        if isinstance(l, vnn.Conv2d):
            L1[i] = l.weight.data.clone()
            B1[i] = l.bias.data.clone()
        y = l(y, method=net.inference)
        O1[i] = y.data.clone()
    
    ## Weight Norm
    
    net.set_normalization('Weight') # insert normalization
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps
    net.remove_normalization()
    y = x
    for i,l in enumerate(net):
        if isinstance(l, vnn.Conv2d):
            assert (L1[i] - l.weight.data).norm() < accuracy_eps
            assert (B1[i] - l.bias.data).norm() < accuracy_eps
        y = l(y, method=net.inference)
        assert (O1[i] - y.data).norm() < accuracy_eps
    
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps
    
    ## AP2 Norm
    net.set_normalization('AP2', eps=1e-16) # insert normalization
    net.init_normalization_AP2(x.size(), preserve_equivalence=True)
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps

    # Test normalization invariances
    y = x
    T1 = [0] * len(net)
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            T1[i] = y
            l.linear.weight.data *= 2
            if l.linear.bias is not None:
                l.linear.bias.data.normal_()
    #
    #
    net.normalization_valid = False
    net.forward(x) # recomputes normalization
    #
    y = x
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            assert (T1[i].data - y.data).norm() < accuracy_eps
    #
    net.normalization_valid = False
    for i, l in enumerate(net):
        if isinstance(l, vnn.LinearNorm):
            l.project = True
    net.forward(x)  # recomputes normalization
    y = x
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            assert (T1[i].data - y.data).norm() < accuracy_eps
    
    # check removing norm preserves equivalence
    #
    net.remove_normalization()
    y = x
    for i,l in enumerate(net):
        if isinstance(l, vnn.Conv2d):
            assert (L1[i] - l.weight.data).norm() < accuracy_eps
            assert (B1[i] - l.bias.data).norm() < accuracy_eps
        y = l(y, method=net.inference)
        assert (O1[i] - y.data).norm() < accuracy_eps
    
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps
    
    ## BN
    net.set_normalization('BN') # insert normalization
    #print(net) ##
    net.init_normalization_BN(x, preserve_equivalence=True)
    net.eval()
    y = x
    for i,l in enumerate(net):
        y = l(y, method=net.inference)
        if (O1[i] - y.data).norm() > accuracy_eps:
            print('layer:{}, norm:{}'.format(i, (O1[i] - y.data).norm()))
    
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps
    net.remove_normalization()
    
    y = x
    for i,l in enumerate(net):
        if isinstance(l, vnn.Conv2d):
            assert (L1[i] - l.weight.data).norm() < 1e-3
            assert (B1[i] - l.bias.data).norm() < 1e-3
        y = l(y, method=net.inference)
        assert (O1[i] - y.data).norm() < 1e-3
    
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < 1e-3

    # Test normalization invariances
    y = x
    T1 = [0] * len(net)
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            T1[i] = y
            l.linear.weight.data *= 2
            if l.linear.bias is not None:
                l.linear.bias.data.normal_()
    #
    y = x
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            assert (T1[i].data - y.data).norm() < accuracy_eps
    #
    for i, l in enumerate(net):
        if isinstance(l, vnn.LinearNorm):
            l.project = True
    y = x
    for i, l in enumerate(net):
        y = l(y, method=net.inference)
        if isinstance(l, vnn.LinearNorm):
            assert (T1[i].data - y.data).norm() < accuracy_eps

    # Weight Norm again
    net.set_normalization('Weight')  # insert normalization
    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps
    net.remove_normalization()
    y = x
    for i, l in enumerate(net):
        if isinstance(l, vnn.Conv2d):
            assert (L1[i] - l.weight.data).norm() < accuracy_eps
            assert (B1[i] - l.bias.data).norm() < accuracy_eps
        y = l(y, method=net.inference)
        assert (O1[i] - y.data).norm() < accuracy_eps

    o1b = net.forward(x)
    assert (o1.data - o1b.data).norm() < accuracy_eps

    # CPU / GPU test
    net = net.cuda()
    net.set_normalization('AP2')  # insert normalization
    net.init_normalization_AP2(x.size(), preserve_equivalence=False)
    net = net.cuda()
    x = x.cuda()
    net.forward_AP2(x)

    net.eval()
    net = net.cpu()
    x = x.cpu()
    net.forward_AP2(x)

    net.train()
    net.forward_AP2(x)

    # save /load test
    torch.save(net, '/tmp/vnn_save_test')
    net = torch.load('/tmp/vnn_save_test')
    
    net.cuda()
    x = x.cuda()
    net.forward_AP2(x)
    net.forward_AP1(x)
    
print('Passed')

a = Parameter(Tensor(1))
b = Parameter(Tensor(2))
c = a + b
g = Tensor().double().cuda()
d = c.type_as(g)
assert d.requires_grad
assert not d.is_leaf

