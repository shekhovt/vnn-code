import torch
import torch.nn as nn

import numpy as np

import torch.nn.functional as F
from torch.nn import Module
from torch.nn.parameter import Parameter

from .random_variable import *

from .layers import *
# from . import compat

import operator
from functools import reduce

import math
from torch import Tensor
from .utils import *
import random

class ModuleList(Module):
    """Redefining pytorch's ModuleList to keep a list of modules -- supports deletion and insertion"""
    
    def __init__(self, modules=None):
        Module.__init__(self)
        self._list = []
        self.counter = 0
        self.register_buffer('a', Tensor(1))
        if modules is not None:
            self += modules
    
    def __getitem__(self, idx):
        if isinstance(idx, str):
            idx = int(idx)
        if idx < 0:
            idx = len(self) + idx
#        if isinstance(idx, str):
#            return Module.__getitem__(self, idx)
        return self._list[idx]
    
    def key_(self, module, idx):
        #return str(id(module))
        if idx <0:
            idx = idx % len(self)
        return str(idx)
    
    def __setitem__(self, idx, module):
        try:
            if isinstance(idx, str):
                idx = int(idx)
            if idx <0:
                idx = len(self)+idx
            assert(idx < len(self))
            assert isinstance(module, nn.Module)
            if self.a.is_cuda:
                module.cuda(self.a.get_device())
            if self[idx] is not None:  # can be during insert / append
                delattr(self, self.key_(self[idx], idx))
            self._list[idx] = module
            return setattr(self, self.key_(module, idx), module)
        except ValueError:
            return self.__setattr__(idx, module)
        
    def __delitem__(self, idx):
        delattr(self, self.key_(self[idx],idx))
        del self._list[idx]
        # update names
        for i in range(idx):
            setattr(self, self.key_(None, i), self._list[i])
    
    def insert(self, index, obj):
        self._list.insert(index, None)
        self[index] = obj
        # update names
        for i in range(index, len(self)):
            setattr(self, self.key_(None, i), self._list[i])
    
    def __len__(self):
        return len(self._list)
    
    def __iter__(self):
        return iter(self._list)
    
    def __iadd__(self, modules):
        return self.extend(modules)
    
    def append(self, module):
        self._list.append(None)
        self[-1] = module
        return self
       
    def extend(self, arg):
        if isinstance(arg, nn.Module):
            self.append(arg)
        elif isinstance(arg, list):
            for m in arg:
                self.append(m)
        return self
    
    def out_size(self, size):
        for (i, layer) in enumerate(self):
            size = layer.out_size(size)
        return size
    
    def can_reduce_input(self):
        return False
    
    def forward(self, x, **kwargs):  # overriding Module.forward
        # preprocess input
        # self.o = list()
        if isinstance(x,Tensor):
            if not x.is_cuda and self.a.is_cuda:
                x = x.cuda(self.a.get_device())
            x = to_variable(x)
        # forward through all layers
        for (i, layer) in enumerate(self):
            in_size = x.size()
            out_sz = layer.out_size(list(x.size()))
            if (kwargs.get('compute_normalization') or kwargs.get('init_normalization')) and layer.can_reduce_input():
                x1 = contract_spatial(x)
                x = layer.forward(x1, **kwargs, in_size=in_size)
                x = x.expand(out_sz)
            else:
                x = layer.forward(x, **kwargs)
                #
                # g = torch.autograd.grad(x[0,0,100,100], self[0].norm.sb.S, retain_graph=True)[0]
                # print('grad=[{:3g}, {:3g}]'.format(g.min().item(), g.max().item()))
                #
            if all_checks:
                if isinstance(x, RandomVar):
                    check_var(x.var)
                    if not (x.mean.abs() < float('inf')).all():
                        raise NumericalProblem('mean is inf')
                    if not (x.var.abs() < float('inf')).all():
                        raise NumericalProblem('var is inf')
                else:
                    if not (x.abs() < float('inf')).all():
                        raise NumericalProblem('x is inf')
                assert list(x.size()) == out_sz
            # self.o.append(x)
        return x
    
    def forward_KL_pq(self, x, method, **kwargs):  # overriding Module.forward
        # preprocess input
        assert(method=='AP2')
        # self.o = list()
        if isinstance(x,Tensor):
            if not x.is_cuda and self.a.is_cuda:
                x = x.cuda(self.a.get_device())
            x = to_variable(x)
        # forward through all layers with AP2 and sample, control samples are likely after linear
        # xs = x.clone()
        t = random.randint(0, x.size()[0]-1)
        xs = x[t].view([1] + list(x.size())[1:]).expand(x.size()) # use the same input for all samples - variance reduction
        for (i, layer) in enumerate(self):
            x = layer.forward(x, **kwargs, method ='AP2')
            xs = layer.forward(xs, **kwargs, method = 'sample')
            if isinstance(layer, LinearModule) or isinstance(layer, LinearNorm):
                XS = RandomVar()
                XS.mean = xs.mean(dim=0, keepdim=False)
                XS.var = xs.var(dim=0, keepdim=False)
                #KL = x.KL_from_delta(xs)
                KL = XS.KL(x[t])
                layer.reg_loss = KL.mean()  # average over units and spatial dimension of a layer
                #
                # layer.reg_loss = KL.mean(dim=0).sum() # average over samples, sum over units
                # layer.reg_loss = KL.mean() # average over samples, units and spatial dimension of a layer
        return x
    
    def project(self):
        for m in self.modules():
            # if isinstance(m, LinearModule):
            if m is not self and hasattr(m, 'project'):
                m.project()
    
    # def forward(self, *args, **kwargs):  # overriding Module.forward
    #     xx = list(args)
    #     for i,x in enumerate(xx):
    #         if isinstance(x,Tensor) and not x.is_cuda and self.a.is_cuda:
    #             xx[i] = x.cuda(self.a.get_device())
    #     x = None
    #     for (i, layer) in enumerate(self):
    #         # hooks not supported
    #         x = layer.forward(*xx, **kwargs)
    #         if isinstance(x, list):
    #             xx = x
    #         else:
    #             xx = [x]
    #     return x
    
    # def forward_sample(self, *args: [Tensor, RandomVar], **kwargs) -> Tensor:  # virtual method to be overloaded by inheriting functions
    #     return self.forward(*args, method='sample', **kwargs)

    # def __repr__(self):
    #     tmpstr = self.__class__.__name__ + ' (\n'
    #     for key, module in enumerate(self):
    #         modstr = module.__repr__()
    #         tmpstr = tmpstr + '  (' + str(key) + '): ' + modstr + '\n'
    #     tmpstr = tmpstr + ')'
    #     return tmpstr

    # def __repr__(self):
    #     tmpstr = self.__class__.__name__ + ' (\n'
    #     for key, module in enumerate(self):
    #         modstr = module.__repr__()
    #         tmpstr = tmpstr + '  (' + str(key) + '): ' + modstr + '\n'
    #     tmpstr = tmpstr + ')'
    #     return tmpstr

    # def named_children(self):
    #     memo = set()
    #     for idx, module in enumerate(self):
    #         if module is not None and module not in memo:
    #             memo.add(module)
    #             yield self.key_(module,idx), module


def insert_normalization(m, norm_class, parent, name, **kwargs):
    if isinstance(m, LinearModule):
        assert not isinstance(parent, LinearNorm), 'Normalization already present'
        norm = norm_class(m.get_out_channels(), linear=m, **kwargs)
        lnorm = LinearNorm(m, norm_layer=norm, **kwargs)
        # lnorm.train(m.training)
        parent[name] = lnorm

def remove_normalization(m, parent, name):
    if isinstance(m, LinearNorm):
        m.remove_norm()
        parent[name] = m.linear

#______________________________________Model_________________________________________________________________
class Model(ModuleList):
    """
    Top level Models should derive from this class
    """
    
    def avg_loss(self, target: Tensor, output: Tensor) -> Tensor:
        """ loss function of the model, defaults to cross-entrpoy for classification"""
        """ target must be in the same domain as output (e.g. log prob distribution for classification)"""
        return n_avg_cross_entropy_log(target, output)
        # return nn.NLLLoss()(output, target)
    
    def map_target(self, t: Tensor) -> Tensor:
        """ how to convert dataset target to model output """
        t = to_tensor(t)
        #return to_variable(t).detach()
        return to_variable(one_hot_log(t, self.get_out_channels())).detach()

    # def optimizer(self) -> torch.optim.Optimizer:
    #     """ selected Optimizer """
    #     return ncross_entropy_log(target, output)
    

    def __init__(self, inference='AP1', train_inference=None, reg=None, data_mean=0.0, data_var=1.0, cuda=torch.cuda.is_available(), **kwargs):
        """
        :param data_mean: mean training data statistics can be a number, a tensor [1, C, 1, 1] or a tensor [1, C, W, H], will be saved with the model
        :param data_var: same as above
        """
        ModuleList.__init__(self)
        self.reg = reg
        self.normalization = None
        self.normalization_valid = False
        self.inference = inference
        if train_inference is None:
            train_inference = inference
        self.train_inference = train_inference
        self.norm_params = {'affine': True, 'eps': 1e-05, 'momentum': 0.1}
        self.args = odict(inference=inference, train_inference=train_inference, reg=reg, cuda=cuda, **kwargs)
        #
        self.init_dataset_statistics(data_mean, data_var)
        if cuda:
            self.cuda()
            
    def upgrade(self, **kwargs):
        self.normalization_valid = False
        for (i, layer) in enumerate(self):
            if hasattr(layer,'upgrade'):
                layer.upgrade(**kwargs)
        
    def reset_parameters(self, weight_init='uniform'):
        for m in self.modules():
            if isinstance(m, Linear) or isinstance(m, Conv2d) or isinstance(m, LinearModule):
                if weight_init == 'uniform':
                    continue
                elif weight_init == 'orthogonal':
                    torch.nn.init.orthogonal(m.weight.data)
                elif weight_init == 'xavier':
                    torch.nn.init.xavier_uniform(m.weight.data)
                else:
                    raise ValueError()
    
    def init_dataset_statistics(self, data_mean, data_var):
        if isinstance(data_mean, numbers.Number):
            data_mean = to_tensor(data_mean).view([1, 1, 1, 1])
        if isinstance(data_var, numbers.Number):
            data_var = to_tensor(data_var).view([1, 1, 1, 1])
        # remove spatial dimensions
        if data_mean.dim()==4:
            data_mean = data_mean.mean(dim=2, keepdim=True).mean(dim=3, keepdim=True)
        if data_var.dim()==4:
            data_var = data_var.mean(dim=2, keepdim=True).mean(dim=3, keepdim=True)
        data_mean = data_mean.contiguous()
        data_var = data_var.contiguous()
        # make data_var same shape as data_mean
        # hope they are the same dimensions
        sz1 = list(data_mean.size())
        sz2 = list(data_var.size())
        assert len(sz1) == len(sz2)
        sz = [max(sz1[d], sz2[d]) for d in range(len(sz1))]
        data_mean = data_mean.expand(sz).contiguous()
        data_var = data_var.expand(sz).contiguous()
        #
        assert data_var.size() == data_mean.size()
        self.register_buffer('train_stat', RandomVar(data_mean, data_var))

    
    def forward(self, *args, **kwargs):

        if 'method' not in kwargs or kwargs['method'] is None:
            kwargs['method'] = self.inference
        #  hooks are not supported

        # apply defaults if not provided
        if kwargs.get('method') == 'MC':
            return self.forward_MC(*args, **kwargs)
        
        # normalization
        if self.normalization == 'AP2' and not self.normalization_valid and not 'init_normalization' in kwargs and not 'compute_normalization' in kwargs:
            self.normalization_valid = True
            self._compute_normalization_AP2(args[0].size(), **kwargs)
        # BN and WeightNorm are in-place
        
        if kwargs.get('method') == 'AP2' and kwargs.get('reg') == 'KL_pq' and not 'init_normalization' in kwargs and not 'compute_normalization' in kwargs:
            y = ModuleList.forward_KL_pq(self, *args, **kwargs)
        else:
            y = ModuleList.forward(self, *args, **kwargs)
        
        if y.dim() == 4 and y.size()[2] == 1 and y.size()[3] == 1:
            y = y.view([y.size()[0],y.size()[1]])
        return y

    def forward_MC(self, x, MC_samples=10, method=None, **kwargs):
        self.normalization_valid = False # triger forward to update normalization and sample parameters
        yy = self.forward(x, method='sample', **kwargs).detach()
        for s in range(MC_samples-1):  # draw several samples
            self.normalization_valid = False
            yy = log_sum_exp2(yy.detach(), self.forward(x, method='sample', **kwargs).detach())
            # yy = yy.detach() + self.forward(x, method='sample', **kwargs).detach()
            # print(torch.cuda.memory_allocated())
        lp = yy - math.log(MC_samples) # logarithm of average probability
        # lp = F.log_softmax(lp, dim=1) # renormalize
        p = lp.exp()
        assert ((p.sum(dim=1) -1).abs() < 0.001).all()
        del p
        return lp.data
    
    # def __call__(self, *args, **kwargs):
    #     # apply defaults if not provided
    #     if 'method' not in kwargs:
    #         kwargs['method'] = self.inference
    #     #  hooks are not supported
    #     return self.forward(*args, **kwargs)
    #     #return nn.Module.__call__(self, *args, **kwargs)
    
    # def reset_parameters(self):
    #     # if self.init_normalized:
    #     if self.init == 'AP2':
    #         self.compute_normalization_AP2()
    #     else:
    #         assert self.init is None
    
    # def __project_weights_module(self, m):
    #     # to#do: only correct if Normalization follows each linear layer, cannot place it in front, etc. Incorporate norm into linear layers
    #     wnorm = None
    #     if isinstance(m, Conv2d):
    #         assert (m.weight.dim() == 4)
    #         wnorm = torch.sqrt(square(m.weight).sum(dim=1, keepdim=True).sum(dim=2, keepdim=True).sum(dim=3, keepdim=True))
    #     if isinstance(m, Linear):
    #         assert (m.weight.dim() == 2)
    #         wnorm = torch.sqrt(square(m.weight).sum(dim=1, keepdim=True))
    #     if wnorm is not None:
    #         m.weight.data = (m.weight / (wnorm + 1e-6)).data
    #         assert m.weight.is_leaf, "checking the projection is not in differentiation graph"
    
    # def __project_weights(self):
    #     self.apply(self.__project_weights_module)
    
    def remove_normalization(self):
        if self.normalization is None:
            return
        self.apply(remove_normalization)
        self.normalization = None
        # for i in reversed(range(len(self))):
        #     layer = self[i]
        #     if isinstance(layer, BatchNorm) or isinstance(layer, VNorm):
        #         assert i > 0
        #         t = layer.total_transform()
        #         self[i - 1].merge_after(t)
        #         del self[i]

    def set_normalization(self, normalization, project=False, **kwargs):
        if normalization == self.normalization:
            return
        self.remove_normalization()
        if normalization is None:
            return
        elif normalization == 'AP2':
            norm_class = VNorm
        elif normalization == 'BN':
            norm_class = BatchNorm
        elif normalization == 'Weight':
            norm_class = WeightNorm
        elif normalization == 'Layer':
            norm_class = LayerNorm
        else:
            raise ValueError('Cannot recognize normalization:{}'.format(normalization))
        args = dict(self.norm_params, project=project, **kwargs)
        self.apply(insert_normalization, norm_class, **args)
        if self.args.cuda:
            self.cuda()
        #
        # for i in reversed(range(len(self))):
        #     layer = self[i]
        #     if isinstance(layer, Linear) or isinstance(layer, Conv2d):
        #         if normalization == 'AP2':
        #             self.insert(i+1, VNorm(layer.weight.size()[0], **self.norm_params))
        #         elif normalization == 'BN':
        #             self.insert(i+1, BatchNorm(layer.weight.size()[0], **self.norm_params))
        #         else:
        #             raise ValueError()
        self.normalization = normalization

    def init_normalization_AP2(self, input_size, preserve_equivalence=False, **kwargs):
        input_size = list(input_size)
        input_size[0] = 1
        x = align_dim(self.train_stat.type_as(self.a), len(input_size)).expand(input_size)
        self.forward_AP2(x, init_normalization=True, preserve_equivalence=preserve_equivalence, **kwargs)
        self.normalization_valid = True

    def init_normalization_BN(self, x: Tensor, preserve_equivalence=False, **kwargs):
        x = to_variable(x.type_as(self.a))
        self.forward(x, method='AP1', init_normalization=True, preserve_equivalence=preserve_equivalence, **kwargs)

    def _compute_normalization_AP2(self, input_size, **kwargs):
        #self.__project_weights()  # before computing normalization, take out redundant degree of freedom, just for stability
        input_size = list(input_size)
        if kwargs.get('sample_params') and self.training and False: # todo unhack
            pass
        else:
            input_size[0] = 1
        #input_size[0] = 1
        x = align_dim(self.train_stat.type_as(self.a), len(input_size)).expand(input_size)
        self.forward(x, **dict(kwargs, compute_normalization=True, method='AP2'))
        self.normalization_valid = True
        
    def approx_params(self):
        for m in self.modules():
            if m is self:
                continue
            if hasattr(m, 'approx_params'):
                for k, p in m.approx_params.items():
                     yield p

    def train(self, mode=True):
        nn.Module.train(self, mode)
        if self.normalization == 'AP2':
            self.normalization_valid = False
            
    # def _compute_normalization(self, **kwargs):
    #     if self.normalization is None:
    #         return
    #     elif self.normalization == 'BN':
    #         return
    #     elif self.normalization == 'AP2':
    #         self._compute_normalization_AP2(**kwargs)
    #     else:
    #         raise ValueError()
    
    def layers_reg(self) -> Tensor:
        treg = 0
        for m in self.modules():
            reg_loss = getattr(m,'reg_loss', None)
            if reg_loss is not None:
                treg = reg_loss + treg
        return treg
    
    def train_forward(self, x, target, **kwargs):
        """
            this shoud for a given model structure compute the gradient for chosen loss and regularization
        """
        # apply defaults if not provided
        if 'train_inference' not in kwargs:
            kwargs['train_inference'] = self.train_inference
        if 'reg' not in kwargs:
            kwargs['reg'] = self.reg
        #
        inference = kwargs['train_inference']
        reg = kwargs['reg']
        # convert to model type-storage, map target to model output space
        x = to_variable(x.type_as(self.a))
        target = self.map_target(target)
        #
        # # normalization
        # if self.normalization == 'AP2':
        #     self._compute_normalization_AP2(x.size(), **kwargs)
        # # BN and WeightNorm are in-place
        # forward
        #
        y = self.forward(x, method=inference, **kwargs)
        self.normalization_valid = False
        #
        y = y.view(target.size())
        if kwargs.get('select_batch') is not None: # test BN stochasticity, selecting a subset of outputs for the loss
            b = kwargs['select_batch'] * kwargs['batch_size']
            e = (kwargs['select_batch'] + 1) * kwargs['batch_size']
            l0 = self.avg_loss(target[b:e], y[b:e])
        else:
            l0 = self.avg_loss(target, y)
        #
        # regularization
        ll = [l0]
        if reg == 'KL' or reg == 'KL_pq':
            l_reg = self.layers_reg()
            if isinstance(l_reg, Tensor):
                ll.append(l_reg)
            
        elif reg == 'sample': #
            y1 = x.clone()
            for i in range(len(self) - 1):
                l = self[i]
                y1 = l.forward(y1, method='sample', **kwargs)  # sample through all but last
                # y1 = upgrade_to_RV(y1)
                # if y1.var is None:
                #     y1.var = Tensor(y1.mean.data.new(y1.size()))
                #     y1.var.data.zero_()
                # y1 = l.forward(y1, method='AP2', **kwargs)  # AP2 through Sigmoid and Linear
                # assert (y1.mean.data == y1.mean.data).all()
                # assert (y1.var.data >=0).all()
                # if isinstance(l, Conv2d) or isinstance(l, Linear) or isinstance(l, LinearNorm):
                #     y1 = y1.sample()  # After Conv2d sample from normal distribution
                #     assert (y1.data == y1.data).all()
            assert isinstance(y1, Tensor)
            y1 = self[-1].forward(y1, method='AP1', **kwargs)  # propagate last layer
            # squash
            #y1 = y1.detach()
            while y1.dim() > 2 and y1.size()[-1] == 1:
                y1 = y1.view(y1.size()[0:-1])
            # stabilize
            # m, _ = y1.max(dim=1, keepdim=True)
            # m = m.detach()
            m = y1.max()
            #print(m.exp().item())
            l1 = self.avg_loss(y1-m, y)*( m.exp() )
            ll.append(l1)
            
        if reg == 'indep': #
            y1 = x.clone()
            for i in range(len(self) - 1):
                l = self[i]
                y1 = upgrade_to_RV(y1)
                if y1.var is None:
                    y1.var = y1.mean.new(y1.size())
                    y1.var.data.zero_()
                y1 = l.forward(y1, method='AP2', **kwargs)  # AP2 through all layers
                assert (y1.mean.data == y1.mean.data).all()
                assert (y1.var.data >=0).all()
                if isinstance(l, Conv2d) or isinstance(l, Linear) or isinstance(l, LinearNorm):
                    y1 = y1.sample()  # After Linear/Conv2d sample from normal distribution
                    assert (y1.data == y1.data).all()
            assert isinstance(y1, Tensor)
            y1 = self[-1].forward(y1, method='AP1', **kwargs)  # propagate last layer
            # squash
            #y1 = y1.detach()
            while y1.dim() > 2 and y1.size()[-1] == 1:
                y1 = y1.view(y1.size()[0:-1])
            # stabilize
            # m, _ = y1.max(dim=1, keepdim=True)
            # m = m.detach()
            m = y1.max()
            #print(m.exp().item())
            l1 = self.avg_loss(y1-m, y)*( m.exp() )
            ll.append(l1)
        return ll

    def get_out_channels(self) -> int or None:
        """how many output channels are there on the output"""
        for m in reversed(self):
            c = m.get_out_channels()
            if c is not None:
                return c
        return None
    
    def __repr__(self):
        np = sum([p.numel() for p in self.parameters()])
        tmpstr = self.__class__.__name__ + ' params: {} '.format(np)
        tmpstr += super().__repr__()
        return tmpstr

def test1():
    ll = ModuleList([nn.Linear(i + 1, i + 1) for i in range(3)])
    ll[1] = nn.Linear(2, 2)
    print(ll)
    del ll[0]
    print(ll)
    ll.insert(2, nn.Linear(5, 5))
    print(ll)
    del ll[0]
    print(ll)
    assert len(ll) == len(ll._modules)
