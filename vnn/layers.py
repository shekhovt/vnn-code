import torch
import torch.nn as nn

import numpy as np
import copy
import torch.nn.functional as F
from torch.nn.parameter import Parameter

from .random_variable import *
from .functions import *
from .module import Module
from .utils import *

from torch import Tensor
import warnings
import operator
from functools import reduce

import math
from collections import OrderedDict
import warnings
from abc import ABCMeta, abstractmethod, abstractproperty


def align_dim(x:Tensor, dim):
    # align dimensions to input for broadcasting
    if dim == 4:
        x = x.view(list(x.size()) + [1]*(dim - x.dim()) )
    elif dim == 2:
        x = x.view([x.size()[0], x.size()[1]])
    else:
        raise ValueError("don\'t know how to trat such input dimension")
    return x

class Sigmoid(Module):
    """
    The logistic-Bernoulli or logistic transform, depending on control parameter bernoulli
    """
    
    def __init__(self, bernoulli: bool = True):
        """
        :param bernoulli: True -> logistic Bernoulli, False -> logistic Transform
        """
        Module.__init__(self)
        self.bernoulli = bernoulli
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return x.sigmoid()  #
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        # assert (x.var == x.var).all(), 'NaN in x.var'
        # mean approx
        s = torch.sqrt(x.var + V_S)
        a = x.mean / s
        y = RandomVar()
        y.mean = Phi_approx(a)
        # variance approx
        if self.bernoulli:
            y.var = y.mean * (1 - y.mean)
        else:
            var_at_zero = torch.reciprocal(4 * torch.reciprocal(x.var + MIN_VAR) + 1) * 4
            y.var = var_at_zero * square(y.mean * (1 - y.mean))
        return y
    
    def forward_sample(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
        """
        do forward estimation, then sample from Bernoulli
        """
        if self.training:
            warnings.warn("Warning: Performing sample in training mode, but differentiation is not possible")
        if not self.bernoulli:  # determenistic function, default behaviour
            return self.forward_AP1(x, **kwargs)
        else:
            if isinstance(x, RandomVar):
                p = self.forward_AP2(x, **kwargs).mean
            else:
                p = self.forward_AP1(x, **kwargs)
            # sample from bernoulli distributino with p
            u = p.new(p.size())
            u.uniform_()
            return (u <= p).detach().float()


class Heaviside(Module):
    """
    Heaviside step function
    """
    
    def __init__(self):
        Module.__init__(self)
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return (x.sign() + 1) / 2
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        s = torch.sqrt(x.var + MIN_VAR)
        a = x.mean / s
        y = RandomVar()
        y.mean = Phi_approx(a)
        y.var = y.mean * (1 - y.mean)
        return y


class ReLU(Module):
    """
        Y = max(X,0)
    """
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return torch.nn.functional.relu(x)
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        var = x.var + MIN_VAR
        y = RandomVar()
        # logistic approximation for the mean (cheaper? but bad numerics)
        # y.mean = s * log1p_exp(a)
        # s = torch.sqrt(var / V_S)
        # a = x.mean / s
        
        # normal approximation for mean and variance
        s = torch.sqrt(var)
        a = x.mean / s
        y.mean = x.mean * Phi_approx(a) + s * phi(a)
        y.var = var * R_approximation(a)
        return y


class LeakyReLU(ReLU):
    r"""Applies element-wise,
        :math:`f(x) = \max(\text{negative\_slope}*x, x)`
    """
    
    def __init__(self, negative_slope=0.01):
        ReLU.__init__(self)
        assert (0 <= negative_slope < 1), 'negative_slope must be in range [0,1)'
        self.alpha = negative_slope
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        # print(x.mean().item(), x.var().item())
        return torch.nn.functional.leaky_relu(x, self.alpha)
    
    def forward_AP2(self, x: RandomVar, **kwargs):
        y = RandomVar()
        var = x.var + MIN_VAR
        s = torch.sqrt(var)
        a = x.mean / s
        y.mean = x.mean * (self.alpha + (1 - self.alpha) * Phi_approx(a)) + s * (1 - self.alpha) * phi(a)
        y.var = x.var * (square(self.alpha) + (1 - square(self.alpha)) * R_approximation(a))
        # must be guaranteed positive
        return y


class Max(Module):
    """
        Y = max(X1, X2)
    """
    def __init__(self):
        Module.__init__(self)
        self.t = [0.0, 0.4281]  # KL < 0.0325
    
    def forward_AP1(self, x1: Tensor, x2: Tensor, **kwargs) -> Tensor:
        return torch.max(x1, x2)
    
    def forward_AP2(self, x1: RandomVar, x2: RandomVar, **kwargs) -> RandomVar:
        v = x1.var + x2.var + MIN_VAR
        s = torch.sqrt(v)
        a = (x1.mean - x2.mean) / s
        y = RandomVar()
        P = Phi_approx(a)
        y.mean = x1.mean * P + x2.mean * (1 - P) + s * phi(a)
        r = (a - self.t[0]) / self.t[1]
        y.var = x1.var * logistic_cdf(r) + x2.var * logistic_cdf(-r)
        return y


class LogArgmax(Module):
    """
    log of expected argmax along dimension dim
    x: input activations (4D RandomVar)
    :return: log-probability of classes (4D Tensor)
    """

    def __init__(self, simplified: bool = False, dim=1, **kwargs):
        Module.__init__(self)
        self.simplified = simplified
        self.dim = dim
        self.latent_noise_var = MIN_VAR
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        # recall this is log argmax
        y = torch.ones_like(x)*(-30)
        idx = x.max(dim=self.dim, keepdim=True)[1]
        # y.scatter_(1, idx, Tensor([1]).type_as(x).expand(y.size()))
        y.scatter_(1, idx, 0)
        return y
    
    def forward_AP2(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
        if not hasattr(self, 'dim'):
            self.dim = 1
        n = float(x.size()[self.dim])
        if self.simplified:  # simple approx, linear complexity
            if True: # new approx
                if True: # stable implementation
                    # v = x.var / square(0.39) # <- this is a hacky way to do t-scaling calibration knowing the scale from the test
                    v = x.var
                    va = v.min(dim=self.dim, keepdim=True)[0]
                    s = torch.sqrt((2 * va + self.latent_noise_var) / V_S)
                    gamma = torch.sqrt((2 * va + self.latent_noise_var) / (v + va + self.latent_noise_var))
                    qq = []
                    slices = x.split(1, dim=self.dim)  # split dimension 1 by 1 elements
                    for y in range(int(n)):
                        slice_y = slices[y].expand(x.size())  # broadcast
                        a = (x.mean - slice_y.mean) / s
                        # we have one of them equal to zero, reset
                        a[:, y] = -100000
                        S1 = log_sum_exp_1d(a, dim=self.dim, keepdim=True)
                        qq = qq + [S1]  # collect in list
                    q = torch.cat(qq, dim=self.dim)  # cat all classes
                    q = -log1p_exp(q * gamma)
                    return F.log_softmax(q, dim=self.dim)
                else:
                    m = x.mean #- x.mean.max(dim=self.dim, keepdim=True)[0].detach()
                    va = x.var.min(dim=self.dim, keepdim=True)[0] #.detach()
                    s = torch.sqrt(2 * va / V_S)
                    sy = torch.sqrt((x.var + va) / V_S)
                    # gamma = s/ sy
                    gamma = torch.sqrt(va / (x.var + va))
                    a = m / s
                    ay = m / sy
                    S_ex = log_sum_exp_ex(a, dim=self.dim, keepdim=True)
                    q = ay - log_sum_exp2(S_ex * gamma, ay)
                    return q - log_sum_exp_1d(q, dim=self.dim, keepdim=True)
                    # sy = torch.sqrt(x.var / V_S)
                    # a = x.mean / s
                    # ay = x.mean / sy
                    # S_ex = log_sum_exp_ex(a, dim=self.dim, keepdim=True)
                    # q = ay - log_sum_exp2(S_ex * s / sy, ay)
            else:
                v = x.var.mean(dim=self.dim, keepdim=True)  # averagevariance over classes
                s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1)) + self.latent_noise_var) / V_S)
                q = x.mean / s
        else:  # better approx, quadratic time and linear memory complexity
            qq = []
            slices = x.split(1, dim=self.dim)  # split dimension 1 by 1 elements
            for y in range(int(n)):
                slice_y = slices[y].expand(x.size())  # broadcast
                a = (x.mean - slice_y.mean) / torch.sqrt((x.var + slice_y.var +  self.latent_noise_var) / V_S)
                q_y = -log_sum_exp_1d(a, dim=self.dim, keepdim=True)  # log probability for one class
                qq = qq + [q_y]  # collect in list
            q = torch.cat(qq, dim=self.dim)  # cat all classes
        # renormalize in log domain
        return F.log_softmax(q, dim=self.dim)
    
    def forward_sample(self, *args, **kwargs):
        """ Need to sample from a discrete distribution with n outcomes with probabilities specified by forward """
        """ For now, just return the distribution """
        return self.forward_AP1(*args, **kwargs)


class LogSoftmax(LogArgmax):
    """
    log softmax along dimension 1
    x: input activations (4D RandomVar)
    :return: log-probability of classes (4D Tensor)
    """

    def __init__(self, simplified: bool = False, dim=1, **kwargs):
        LogArgmax.__init__(self, simplified=simplified, dim=dim)
        self.latent_noise_var = V_S
        
    def upgrade(self, **kwargs):
        self.latent_noise_var = V_S
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        # calibrated in specific experiments
        # return F.log_softmax(x / 16, dim=1)
        # return F.log_softmax(x / 3.7, dim=1)
        # return F.log_softmax(x / 1.8, dim=1)
        #
        return F.log_softmax(x, dim=1)

    # def forward_AP2(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
    #     self.latent_noise_var = V_S
    #     return LogArgmax.forward_AP2(self, x, **kwargs)
        
    def forward_AP2(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
        if not hasattr(self, 'dim'):
            self.dim = 1
        n = float(x.size()[self.dim])
        if self.simplified:  # simple approx, linear complexity
            if True: # new linear time approximation
                return LogArgmax.forward_AP2(self, x, **kwargs)
                #
                # va = x.var.min(dim=self.dim, keepdim=True)[0].detach()
                m = x.mean #- x.mean.max(dim=self.dim, keepdim=True)[0].detach()
                va = x.var.min(dim=self.dim, keepdim=True)[0]
                s = torch.sqrt(2*va / V_S + 1)
                sy = torch.sqrt((x.var + va) / V_S + 1)
                # sy = torch.sqrt(x.var / V_S + 1)
                a = m / s
                ay = m / sy
                #S = log_sum_exp_1d(a,dim=self.dim);
                #S = a.logsumexp(dim=self.dim, keepdim=True)
                S_ex = log_sum_exp_ex(a, dim=self.dim, keepdim=True)
                q = ay - log_sum_exp2(S_ex * s / sy, ay)
                return q - log_sum_exp_1d(q, dim=self.dim, keepdim=True)
                # needs to be renormalized
                #
            else:
                v = x.var.mean(dim=self.dim, keepdim=True) # averagevariance over classes
                s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1)) ) / V_S + 1)
                # calibrated in specific experiments (also a hacky way to do things)
                # real dropout, norm=AP2
                # s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1)) )/square(0.45) / V_S + 1)
                # AP2 dropout, norm=AP2
                # s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1)) )/square(0.39) / V_S + 1)
                # s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1)) )/square(0.65) / V_S + 1)
                #
                # # s = torch.sqrt((x.var * ((n - 2) / (n - 1)) + v * (n / (n - 1))) / 0.193 / V_S + 1)
                q = x.mean / s
        else:  # better approx, quadratic time and linear memory complexity
            qq = []
            slices = x.split(1, dim=self.dim)  # split dimension 1 by 1 elements
            for y in range(int(n)):
                slice_y = slices[y].expand(x.size())  # broadcast
                a = (x.mean - slice_y.mean) / torch.sqrt((x.var + slice_y.var) / V_S + 1)
                q_y = -log_sum_exp_1d(a, dim=self.dim, keepdim=True)  # log probability for one class
                qq = qq + [q_y]  # collect in list
            q = torch.cat(qq, dim=self.dim)  # cat all classes
        # renormalize in log domain
        return F.log_softmax(q, dim=self.dim)
    #
    # def forward_sample(self, *args, **kwargs):
    #     """ Need to sample from a discrete distribution with n outcomes with probabilities specified by forward """
    #     """ For now, just return the distribution """
    #     return self.forward_AP1(*args, **kwargs)


class Softmax(Module):
    """
    softmax along given dimension
    x: input (RandomVar)
    dim: dimension along which to compute softmax
    :return: class probabilites and variations
    """
    def __init__(self, dim=1):
        Module.__init__(self)
        self.dim = dim

    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        # return F.softmax(x, self.dim)
        return F.log_softmax(x, dim=1).exp()


    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        y = RandomVar()
        # mean = x.mean / torch.sqrt(x.var / V_S + 1)
        # y.mean = F.softmax(mean, self.dim) # not numerically stable
        # y.mean = F.log_softmax(mean, dim=1).exp()
        y.mean = LogSoftmax(simplified=True, dim=self.dim).forward_AP2(x, **kwargs).exp()
        # y.mean /= y.mean.sum(dim = self.dim, keepdim=True)
        y.var = y.mean * (1 - y.mean)
        return y


class MaxPool2d(Module, nn.MaxPool2d):
    
    def __init__(self, kernel_size, stride=None, padding=0, dilation=1, return_indices=False, ceil_mode=False, **kwargs):
        """ Same parameters as as nn.AvgPool2d except no support for strides in AP2"""
        Module.__init__(self)
        nn.MaxPool2d.__init__(self, kernel_size, stride=stride, padding=padding, dilation=dilation, return_indices=return_indices, ceil_mode=return_indices)
        def to_tuple(a):
            if isinstance(a, (list, tuple)):
                if len(a) == 1:
                    return (a[0], a[0])
                else:
                    return a
            else:
                return (a, a)
        self.kernel_size = to_tuple(self.kernel_size)
        self.padding = to_tuple(self.padding)
        self.stride = to_tuple(self.stride)
        self.dilation = to_tuple(self.dilation)
    
    def out_size(self, size):
        sz = list(size)
        for i in [2, 3]:
            sz[i] = int((sz[i] + 2 * self.padding[i-2] - self.dilation[i-2]*(self.kernel_size[i-2]-1)-1) / self.stride[i-2] + 1)
        return sz
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return nn.MaxPool2d.forward(self, x)
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> [Tensor, RandomVar]:
        t = [-1.33751, 0.886763]
        if x.size()[2] == 1 and x.size()[3] == 1: # 1x1 input, normalization use case
            # case of i.i.d. values
            n = reduce(operator.mul, self.kernel_size, 1)
            q = 1 / n  # equal fraction
            H = -(q * math.log(q) + (1 - q) * math.log(1 - q))
            y = RandomVar()
            if True:
                y.mean = x.mean + (n * H / math.sqrt(V_S)) * x.var.sqrt()
                y.var = x.var * (n * logistic_cdf(t[0] + t[1] * logistic_icdf(q)))
            else:
                # Baseline simple approx
                y.mean = x.mean + x.var.sqrt()
                y.var = x.var
            check_var(y.var)
            return y
        # split reduced blocks as separate dimensions
        sz = list(x.size())
        ks = self.kernel_size
        rsz = [sz[0], sz[1], sz[2] // ks[0], sz[3] // ks[1], -1]
        xr = x.view([sz[0], sz[1], sz[2] // ks[0], ks[0], sz[3] // ks[1], ks[1]]).transpose(3, 4).contiguous().view(rsz)
        rsz[-1] = 1
        lq = LogArgmax(simplified=True, dim=4).forward_AP2(xr)
        q = lq.exp()
        logitq = logit(q)
        # lnq = torch.log((1 - q).clamp_(min=_finfo(q).tiny))
        # H1 = - (q * lq + (1 - q) * lnq)
        H = F.binary_cross_entropy_with_logits(input=logitq, target=q, reduction='none')
        H = H.detach()  # gradient simplification
        y = RandomVar()
        # y.mean = (q * xr.mean).sum(dim=4) + (xr.var.sqrt() * H).sum(dim=4) / math.sqrt(V_S)
        # mu_hat = xr.mean + xr.var.sqrt() * (H / q)
        # y.mean = (mu_hat * q).sum(dim=4)
        m1 = (q * xr.mean).sum(dim=4)
        y.mean = m1.clone()
        y.mean += (xr.var.sqrt() * H).sum(dim=4) / math.sqrt(V_S)
        # y.mean = nn.MaxPool2d.forward(self, x.mean)
        # icdf_q = lq - lnq
        S = logistic_cdf(t[0] + t[1] * logitq)
        y.var = (S * xr.var).sum(dim=4)
        # y.var += (q * square(xr.mean - y.mean.view(rsz))).sum(dim=4)
        y.var += (  q * square(xr.mean - m1.view(rsz))  ).sum(dim=4)
        check_var(y.var)
        check_real(y.mean)
        return y
    
    def __repr__(self):
        tmpstr = self.__class__.__name__ + '(kernel_size={}, stride={})'.format(self.kernel_size, self.stride)
        return tmpstr


class AvgPool2d(Module, nn.AvgPool2d):
    """ Same as nn.AvgPool2d """
    def __init__(self, kernel_size, **kwargs):
        """ Same parameters as as nn.AvgPool2d """
        Module.__init__(self)
        nn.AvgPool2d.__init__(self, kernel_size, **kwargs)
        def to_tuple(a):
            if isinstance(a, (list, tuple)):
                if len(a) == 1:
                    return (a[0], a[0])
                else:
                    return a
            else:
                return (a, a)
        self.kernel_size = to_tuple(self.kernel_size)
        self.padding = to_tuple(self.padding)
        self.stride = to_tuple(self.stride)

    def out_size(self, size):
        sz = list(size)
        for i in [2,3]:
            sz[i] = int((sz[i] + 2*self.padding[0] - self.kernel_size[0])/self.stride[0] +1 )
        return sz
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return nn.AvgPool2d.forward(self, x)
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> [Tensor, RandomVar]:
        y = RandomVar()
        weight = reduce(operator.mul, self.kernel_size, 1)
        if x.size()[2] == 1 and x.size()[3] == 1:
            y.mean = x.mean
            y.var = x.var / weight
        else:
            y.mean =  nn.AvgPool2d.forward(self, x.mean)
            y.var = nn.AvgPool2d.forward(self, x.var) / weight
        return y

    def __repr__(self):
        tmpstr = self.__class__.__name__ + '(kernel_size={}, stride={})'.format(self.kernel_size, self.stride)
        return tmpstr


class AdaptiveAvgPool2d(Module):
    """ Replicates nn.AdaptiveAvgPool2d """
    def __init__(self, output_size = (1,1)):
        Module.__init__(self)
        self.output_size = list(output_size)
        if len(output_size)==1:
            self.output_size.append(output_size[0])

    def upgrade(self, **kwargs):
        if not hasattr(self, 'output_size'):
            self.output_size = (1,1)

    def out_size(self, size):
        sz = list(size)
        if hasattr(self, 'output_size'):
            if self.output_size[0] is not None:
                sz[2] = self.output_size[0]
            if self.output_size[1] is not None:
                sz[3] = self.output_size[1]
        return sz
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return torch.nn.functional.adaptive_avg_pool2d(x, self.output_size)
    
    def forward_AP2(self, x: RandomVar, compute_normalization=False, in_size=None, **kwargs) -> RandomVar:
        y = RandomVar()
        if in_size is None:
            in_size = x.size()
        out_sz = self.out_size(in_size)
        kernel_sz = [in_size[2] / out_sz[2], in_size[3] / out_sz[3]]
        weight = kernel_sz[0]*kernel_sz[1]
        #
        y.mean = torch.nn.functional.adaptive_avg_pool2d(x.mean, self.output_size)
        y.var = torch.nn.functional.adaptive_avg_pool2d(x.var, (1, 1)) / weight
        return y


class Upsample(Module, nn.Upsample):
    def __init__(self, **kwargs):
        Module.__init__(self)
        nn.Upsample.__init__(self, **kwargs)
    
    def out_size(self, size):
        sz = list(size)
        if self.size is not None:
            return sz[0:2] + list(self.size)
        else:
            return sz[0:2] + [sz[2]*self.scale_factor, sz[3]*self.scale_factor]
        
    def forward_AP1(self, x: Tensor, out_size=None, **kwargs) -> Tensor:
        if out_size is not None:
            self.size = out_size
        return nn.Upsample.forward(self, x)

    def forward_AP2(self, x: RandomVar, out_size=None, in_size=None, **kwargs) -> RandomVar:
        if out_size is not None:
            self.size = out_size
        y = RandomVar()
        # weight = ( np.array(self.size) / np.array(in_size[2:]) ).prod()
        if self.scale_factor is not None:
            if isinstance(self.scale_factor, tuple):
                weight = np.array(self.scale_factor).prod()
            else:
                weight = self.scale_factor**2
        elif self.size is not None:
            weight = self.size[0]*self.size[1] / (in_size[2]*in_size[3])
        # weight = 1.0
        if x.size()[2] == 1 and x.size()[3] == 1:
            y.mean = x.mean
            # assert (x.var > 0).all()
            y.var = x.var * weight
            # y.mean.fill_(0.0)
            # y.var.fill_(1.0)
            # y.mean = y.mean.detach()
            # y.var = y.var.detach()
            return y
        y.mean = nn.Upsample.forward(self, x.mean)
        y.var = nn.Upsample.forward(self, x.var) * weight
        return y

class AddNoise(Module):
    """
    injects noise (the noise variance may be learned in the future)
    AP1: do nothing
    Ap2: increase varinace
    sample: increase variance and draw from Normal
    """
    
    def __init__(self, noise_var):
        Module.__init__(self)
        self.noise_var = noise_var
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return x
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        if self.noise_var > 0:
            return RandomVar(x.mean, x.var + self.noise_var)
        else:
            return x
    
    def forward_sample(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
        """
        sample from a gaussian with added variance
        """
        if torch.is_tensor(x):
            if self.noise_var > 0:
                n = x.new(x.size()).normal_(std=math.sqrt(self.noise_var))
                return x + n
            else:
                return x
        else:
            y = self.forward_AP2(x, **kwargs)  # here we add variance and then sample, instead of sampling input and noise separately
            return y.sample()


class Flatten(Module):
    
    def out_size(self, size):
        return [size[0], size[1] * size[2] * size[3]]
    
    def can_reduce_input(self):
        return False
    
    def forward(self, x: [Tensor, RandomVar], **kwargs) -> [Tensor, RandomVar]:
        n = x.size()[0]
        if x.dim() == 2:
            return x
        return x.contiguous().view([n, -1])


class InputNoise(Module):
    """
        changes the input to match the intended forward method: Tensor for AP1/sample and RandomVar for AP2
        when forward method is AP2 and input is a Tensor/Tensor, the default noise_var is added and if it is RandomVar, no noise_var added
    """
    
    def __init__(self, noise_var = 0):
        Module.__init__(self)
        self.noise_var = noise_var

    def is_stochastic(self):
        return self.noise_var > 0
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return to_variable(x)
    
    def forward_AP2(self, x: Union[Tensor, RandomVar], **kwargs) -> RandomVar:
        if isinstance(x, RandomVar):
            return x
        else:
            x = to_variable(x)
            return RandomVar(x, x.new(x.size()).fill_(self.noise_var))
    
    def forward_sample(self, x: Union[Tensor, RandomVar], **kwargs) -> Tensor:
        if self.noise_var > 0:
            return self.forward_AP2(x).sample()
        else:
            return self.forward_AP1(x)


class RandomScaleBias(Module):
    """Random Scale Bias module, currently a stationary distribution over all channels / dimensions """
    """Y = (X + V)*U,  U has statistics (1,var_mult) and V has statistics (0, var_add)"""
    
    def __init__(self, reg_BN_k=None, reg_BN_v=None,  **kwargs):
        Module.__init__(self)
        assert reg_BN_k is not None
        assert reg_BN_v is not None
        self.k = reg_BN_k
        self.v = reg_BN_v
        self.s = None  # stochastic scale
        self.b = None  # stochastic scale
        self.register_buffer('a', Tensor())
        self.reset_parameters()
        
    def reset_parameters(self):
        pass
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        assert not self.training, 'AP1 shouldn\'t be used in training mode for this layer'
        return x # we keep that E[(x + V)*U] = x
    
    def reg_variant(self, sz):
        sz = list(sz)
        if self.v == 0:
            # uncorrelated in batch, multiplicative
            sz[0] = 1  # correlated in batch
            sz[2] = 1  # correlated in X
            sz[3] = 1  # correlated in Y
            var_mult = 1 / (2 * self.k)
            var_add = 0
        elif self.v == 1:
            # multiplicative
            sz[2] = 1  # correlated in X
            sz[3] = 1  # correlated in Y
            var_mult = 1 / (2 * self.k)
            var_add = 0
        else:
            raise NotImplementedError()
        return sz, var_mult, var_add
    
    def forward_AP2(self, x: RandomVar, sample_params=False, **kwargs) -> RandomVar:
        if self.training and kwargs.get('compute_normalization'):
            self.sample_perturbation(list(x.size())) # scale and bias are stochastic perturbations of true Parameters
            return (x + self.b) * self.s
        else:
            return x
    
    def sample_perturbation(self, sz): # todo call this before normalization with the right size
        sz, var_mult, var_add = self.reg_variant(sz)
        # Log-normal multiplicative noise model
        l_var = math.log(1 + var_mult)
        l_std = math.sqrt(l_var)
        l_mean = -l_var / 2
        s = self.a.new(torch.Size(sz)).log_normal_(mean=l_mean, std=l_std)  # correlated noise over batch and spatial
        b = self.a.new(torch.Size(sz)).normal_(mean=0, std=math.sqrt(var_add))
        # make noise bounded to prevent accidents
        s = torch.clamp(s, min=0, max=1 + 3 * math.sqrt(var_mult))
        b = torch.clamp(b, min=-3 * math.sqrt(var_add), max=3 * math.sqrt(var_add))
        self.s = s
        self.b = b
    
    def forward_sample(self, x: [Tensor, RandomVar], **kwargs) -> Tensor:
        assert (self.s is not None)
        y = (x + self.b) * self.s
        return y

class ScaleBias(Module):
    """
    applies per-channel scale and bias y(n,c,w,h) = x(n,c,w,h) * w(c) + b(c)
    """
    
    def __init__(self, input_channels, **kwargs):
        Module.__init__(self)
        self.bias = Parameter(Tensor(input_channels))
        self.weight = Parameter(Tensor(input_channels))
        
        self.reset_parameters()
    
    def reset_parameters(self):
        # this is ment to be for renormalization, not random
        self.bias.data.fill_(0.0)
        self.weight.data.fill_(1.0)

    def get_affine(self):
        """ get the affine transform in the form f(x) = x*weight + bias"""
        return self.weight, self.bias

    def set_affine(self, weight, bias):
        """ set to affine f(x) = x*weight + bias"""
        self.weight.data = weight
        self.bias.data = bias
    
    def align_shape(self, x: Tensor):
        # align dimensions to input for broadcasting
        if x.dim() == 4:
            shape = [1, -1, 1, 1]
        elif x.dim() == 2:
            shape = [1, -1]
        else:
            raise ValueError("don\'t know how to trat such input dimension")
        w = self.weight.view(shape)
        b = self.bias.view(shape)
        return w, b
    
    def forward(self, x, **kwargs):
        w, b = self.align_shape(x)
        return x * w + b
    
    # def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
    #     w, b = self.align_shape(x)
    #     return x * w + b
    #
    # def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
    #     y = RandomVar()
    #     w, b = self.align_shape(x.mean)
    #     y.mean = x.mean * w + b
    #     y.var = x.var * square(w)
    #     return y

    def __repr__(self):
        tmpstr = 'bias: {:.2g}-{:.2g} scale: {:.2g}-{:.2g}'.format(self.bias.min().item(), self.bias.max().item(), self.weight.min().item(), self.weight.max().item())
        return tmpstr
        

class Gain(Module):
    """
    applies per-channel gain: x = x - alpha m, where m is the mean of x in the layer
    """
    def __init__(self, input_channels, **kwargs):
        Module.__init__(self)
        #self.bias = Parameter(Tensor(input_channels))
        #self.weight = Parameter(Tensor(input_channels))
        #self.register_buffer('weight', Tensor(Tensor(input_channels)))
        self.weight = Parameter(Tensor(input_channels, input_channels))
        # self.sb = ScaleBias(input_channels)
        self.reset_parameters()
    
    def reset_parameters(self):
        #self.weight.data.fill_(1/32)
        self.weight.data.fill_(0)
        # self.sb.reset_parameters()

    def forward_AP1(self, x, **kwargs):
        n = x.size()[0]
        if x.dim() == 2:
            return x
        sz = [x.size()[0], x.size()[1]]
        m = x.view(sz + [-1]).mean(dim=2)
        y = x + F.linear(m, self.weight).view(sz + [1,1])
        # y = x + m * self.weight.view([1,-1,1,1])
        # y = self.sb.forward(y, **kwargs)
        return y
    
    def forward_AP2(self, x, **kwargs):
        sz = [x.size()[0], x.size()[1]]
        m = x.mean.view(sz + [-1]).mean(dim=2)
        y = x + F.linear(m, self.weight).view(sz + [1,1])
        # y = x + m * self.weight.view([1,-1,1,1])
        # y = self.sb.forward(y, **kwargs)
        return y

class StochasticScaleBias(Module):
    def __init__(self, input_channels, **kwargs):
        Module.__init__(self)
        self.sb = ScaleBias(input_channels)
        if 'reg_BN_k' in kwargs and kwargs['reg_BN_k'] is not None:
            self.reg = RandomScaleBias(**kwargs)
        else:
            self.reg = None
        self.reset_parameters()
    
    def reset_parameters(self):
        self.sb.reset_parameters()
    
    def forward(self, x, **kwargs):
        if self.reg is not None:
            x = self.reg.forward(x, **kwargs)
        x = self.sb.forward(x, **kwargs)
        return x
    
    @property
    def weight(self):
        return self.sb.weight
    
    @property
    def bias(self):
        return self.sb.bias

    @weight.setter
    def weight(self, value):
        self.sb.weight.data = value

    @bias.setter
    def bias(self, value):
        self.sb.bias.data = value


class SSB1(Module):
    def __init__(self, input_channels, **kwargs):
        Module.__init__(self)
        self.input_channels = input_channels
        # self.B = Parameter(Tensor(input_channels, device = torch.device('cuda')))
        m_sz = [1, input_channels, 1, 1]
        if kwargs.get('SB_channelwise'):
            var_sz = [1, input_channels, 1, 1]
        else: # layerwise
            var_sz = [1, 1, 1, 1]
        if kwargs.get('stochastic_B'):
            # self.B = StochasticParamNormal(torch.zeros(msz), var_init=0.1 ** 2, mean_prior=0, var_prior=1.0 ** 2)
            self.B = StochasticParamNormal(torch.zeros(m_sz), var_sz=var_sz, var_init=1e-4 ** 2, mean_prior=0, var_prior=0.3 ** 2, positive_mean=False)
            #CIFAR experiment was:  var_init= 0.1 ** 2
        else:
            self.B = Parameter(Tensor(input_channels))
            self.B.data.fill_(0.0)

        if kwargs.get('stochastic_S'):
            if kwargs.get('ScaleGamma'):
                self.S = StochasticParamGamma(torch.ones(m_sz), var_sz=var_sz, var_init=0.1 ** 2, mean_prior=10, var_prior=10.0 ** 2)
            elif kwargs.get('ScaleNormal'):
                self.S = StochasticParamNormal(torch.ones(m_sz), var_sz=var_sz, var_init=1e-4 ** 2, mean_prior=1, var_prior=10.0 ** 2)
                # CIFAR experiment was:  var_init= 0.1 ** 2
            elif kwargs.get('ScaleLogNormal'):
                self.S = StochasticParamLogNormal(torch.ones(m_sz), var_sz=var_sz, var_init=0.1 ** 2, mean_prior=1, var_prior=1.0 ** 2)
            else:
                raise KeyError()
        else:
            self.S = Parameter(Tensor(input_channels))
            self.S.data.fill_(1.0)
            
        self.reset_parameters()
        self.sample_used = False
        
    def add_sb(self):
        self.sb = ScaleBias(self.input_channels)
        self.sb.reset_parameters()
        
    def reset_parameters(self):
        pass
    
    def train(self, training=True):
        self.sample_used = True
        super().train(training)

    def get_affine(self):
        """ get the affine transform in the form f(x) = x*weight + bias"""
        w = self.weight.view([-1])
        b = w * self.bias.view([-1])
        return w, b

    def set_affine(self, weight, bias):
        """ set to affine f(x) = x*weight + bias"""
        self.weight = weight
        self.bias = (bias / weight)
    
    @property
    def bias(self):
        if self.B is None:
            return 0
        elif isinstance(self.B, Tensor):
            return self.B.view([1, -1, 1, 1])
        else:
            return self.B.current

    @bias.setter
    def bias(self, value: Tensor):
        if self.B is None:
            raise ValueError('Nias term was constructed as None')
        elif isinstance(self.B, Tensor):
            self.B.data = value.view([1, -1, 1, 1])
        else:
            self.B.current = value.view([1, -1, 1, 1])

    @property
    def weight(self) -> Tensor:
        if isinstance(self.S, Tensor):
            return self.S.view([1, -1, 1, 1])
        else:
            return self.S.current

    @weight.setter
    def weight(self, value: Tensor):
        if isinstance(self.S, Tensor):
            self.S.data = value.view([1, -1, 1, 1])
        else:
            self.S.current = value.view([1, -1, 1, 1])
    
    def forward(self, x: [Tensor, RandomVar], **kwargs) -> RandomVar:
        if (self.training or kwargs.get('sample_params')) and (kwargs.get('compute_normalization') or self.sample_used):
            if isinstance(self.S, StochasticParam):
                self.S.new_sample(list(x.size()))
            if isinstance(self.B, StochasticParam):
                self.B.new_sample(list(x.size()))
        if self.training and kwargs.get('compute_normalization'):
            self.sample_used = False # normalization
        else:
            self.sample_used = True
        if self.B is None:
            y = x * self.weight
        else:
            y = (x + self.bias) * self.weight
        if getattr(self, 'sb', None):
            y = self.sb.forward(y, **kwargs)
        return y
    
    def __repr__(self):
        #tmpstr = 'B: {:.2g} S_mean: {:.2g}-{:.2g} S_var: {:.2g}-{:.2g}'.format(self.B.mean().item(), self.S.mean.abs().min().item(), self.S.mean.abs().max().item(), self.S.var.min().item(), self.S.var.max().item())
        tmpstr = '({}) '.format(self.__class__.__name__)
        if isinstance(self.B, Tensor):
            tmpstr += ('B mean:{:.2g} '.format(self.bias.mean().item()) )
        else:
            tmpstr += ('B:' + repr(self.B) + ' ')
        if isinstance(self.S, Tensor):
            tmpstr += ('S mean:{:.2g} '.format(self.weight.mean().item()) )
        else:
            tmpstr += ('S:' + repr(self.S))
        tmpstr += super().__repr__()
        return tmpstr


class VariationalDropout(SSB1):
    def __init__(self, input_channels, **kwargs):
        SSB1.__init__(self, input_channels, **kwargs)
        m_sz = [1, input_channels, 1, 1]
        if kwargs.get('SB_channelwise'):
            var_sz = [1, input_channels, 1, 1]
        else: # layerwise
            var_sz = [1, 1, 1, 1]
        self.B = None
        self.S = StochasticParamNormal_LogUniform(torch.ones(m_sz), var_sz=var_sz, var_init=0.1 ** 2, var_prior = 1.0)
        self.reset_parameters()
        self.sample_used = False


class SSB(Module):
    """ Stochastic Scale Bias, for Bayesian Learning"""

    def __init__(self, input_channels, **kwargs):
        Module.__init__(self)
        if kwargs.get('ssb_affine'):
            self.register_buffer('B_mean', Tensor(input_channels))
            self.register_buffer('S_mean', Tensor(input_channels))
            self.sb = ScaleBias(input_channels)
        else:
            self.register_parameter('B_mean', Parameter(Tensor(input_channels)))
            self.register_parameter('S_mean', Parameter(Tensor(input_channels)))
            self.sb = None

        if 'stochastic_B' in kwargs and kwargs['stochastic_B']:
            # self.register_parameter('B_lvar', Parameter(Tensor(input_channels)))
            self.register_parameter('B_std', Parameter(Tensor(input_channels)))
        else:
            # self.register_buffer('B_lvar', Tensor(input_channels))
            self.register_buffer('B_std', Tensor(input_channels))

        if 'stochastic_S' in kwargs and kwargs['stochastic_S']:
            # self.register_parameter('S_lvar', Parameter(Tensor(input_channels)))
            self.register_parameter('S_std', Parameter(Tensor(input_channels)))
        else:
            # self.register_buffer('S_lvar', Tensor(input_channels))
            self.register_buffer('S_std', Tensor(input_channels))
        
        self.b = None
        self.s = None
        self.reg_loss = None
        self.eps = 1e-8

        self.register_buffer('B_prior', RandomVar(0.0, 10.0 ** 2))  # bias prior
        self.register_buffer('S_prior', RandomVar(0.0, 10.0))  # exponential scale prior
        # self.register_buffer('B_prior', RandomVar(0, 5 ** 2))  # bias prior
        # self.register_buffer('S_prior', RandomVar(0, 5))  # exponential scale prior
        self.reset_parameters()
        self.sample_used = False

    def reset_parameters(self):
        self.B_mean.data.fill_(0.0)
        self.S_mean.data.fill_(0.0)
        # self.B_log_var.data.fill_(math.log(0.1))
        # starting with a tiny variance - no noise, may be a good idea, the optimization will push it up
        # self.B_lvar.data.fill_(math.log(0.001**2))
        # self.S_lvar.data.fill_(math.log(0.001**2))
        self.B_std.data.fill_(0.001)
        self.S_std.data.fill_(0.001)
        #
        # self.S_log_var.fill_(math.log(0.1))
        if self.sb is not None:
            self.sb.reset_parameters()

    def project(self):
        self.B_std.data = self.B_std.data.clamp(min=0)
        self.S_std.data = self.S_std.data.clamp(min=0)

    @property
    def B_var(self):
        return square(self.B_std) + self.eps
        # return self.B_lvar.exp()

    @property
    def S_var(self):
        return square(self.S_std) + self.eps
        # return self.S_lvar.exp()

    @property
    def B(self):
        return RandomVar(self.B_mean, self.B_var)

    @property
    def S(self):
        return RandomVar(self.S_mean, self.S_var)

    @property
    def bias(self) -> Tensor:
        return self.B_mean

    @bias.setter
    def bias(self, value: Tensor):
        self.B.mean.data = value

    @property
    def weight(self) -> Tensor:
        "mean of the logNormal distribution"
        # return (self.S_mean + self.S_log_var.exp() / 2).exp()
        return (self.S_mean + self.S_var / 2).exp()
        # return self.S_mean.exp()

    @weight.setter
    def weight(self, value: Tensor):
        "set the mean of the logNormal distribution to a desired value"
        assert (value > 0).all()
        self.S.mean.data = value.log() - self.S_var.data / 2
        
    @property
    def W(self):
        "Random Tensor of the weight"
        # variance of LogNOrmal distribution
        v = (self.S_var.exp() - 1) * (2 * self.S_mean + self.S_var).exp()
        return RandomVar(self.weight, v)

    def get_affine(self):
        """ get the affine transform in the form f(x) = x*weight + bias"""
        w = self.weight
        return w, w * self.bias

    def set_affine(self, weight, bias):
        """ set to affine f(x) = x*weight + bias"""
        self.weight.data = weight
        self.bias.data = bias / weight

    def align_shape(self, dim) -> [int]:
        """ align dimensions to input for broadcasting """
        if dim == 4:
            shape = [1, -1, 1, 1]
        elif dim == 2:
            shape = [1, -1]
        else:
            raise ValueError("don\'t know how to treat such input dimension")
        return shape

    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        """ Propagate using the mean values of S and B """
        sz = self.align_shape(x.dim())
        y = (x + self.bias.view(sz)) * self.weight.view(sz)
        if self.sb is not None:
            y = self.sb.forward_AP1(y, **kwargs)
        return y
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        sz = self.align_shape(x.dim())
        if self.training and kwargs.get('compute_normalization'):
            self.sample_parameters(list(x.size()), **kwargs) # scale and bias are stochastic perturbations of true parameters
            y = (x + self.b) * self.s.exp()
        else:
            y = (x + self.bias.view(sz)) * self.weight.view(sz)
        if self.sb is not None:
            y = self.sb.forward_AP2(y, **kwargs)
        return y
    
    def sample_parameters(self, sz, **kwargs):
        if len(sz) == 4:
            sz[2] = 1
            sz[3] = 1
        # assert sz[0] > 1
        # !hack
        # sz[0] = 1 # batch-coherent noise
        #
        sz0 = self.align_shape(len(sz))
        self.b = self.B.view(sz0).expand(sz).sample(bounded = True) # expand over batch size, draw a differentiable sample
        self.s = self.S.view(sz0).expand(sz).sample(bounded = True)
        self.sample_used = False

    def compute_KL(self, **kwargs):
        """ KL(approx posterior || prior) """
        self.reg_loss = self.B.KL(self.B_prior).sum() + self.S.KL(self.S_prior).sum()
        # self.reg_loss = self.S.KL(self.S_prior).sum()

    def forward_sample(self, x: Tensor, **kwargs) -> Tensor:
        """ Propagate using the sample """
        self.compute_KL(**kwargs)
        assert not self.sample_used
        self.sample_used = True
        assert self.b.dim() == x.dim()
        y = (x + self.b) * self.s.exp()
        if self.sb is not None:
            y = self.sb.forward_sample(y, **kwargs)
        return y

    def __repr__(self):
        # tmpstr = self.__class__.__name__
        tmpstr = 'B_var: {:.2g} W_var: {:.2g}-{:.2g} W_mean: {:.2g}-{:.2g}'.format(self.B_var.mean().item(),self.W.var.min().item(),self.W.var.max().item(), self.W.mean.abs().min().item(),self.W.mean.abs().max().item())
        return tmpstr


class LinearModule(Module):
    """Base class for Linear, Conv2d, ConvTranspose2d, etc."""
    
    @abstractmethod
    def out_size(self, size) -> [int]:
        pass
    
    @property
    @abstractmethod
    def in_c_dim(self) -> int:
        pass
    
    @property
    @abstractmethod
    def out_c_dim(self) -> int:
        pass
    
    def merge_after(self, s: ScaleBias):
        """sw .* (W * x + b) + sb = (sw .* W) * x + sw .* b + sb"""
        if self.bias is None:
            self.bias = Parameter(self.weight.new(self.get_out_channels()))
            self.bias.data.fill_(0.0)
        s_shape = [1] * self.weight.dim()
        s_shape[self.out_c_dim] = -1
        self.weight.data *= s.weight.data.view(s_shape)  # multiply with out_channels of W
        self.bias.data *= s.weight.data.view([-1])
        self.bias.data += s.bias.data.view([-1])
    
    def merge_before(self, s: ScaleBias):
        if self.bias is None:
            self.bias = Parameter(self.weight.new(self.get_out_channels()))
            self.bias.data.fill_(0.0)
        """W*(sw .* x + sb) + b = (W .* sw) * x + W*sb + b"""
        b = self.forward_AP1(s.bias.view([1, -1])).view([-1])
        self.bias.data += b.data
        s_shape = [1] * self.weight.dim()
        s_shape[self.out_c_dim] = -1
        self.weight.data *= s.weight.data.view(s_shape)  # multiply with in_channels of W
    
    def w_norm(self) -> Tensor:
        # keep out_channels
        s = square(self.weight)
        for d in range(self.weight.dim()):
            if d != self.out_c_dim:
                s = s.sum(dim=d, keepdim=True)
        return torch.sqrt(s + 1e-16)
    
    def project(self) -> [Tensor, Tensor]:
        """project out scale and bias """
        # normalize per output channel
        s = self.w_norm()
        self.weight.data = (self.weight / s).data
        b = self.bias
        self.bias = None
        return s, b

    def __repr__(self):
        return 'W {}'.format(list(self.weight.size()))

class Linear(LinearModule, nn.Linear):
    """
    Same semantics as torch.nn.Linear
    """
    """weight: (out_channels, in_channels) """
    """bias: (out_channels) """
    
    @property
    def in_c_dim(self) -> int:
        return 1
    
    @property
    def out_c_dim(self) -> int:
        return 0
    
    def __init__(self, in_features, out_features, bias=True):
        LinearModule.__init__(self)
        nn.Linear.__init__(self, in_features, out_features, bias=bias)
    
    def out_size(self, size):
        sz = list(size)
        sz[1] = self.out_features
        return sz
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return F.linear(x, self.weight, self.bias)
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        y = RandomVar()
        y.mean = F.linear(x.mean, self.weight, self.bias)
        y.var = F.linear(x.var, square(self.weight))
        return y


class ConvBase(LinearModule):
    
    def __init__(self, in_channels, out_channels, **kwargs):
        # self.approx_params = odict()
        # self.approx_params.logscale = Parameter((torch.zeros([out_channels])).cuda())
        #
        self.upgrade(approx_params=True)
        pass
    
    def upgrade(self, **kwargs):
        if kwargs.get('approx_params') and not hasattr(self, 'approx_params'):
            self.approx_params = odict()
            self.approx_params.logscale = Parameter((torch.zeros([self.get_out_channels()])).to(self.weight))
    
    @abstractmethod
    def conv_reduce(self, x: Tensor, weight, bias=None, output_size=None, **kwargs) -> Tensor:
        pass
    
    def forward_AP1(self, x: Tensor, output_size=None, **kwargs) -> Tensor:
        return self.conv_reduce(x, self.weight, self.bias, output_size=output_size, **kwargs)
    
    def forward_AP2(self, x: RandomVar, output_size=None, **kwargs) -> RandomVar:
        y = RandomVar()
        y.mean = self.conv_reduce(x.mean, self.weight, self.bias, output_size=output_size, **kwargs)
        if kwargs.get('correlated_var'):
            v = x.var.sum(dim=2, keepdim=True).sum(dim=3, keepdim=True)
            w = self.weight.sum(dim=2, keepdim=True).sum(dim=3, keepdim=True)
            y.var = self.conv_reduce(v, square(w), None, output_size=output_size, **kwargs)
        else:
            y.var = self.conv_reduce(x.var, square(self.weight), None, output_size=output_size, **kwargs)
        if hasattr(self, 'approx_params') and not( kwargs.get('init_normalization') or  kwargs.get('compute_normalization')):
            # y.var *= self.approx_params.logscale.exp().view([1, -1, 1, 1])
            if not self.approx_params.logs1 is None:
                correlated_var = square(self.conv_reduce(x.var.sqrt(), self.weight, None, output_size=output_size, **kwargs))
                s1 = self.approx_params.logs1.exp().view([1, -1, 1, 1])
                s2 = self.approx_params.logs2.exp().view([1, -1, 1, 1])
                y.var = y.var * s1 + correlated_var * s2
            if not self.approx_params.corr_logit is None:
                # r = F.tanh(self.approx_params.corr_logit).view([1, -1, 1, 1])
                r = torch.sigmoid(self.approx_params.corr_logit - 2).view([1, -1, 1, 1])
                correlated_var = square(self.conv_reduce(x.var.sqrt(), self.weight, None, output_size=output_size, **kwargs))
                y.var = y.var*(1-r) + r*correlated_var
            elif not self.approx_params.v_logit is None:
                v = F.tanh(self.approx_params.v_logit)
                y.var = self.conv_reduce(x.var, square(self.weight)*(1-square(v)), None, output_size=output_size, **kwargs)
                correlated_var = square(self.conv_reduce(x.var.sqrt(), self.weight*v, None, output_size=output_size, **kwargs))
                y.var = y.var + correlated_var
            elif self.approx_params.logscale is not None:
                y.var *= self.approx_params.logscale.to(y.var).exp().view([1, -1, 1, 1])
                
            if not (y.var.data >= 0).all():
                raise NumericalProblem('variance is negative or nan')
            pass
        return y
    
    def __repr__(self):
        sz = list(self.weight.size())
        return 'W [{}->{} {}x{}] stride={}, dilation={}'.format(sz[self.in_c_dim], sz[self.out_c_dim], sz[2], sz[3], self.stride, self.dilation)

class Conv2d(ConvBase, nn.Conv2d):
    """
    Same semantics as torch.nn.Conv2d
    Extension: 1 pixel input is extended coherently to give the assumed output size
    """
    """weight: (out_channels, in_channels, X, Y), if not transposed"""
    """bias: (out_channels) """
    
    def __init__(self, in_channels, out_channels, kernel_size, **kwargs):
        nn.Conv2d.__init__(self, in_channels, out_channels, kernel_size, **kwargs)
        ConvBase.__init__(self, in_channels, out_channels)
   
    @property
    def in_c_dim(self) -> int:
        return 1
    
    @property
    def out_c_dim(self) -> int:
        return 0
    
    def out_size(self, size):
        """ expressions copied from documentation """
        N = size[0]
        H_in = size[2]
        W_in = size[3]
        H_out = math.floor((H_in + 2 * self.padding[0] - self.dilation[0] * (self.kernel_size[0] - 1) - 1) / self.stride[0] + 1)
        W_out = math.floor((W_in + 2 * self.padding[1] - self.dilation[1] * (self.kernel_size[1] - 1) - 1) / self.stride[1] + 1)
        C_out = self.out_channels
        return [N, C_out, max(H_out,1), max(W_out,1)]
    
    def conv_reduce(self, x: Tensor, weight, bias=None, output_size=None, **kwargs) -> Tensor:
        if x.size()[2] == 1 and x.size()[3] == 1:  # 1 pixel input
            weight = weight.sum(dim=2, keepdim=True).sum(dim=3, keepdim=True)
            return F.conv2d(x, weight, bias, groups=self.groups)
        else:
            return F.conv2d(x, weight, bias, self.stride, self.padding, self.groups)


class SConv2d(Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, **kwargs):
        prototype = Conv2d(in_channels, out_channels, kernel_size, **kwargs)
        # make S_weight available in the __dict__ so property weight would work
        sz = list(prototype.weight.size())
        del sz[prototype.out_c_dim]
        N = reduce(operator.mul, sz, 1)
        var_prior = 0.2**2 / (2*N)
        object.__setattr__(self, 'S_weight', StochasticParameter(prototype.weight, var_init=0.1*var_prior, var_prior=var_prior, mean_prior = False))
        # now can initialize Conv2d with the weight overriden
        Conv2d.__init__(self, in_channels, out_channels, kernel_size, **kwargs)
        # reassign S_weight to self._modules
        self.S_weight = self.S_weight
    
    @property
    def weight(self):
        return self.S_weight.current
    
    @weight.setter
    def weight(self, value):
        self.S_weight.current = value

    def forward_AP2(self, x: RandomVar, output_size=None, **kwargs) -> RandomVar:
        if self.training and kwargs.get('compute_normalization'):
            self.S_weight.new_sample()
        return Conv2d.forward_AP2(self, x, output_size=output_size, **kwargs)


class VariationalDropoutRT_Conv2d(Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, **kwargs):
        prototype = Conv2d(in_channels, out_channels, kernel_size, **kwargs)
        # make S_weight available in the __dict__ so property weight would work
        
        m_sz = prototype.weight.size()
        var_sz = [1, 1, 1, 1]
        # S_weight = StochasticParamNormal_VariationalDropoutRT(torch.ones(m_sz), var_sz=var_sz, var_init=0.01 ** 2, var_prior=1.0)
        S_weight = StochasticParamNormal_VariationalDropoutRT(torch.ones(m_sz), var_sz=var_sz, var_init=0.1 ** 2)
        # set it to dict so that weight property works before super constructors are called
        object.__setattr__(self, 'S_weight', S_weight)
        # now can initialize Conv2d and Module
        Conv2d.__init__(self, in_channels, out_channels, kernel_size, **kwargs)
        # reassign S_weight to self._modules
        self.S_weight = S_weight
        self.reset_parameters()
        self.sample_used = True
    
    @property
    def weight(self):
        return self.S_weight.current
    
    @weight.setter
    def weight(self, value):
        self.S_weight.current = value
        
    def forward_sample(self, x, output_size=None, **kwargs):
        y = RandomVar()
        y.mean = self.conv_reduce(x, self.S_weight.mean, self.bias, output_size=output_size, **kwargs)
        y.var = self.conv_reduce(square(x) + 1e-6, self.S_weight.var, output_size=output_size, **kwargs)
        # y.var = self.conv_reduce(square(x), square(self.S_weight.mean), output_size=output_size, **kwargs)
        # self.S_weight._sample = self.S_weight.mean # sample size needed for KL
        # n = y.mean.data.new(y.mean.size())
        # n.normal_()
        # n = torch.clamp(n, min=-3, max=3)
        # assert (y.var.min() >= 0)
        y.var.clamp_(min=1e-16)
        y.var += 1e-6
        # z = y.mean + n * (y.var + MIN_VAR).sqrt()
        #
        self.S_weight.compute_KL()
        return y.sample(bounded=True)

    def forward_AP1(self, x, **kwargs):
        assert not self.training
        return Conv2d.forward_AP1(self, x, **kwargs)

    def forward_AP2(self, x: RandomVar, output_size=None, **kwargs):
        assert kwargs.get('compute_normalization') or kwargs.get('init_normalization')
        # self.new_sample(**kwargs)
        y = RandomVar()
        y.mean = self.conv_reduce(x.mean, self.S_weight.mean, self.bias, output_size=output_size, **kwargs)
        y.var = self.conv_reduce(square(x.mean), self.S_weight.var, output_size=output_size, **kwargs)
        y.var += self.conv_reduce(x.var, self.S_weight.var, output_size=output_size, **kwargs)
        y.var += self.conv_reduce(x.var, square(self.S_weight.mean), output_size=output_size, **kwargs)
        # y.var = self.conv_reduce(x.var, square(self.S_weight.mean), output_size=output_size, **kwargs)
        # Conv2d.forward_AP2(self, x, **kwargs)
        return y


class VariationalDropout_Conv2d(Conv2d):
    def __init__(self, in_channels, out_channels, kernel_size, **kwargs):
        prototype = Conv2d(in_channels, out_channels, kernel_size, **kwargs)
        # make S_weight available in the __dict__ so property weight would work
        
        m_sz = prototype.weight.size()
        var_sz = [1, 1, 1, 1]
        S_weight = StochasticParamNormal_VariationalDropout(torch.ones(m_sz), var_sz=var_sz, var_init=0.1 ** 2, var_prior=1.0)
        # set it to dict so that weight property works before super constructors are called
        object.__setattr__(self, 'S_weight', S_weight)
        # now can initialize Conv2d and Module
        Conv2d.__init__(self, in_channels, out_channels, kernel_size, **kwargs)
        # reassign S_weight to self._modules
        self.S_weight = S_weight
        self.reset_parameters()
        self.sample_used = True
    
    @property
    def weight(self):
        return self.S_weight.current
    
    @weight.setter
    def weight(self, value):
        self.S_weight.current = value
    
    def new_sample(self, **kwargs):
        if self.training and (kwargs.get('compute_normalization') or self.sample_used):
            self.S_weight.new_sample()
        if self.training and kwargs.get('compute_normalization'):
            self.sample_used = False  # normalization is not the final consumer of the sample
        else:
            self.sample_used = True
    
    def forward_sample(self, x, **kwargs):
        self.new_sample(**kwargs)
        return Conv2d.forward_AP1(self, x, **kwargs)
    
    def forward_AP1(self, x, **kwargs):
        assert not self.training
        return Conv2d.forward_AP1(self, x, **kwargs)
    
    def forward_AP2(self, x, **kwargs):
        assert kwargs.get('compute_normalization') or kwargs.get('init_normalization')
        self.new_sample(**kwargs)
        return Conv2d.forward_AP2(self, x, **kwargs)


class ConvTranspose2d(ConvBase, nn.ConvTranspose2d):
    """
    Same semantics as torch.nn.ConvTranspose2d
    """
    """weight: (in_channels, out_channels, kernel_size[0], kernel_size[1])"""
    """bias: (out_channels) """
    
    def __init__(self, in_channels, out_channels, kernel_size, **kwargs):
        nn.ConvTranspose2d.__init__(self, in_channels, out_channels, kernel_size, **kwargs)
        ConvBase.__init__(self, in_channels, out_channels)
    
    @property
    def in_c_dim(self):
        return 0
    
    @property
    def out_c_dim(self):
        return 1
    
    def out_size(self, size):
        """ expressions copied from documentation """
        N = size[0]
        H_in = size[2]
        W_in = size[3]
        H_out = (H_in - 1) * self.stride[0] - 2 * self.padding[0] + self.kernel_size[0] + self.output_padding[0]
        W_out = (W_in - 1) * self.stride[1] - 2 * self.padding[1] + self.kernel_size[1] + self.output_padding[1]
        C_out = self.out_channels
        return [N, C_out, max(H_out, 1), max(W_out, 1)]
    
    def conv_reduce(self, x: Tensor, weight, bias=None, output_size=None, **kwargs) -> Tensor:
        if x.size()[2] == 1 and x.size()[3] == 1:  # 1 pixel input
            weight = weight.sum(dim=2, keepdim=True).sum(dim=3, keepdim=True)
            return F.conv_transpose2d(x, weight, bias, groups=self.groups)
        else:
            output_padding = self._output_padding(x, output_size)
            return F.conv_transpose2d(x, weight, bias, self.stride, self.padding, output_padding, self.groups, self.dilation)

    # def forward_AP2(self, x: RandomVar, output_size=None, **kwargs) -> RandomVar:
    #     y = ConvBase.forward_AP2(self,x ,output_size, **kwargs)
    #     y = y.detach()
    #     y.mean.fill_(0.0)
    #     y.var.fill_(1.0)
    #     return y

class BatchNorm(Module):
    def __init__(self, num_features, eps=1e-5, momentum=0.1, **kwargs):
        Module.__init__(self)
        self.num_features = num_features
        self.eps = eps
        self.momentum = momentum
        # self.sb = StochasticScaleBias(input_channels=num_features, **kwargs)
        if kwargs.get('ssb') == True:
            self.sb = SSB(input_channels=num_features, **kwargs)
        elif kwargs.get('ssb') == 'SSB1':
            self.sb = SSB1(input_channels=num_features, **kwargs)
        else: # no SSB
            self.sb = ScaleBias(input_channels=num_features, **kwargs)
        self.initialized = False
        self.register_buffer('running_mean', torch.zeros(num_features))
        self.register_buffer('running_var', torch.ones(num_features))
    
    @property
    def weight(self):
        return self.sb.weight
    
    @property
    def bias(self):
        return self.sb.bias
    
    def reset_parameters(self):
        self.running_mean.zero_()
        self.running_var.data.fill_(1)
    
    def init_normalization_(self, x: Tensor) -> Tensor:
        """ first reccurent pass through the network: discard scale and bias of the precciding layer """
        self.initialized = True
        self.reset_parameters()
        # initialize running mean and var without momentum
        y = F.batch_norm(x, self.running_mean, self.running_var, training=True, momentum=1, eps=self.eps)
        return y
    
    def inject_normalization_(self, x: Tensor) -> Tensor:
        """ first reccurent pass through the network: preserve the scale and bias of the precciding layer """
        xt = x.data.transpose(0, 1).contiguous().view([x.size()[1], -1])  # all dimensions except channels
        m = xt.mean(dim=1)
        v = xt.var(dim=1, unbiased=False)
        self.running_mean = m
        self.running_var = v
        # self.weight.data = torch.sqrt(self.running_var + self.eps).view([-1])
        # self.bias.data = self.running_mean.view([-1])
        
        v = self.running_var + self.eps
        m = self.running_mean
        self.sb.set_affine(torch.sqrt(v).view([-1]), m.view([-1]))
        self.initialized = True
        return x
    
    def init_normalization(self, x, preserve_equivalence):
        if preserve_equivalence:
            return self.inject_normalization_(x)
        else:
            return self.init_normalization_(x)
    
    def forward_AP1(self, x, init_normalization=False, preserve_equivalence=False, **kwargs):
        if init_normalization:
            return self.init_normalization(x, preserve_equivalence)
        else:
            x = F.batch_norm(x, self.running_mean, self.running_var, training=self.training, momentum=self.momentum, eps=self.eps)
            return self.sb.forward_AP1(x, **kwargs)
    
    def forward_sample(self, x: Tensor, init_normalization=False, preserve_equivalence=False, BN_v=None, **kwargs) -> Tensor:
        """ default verion (BN_v) is identical to forward_AP1, other stuff experimental """
        if init_normalization:
            x = self.init_normalization(x, preserve_equivalence)
        else:
            if BN_v == None:
                x = F.batch_norm(x, self.running_mean, self.running_var, training=self.training, momentum=self.momentum, eps=self.eps)
            elif BN_v == 0:  # should reproduce the default implementation
                xf = x.view([x.size()[0], x.size()[1], -1])  # flatten spatial
                Mx = xf.mean(dim=2).view([x.size()[0], x.size()[1], 1, 1])
                Vx = xf.var(dim=2, unbiased=False).view([x.size()[0], x.size()[1], 1, 1])
                M = Mx.mean(dim=0, keepdim=True)
                V = Vx.mean(dim=0, keepdim=True)
                # update runnig mean, variance
                self.running_mean = (1 - self.momentum) * self.running_mean + self.momentum * M.view([-1]).data
                self.running_var = (1 - self.momentum) * self.running_var + self.momentum * V.view([-1]).data
                x = (x - M) / torch.sqrt(V + self.eps)
            
            elif BN_v == 1 or BN_v == 2:  #
                xf = x.view([x.size()[0], x.size()[1], -1])  # flatten spatial
                Mx = xf.mean(dim=2).view([x.size()[0], x.size()[1], 1, 1])
                M2x = square(xf).mean(dim=2).view([x.size()[0], x.size()[1], 1, 1])
                M1 = Mx.mean(dim=0, keepdim=True)
                M2 = M2x.mean(dim=0, keepdim=True)
                V = M2 - square(M1)
                # update runnig mean, variance
                self.running_mean = (1 - self.momentum) * self.running_mean + self.momentum * M1.view([-1]).data
                self.running_var = (1 - self.momentum) * self.running_var + self.momentum * V.view([-1]).data
                
                k = x.size()[0]
                Mc = (k * M1 - Mx) / (k - 1)
                M2c = (k * M2 - M2x) / (k - 1)
                Vc = M2c - square(Mc)
                # assert ((Vc.data >= 0).all())
                x = (x - Mc) / torch.sqrt(Vc + self.eps)
                if BN_v == 2:  # Correct bias as derived from the Gaussian model
                    x = x / (1 + 3 / (4 * (k - 1)))
                # k = x.size()[0]
                # M1 = k / (k - 1) * M - 1 / (k - 1) * Mx
                # V1 = (k * V - Vx)/(k-1) - 1 / k * square(Mx - M1)
                # assert((V1.data >=0).all())
                # x = (x - M1)/torch.sqrt(V1 + self.eps)
        #
        return self.sb.forward_sample(x, **kwargs)
    
    def forward_AP2(self, x: RandomVar, **kwargs):
        """ experimental """
        x = (x - self.running_mean.view([1, -1, 1, 1]).detach() / torch.sqrt(self.running_var.view([1, -1, 1, 1]).detach() + self.eps))
        k = x.size()[0]
        var_add = 1 / k
        var_mult = 1 / (2 * k)
        b = RandomVar(0, var_add).detach().type_as(x)
        s = RandomVar(1, var_mult).detach().type_as(x)
        x = (x + b) * s
        return self.sb.forward_AP2(x, **kwargs)
    
    def total_transform(self) -> ScaleBias:
        """total efficient ScaleBias"""
        t = ScaleBias(self.num_features)
        scale = 1 / torch.sqrt(self.running_var + self.eps).view([-1])
        scale *= self.weight.data.view([-1])
        t.bias.data = -self.running_mean.view([-1]) * scale
        t.bias.data += self.bias.data.view([-1])
        t.weight.data = scale
        return t

        # t = ScaleBias(self.num_features)
        # scale = 1 / torch.sqrt(self.running_var + self.eps).view([-1])
        # t.weight.data = self.sb.weight.data * scale
        # t.bias.data = -self.running_mean.view([-1]) * scale
        # t.bias.data += self.sb.bias.data
        # return t


class LayerNorm(Module):
    def __init__(self, num_features, eps=1e-5, momentum=0.1, **kwargs):
        Module.__init__(self)
        self.num_features = num_features
        self.eps = eps
        self.momentum = momentum
        #
        if kwargs.get('ssb') == True:
            self.sb = SSB(input_channels=num_features, **kwargs)
        elif kwargs.get('ssb') == 'SSB1':
            self.sb = SSB1(input_channels=num_features, **kwargs)
        else:  # no SSB
            self.sb = ScaleBias(input_channels=num_features, **kwargs)
        self.initialized = False
    
    @property
    def weight(self):
        return self.sb.weight
    
    @property
    def bias(self):
        return self.sb.bias
    
    def reset_parameters(self):
        self.running_mean.zero_()
        self.running_var.data.fill_(1)
        
    def normalize(self, x):
        sz = [x.size()[0], x.size()[1]]
        m = x.view(sz + [-1]).mean(dim=2).view(sz + [1,1])
        v = x.view(sz + [-1]).var(dim=2).view(sz + [1,1]) + self.eps
        y = (x - m) / torch.sqrt(v)
        return y
    
    def init_normalization_(self, x: Tensor) -> Tensor:
        """ first reccurent pass through the network: discard scale and bias of the precciding layer """
        self.initialized = True
        self.reset_parameters()
        y = self.normalize(x)
        return y
    
    def inject_normalization_(self, x: Tensor) -> Tensor:
        raise NotImplementedError
    
    def init_normalization(self, x, preserve_equivalence):
        if preserve_equivalence:
            return self.inject_normalization_(x)
        else:
            return self.init_normalization_(x)
    
    def forward_AP1(self, x, init_normalization=False, preserve_equivalence=False, **kwargs):
        if init_normalization:
            return self.init_normalization(x, preserve_equivalence)
        else:
            x = self.normalize(x)
            return self.sb.forward_AP1(x, **kwargs)
    
    def forward_AP2(self, x: RandomVar, **kwargs):
        """ experimental """
        y = RandomVar(0.0, 1.0).type_as(x.mean).dims_as(x)
        return self.sb.forward_AP2(y, **kwargs)
    
    def total_transform(self) -> ScaleBias:
        raise NotImplementedError


class VNorm(Module):
    """
        the layer  will copmute apply and differentiate normalization, depending on passed parameters, the default is to do nothing
    """
    
    def __init__(self, input_channels, eps=1e-16, affine=True, **kwargs):
        Module.__init__(self)
        # this will hold normalization statistics, can be recomputed, but is saved for consistency
        self.register_buffer('norm', RandomVar())
        #
        self.affine = affine
        if kwargs.get('ssb') == True:
            self.sb = SSB(input_channels=input_channels, **kwargs)
        elif kwargs.get('ssb') == 'SSB1':
            self.sb = SSB1(input_channels=input_channels, **kwargs)
        else: # no SSB
            self.sb = ScaleBias(input_channels=input_channels, **kwargs)
        self.input_channels = input_channels
        self.eps = eps
        self.register_buffer('expected_mean', Tensor(input_channels))
        self.register_buffer('expected_var', Tensor(input_channels))
        self.momentum = 0.95
        self.args = odict(**kwargs)
        self.reset_parameters()
        
    def upgrade(self, **kwargs):
        if not hasattr(self,'args'):
            self.args = odict(**kwargs)
    
    def normalization_computed(self):
        """check normalization Tensors exist"""
        return self.norm.mean is not None and self.norm.var is not None
    
    def invalidate(self):
        self.norm.mean = None
        self.norm.var = None
    
    initialized = property(normalization_computed)
    
    def reset_parameters(self):
        self.sb.reset_parameters()
        self.expected_mean.data.fill_(0.0)
        self.expected_var.data.fill_(1.0)
    
    def total_transform(self) -> ScaleBias:
        """total efficient ScaleBias t such that (x-norm.mean/sqrt(norm.var))*s + b = x*t.s + t.b """
        assert self.normalization_computed()
        t = ScaleBias(self.input_channels)
        scale = 1 / (torch.sqrt(self.norm.var.data.view([-1])))
        if self.affine:
            aw, ab = self.sb.get_affine()
            scale *= aw.data
        t.weight.data = scale
        t.bias.data = -self.norm.mean.data.view([-1]) * scale
        if self.affine:
            t.bias.data += ab.data
        return t
    
    def forward_norm(self, x: Tensor, compute_normalization=False, **kwargs) -> Tensor:
        assert compute_normalization is False
        assert self.normalization_computed()
        if self.training:
            norm = self.norm
        else:
            norm = self.norm.detach()
            # norm = RandomVar(self.expected_mean, self.expected_var).view([1,-1,1,1])
        # apply
        if hasattr(self, 'args') and self.args.norm_bias:
            alpha = 1.0 / 32
            mx = x.view([x.size(0), x.size(1), -1]).mean(dim=2).view([x.size(0), x.size(1), 1, 1])  # mean over spatial dimensions only
            m2x = (x.view([x.size(0), x.size(1), -1])**2).mean(dim=2).view([x.size(0), x.size(1), 1, 1])  # mean over spatial dimensions only
            m2 = norm.var + norm.mean**2
            mean = norm.mean * (1 - alpha) + mx * alpha
            m2 = m2 * (1 - alpha) + m2x * alpha
            var = m2 - mean**2
            # var = norm.var * (1 - alpha) + v * alpha
        else:
            mean = norm.mean
            var = norm.var
        y = (x - mean) / torch.sqrt(var)
        
        if self.training:
            s = 1.0 / var.detach().sqrt()
            self.expected_var = self.momentum * self.expected_var + (1 - self.momentum) * s.mean(dim=0, keepdim=True).view([-1]).data
            delta = mean.detach() * s
            self.expected_mean = self.momentum * self.expected_mean + (1 - self.momentum) * delta.mean(dim=0, keepdim=True).view([-1]).data

        
        # if x.dim()==2:
        #     y = (x - norm.mean) / torch.sqrt(norm.var)
        # else:
        #     # if True:
        #     #     # biased normalization estimate using the current sample
        #     #     m = x.view([x.size(0), x.size(1), -1]).mean(dim=2).view([x.size(0), x.size(1), 1, 1])
        #     #     alpha = 1/10
        #     #     bm = norm.mean * (1 - alpha) + m * alpha
        #     #     bv = norm.var * (1 - alpha) + square(m - bm) * alpha
        #     #     y = (x - bm) / torch.sqrt(bv)
        #     # # else:
        #     # y = (x - norm.mean) / torch.sqrt(norm.var)
        return y
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        # y = self.forward_norm(x, **kwargs)
        # this is currently part of normalization
        # assert not self.training
        if not self.training and (isinstance(self.sb, SSB) or isinstance(self.sb, SSB1)) and not kwargs.get('no_expected_norm'):
            # norm = RandomVar(self.expected_mean, self.expected_var).view([1, -1, 1, 1]).detach()
            # y = x / norm.var.sqrt() - norm.mean
            # y = x * norm.var - norm.mean
            s = self.expected_var.view([1, -1, 1, 1])
            b = self.expected_mean.view([1, -1, 1, 1])
            # s = 1.0 / self.norm.var.detach().sqrt()
            # b = self.norm.mean.detach() / self.norm.var.detach().sqrt()
            # b = self.norm.mean.detach() * s # kid of variance reduction
            # b = self.norm.mean.detach() + self.expected_mean.view([1, -1, 1, 1])  # kid of variance reduction
            # b = self.expected_mean.view([1, -1, 1, 1])  # kid of variance reduction
            # b = self.norm.mean.detach()/self.norm.var.detach().sqrt() + self.expected_mean.view([1, -1, 1, 1])  # kid of variance reduction
            y = x * s - b
        else:
            if self.training:
                norm = self.norm
            else:
                norm = self.norm.detach()

            if self.args.norm_bias:
                # alpha = 1.0 / 32
                # m = x.view([x.size(0), x.size(1), -1]).mean(dim=2).view([x.size(0), x.size(1), 1, 1]) # mean over spatial dimensions only
                # # v = x.view([x.size(0), x.size(1), -1]).var(dim=2).view([x.size(0), x.size(1), 1, 1])  # var over spatial dimensions only
                # mean = norm.mean * (1 - alpha) + m * alpha
                # # var = norm.var * (1 - alpha) + v * alpha
                # var = norm.var
                alpha = 1.0 / 32
                mx = x.view([x.size(0), x.size(1), -1]).mean(dim=2).view([x.size(0), x.size(1), 1, 1])  # mean over spatial dimensions only
                m2x = (x.view([x.size(0), x.size(1), -1]) ** 2).mean(dim=2).view([x.size(0), x.size(1), 1, 1])  # mean over spatial dimensions only
                m2 = norm.var + norm.mean ** 2
                mean = norm.mean * (1 - alpha) + mx * alpha
                m2 = m2 * (1 - alpha) + m2x * alpha
                var = m2 - mean ** 2
                y = (x - mean) / torch.sqrt(var)
            else:
                y = (x - norm.mean) / torch.sqrt(norm.var)
        #
        if self.affine:
            y = self.sb.forward_AP1(y, **kwargs)
        return y
    
    def forward_sample(self, x: Tensor, **kwargs) -> Tensor:
        y = self.forward_norm(x, **kwargs)
        if self.affine:
            y = self.sb.forward_sample(y, **kwargs)
        return y
    #
    
    def compute_normalization_(self, x: RandomVar, **kwargs) -> RandomVar:
        # hack
        # if not self.training:
        self.norm.mean = x.mean
        self.norm.var = x.var + self.eps
        
        # we know the normalized input is (0,1)
        # expectation over noises
        # if self.training:
        #     s = 1.0 / x.var.detach().sqrt()
        #     # self.expected_var = self.momentum * self.expected_var + (1 - self.momentum) * self.norm.var.mean(dim=0, keepdim=True).view([-1]).data
        #     self.expected_var = self.momentum * self.expected_var + (1 - self.momentum) * s.mean(dim=0, keepdim=True).view([-1]).data
        #     # delta = (x.mean.detach() - self.norm.mean.detach().view([1, -1, 1, 1]))
        #     # delta = x.mean.detach()
        #     # delta = x.mean.detach() * s - self.norm.mean.detach() / self.norm.var.detach().sqrt()
        #     delta = x.mean.detach() * s
        #     self.expected_mean = self.momentum * self.expected_mean + (1 - self.momentum) * delta.mean(dim=0, keepdim=True).view([-1]).data
        
        # assert (self.expected_var > 0).all()
        #
        y = RandomVar(0.0, 1.0).type_as(x.mean).dims_as(x)
        y = y.expand(x.size()) # keep desired dimensions
        if self.affine:
            y = self.sb.forward_AP2(y, **kwargs)
        return y
    
    def init_normalization_(self, x: RandomVar, **kwargs) -> RandomVar:
        self.sb.reset_parameters()
        return self.compute_normalization_(x, **kwargs)
    
    def inject_normalization_(self, x: RandomVar, **kwargs) -> RandomVar:
        assert self.affine, 'cannot preserve equivalence without affine transfrom'
        self.norm.mean = x.mean
        self.norm.var = x.var + self.eps
        v = self.norm.var.data
        m = x.mean.data
        if v.dim() == 4:
            assert v.size()[2] == 1 or v.stride()[2] == 0
            assert v.size()[3] == 1 or v.stride()[3] == 0
            v = v[:, :, 0, 0]
            m = m[:, :, 0, 0]
        #self.sb.weight = torch.sqrt(v).view([-1])
        #self.sb.bias = m.view([-1])
        self.sb.set_affine(torch.sqrt(v).view([-1]), m.view([-1]))
        return x
    
    def init_normalization(self, x, preserve_equivalence, **kwargs):
        assert x.size()[0] == 1, 'normalization input should be a single sample with dataset statisitcs'
        assert x.dim() == 2 or (x.size()[2] == 1 and x.size()[3] == 1)
        if preserve_equivalence:
            y = self.inject_normalization_(x, **kwargs)
        else:
            y = self.init_normalization_(x, **kwargs)
        var = self.norm.var
        mean = self.norm.mean
        s = 1.0 / var.detach().sqrt()
        self.expected_var = s.mean(dim=0, keepdim=True).view([-1]).data
        delta = mean.detach() * s
        self.expected_mean = delta.mean(dim=0, keepdim=True).view([-1]).data
        # self.expected_mean = self.norm.mean.view([-1]).data
        # self.expected_var = self.norm.var.view([-1]).data
        return y
    
    def forward_AP2(self, x: RandomVar, init_normalization=False, compute_normalization=False, preserve_equivalence=False, **kwargs) -> RandomVar:
        """
        compute_normalization:  if True will compute normalization from current input
        """
        if init_normalization:
            return self.init_normalization(x, preserve_equivalence=preserve_equivalence, init_normalization=init_normalization, compute_normalization=compute_normalization, **kwargs)
        
        if compute_normalization:
            return self.compute_normalization_(x, **kwargs, init_normalization=init_normalization, compute_normalization=compute_normalization)
        
        # inference or training pass with real data
        assert self.normalization_computed()
        if self.training:
            norm = self.norm
        else:
            norm = self.norm.detach()  # don't backprop in this run
        # apply normalization
        y = RandomVar()
        y.mean = (x.mean - norm.mean) / torch.sqrt(norm.var)
        y.var = x.var / norm.var
        #
        if self.affine:
            y = self.sb.forward_AP2(y, **kwargs)
        return y
    
    def __repr__(self):
        tmpstr = '({}) '.format(self.__class__.__name__)
        tmpstr += ('mean:[{:.2g},{:.2g}] '.format(self.norm.mean.min().item(), self.norm.mean.max().item()) )
        tmpstr += ('var:[{:.2g},{:.2g}] '.format(self.norm.var.min().item(), self.norm.var.max().item()))
        tmpstr += super().__repr__()
        return tmpstr


class WeightNorm(Module):
    def __init__(self, out_channels, linear=None, **kwargs):
        assert linear is not None
        Module.__init__(self)
        self.input_channels = linear.get_out_channels()
        # self.sb = ScaleBias(self.input_channels)
        if kwargs.get('ssb') == True:
            self.sb = SSB(input_channels=self.input_channels, **kwargs)
        elif kwargs.get('ssb') == 'SSB1':
            self.sb = SSB1(input_channels=self.input_channels, **kwargs)
        else:  # no SSB
            self.sb = ScaleBias(input_channels=self.input_channels, **kwargs)
        self.initialized = False
        self.__dict__['linear'] = linear
    
    def total_transform(self):
        """total efficient ScaleBias"""
        t = ScaleBias(self.input_channels)
        t.weight.data = self.sb.weight.data / self.linear.w_norm().data.view([-1])
        t.bias.data = self.sb.bias.data.clone()
        return t
    
    def inject_normalization_(self, x: RandomVar) -> RandomVar:
        s = self.linear.w_norm()
        b = self.linear.bias
        # self.sb.weight.data = s.data.view(self.sb.weight.size())
        # self.sb.bias.data = b.data.view(self.sb.bias.size())
        self.sb.set_affine(s.view([-1]), b.view([-1]))
        self.linear.project()
        # self.linear.bias = None
        self.initialized = True
        return x
    
    def forward(self, x, **kwargs):
        if not self.initialized:
            return self.inject_normalization_(x)
        else:
            # w = self.linear.w_norm().view(self.sb.weight.size())
            # w = w.view(list(w.size()) + [1] * (x.dim() - w.dim()))
            w = self.linear.w_norm().view([1, -1, 1, 1])
            return self.sb.forward(x / w, **kwargs)

class LinearNorm(Module):
    def __init__(self, linear=None, norm_layer=None, **kwargs):
        Module.__init__(self)
        self.linear = linear
        self.norm = norm_layer
    
    def out_size(self, size):
        size = self.linear.out_size(size)
        if self.norm is not None:
            size = self.norm.out_size(size)
        return size
    
    def upgrade(self, **kwargs):
        for layer in [self.linear, self.norm]:
            if hasattr(layer,'upgrade'):
                layer.upgrade(**kwargs)
    
    def forward(self, x, **kwargs):
        x = self.linear.forward(x, **kwargs)

        # if isinstance(x, Tensor):
        #     g = torch.autograd.grad(x[0, 0, 100, 100], kwargs['net'][0].norm.sb.S, retain_graph=True, allow_unused=True)[0]
        #     if g is not None:
        #         print('linear grad=[{:3g}, {:3g}]'.format(g.min().item(), g.max().item()))
        
        if isinstance(x, RandomVar):
            check_var(x.var)
        if self.norm is not None:
            x = self.norm.forward(x, **kwargs)

            # if isinstance(x, Tensor):
            #     g = torch.autograd.grad(x[0, 0, 100, 100], kwargs['net'][0].norm.sb.S, retain_graph=True, allow_unused=True)[0]
            #     if g is not None:
            #         print('norm grad=[{:3g}, {:3g}]'.format(g.min().item(), g.max().item()))
        return x
    
    def remove_norm(self):
        if self.norm is not None:
            t = self.norm.total_transform()
            self.linear.merge_after(t)
            self.norm = None
    
    # @property
    def get_out_channels(self) -> int or None:
        return self.linear.get_out_channels()


class Dropout(Module):
    """
        (Scaled) Dropout layer: Y = X*Z/p , Z is Bernoulli with p
        AP1: propagate mean
        AP2: propagate mean and var
        sample: sample Z
    """
    
    def __init__(self, q: float, dropout_noise='bernoulli'):
        """ q is probability to drop (to zero out) """
        Module.__init__(self)
        assert q < 1, 'cannot drop 100% of units'
        self.p = 1 - q
        self.dropout_noise = dropout_noise
        self.var = (1 - self.p) / self.p
        self.mean = 1
    
    def forward_AP1(self, x: Tensor, **kwargs) -> Tensor:
        return x
    
    def forward_AP2(self, x: RandomVar, **kwargs) -> RandomVar:
        if self.p == 1:  # pass all
            return x
        else:
            return x * RandomVar(x.mean.data.new([1]), x.mean.data.new([self.var]))
    
    def forward_sample(self, x: Tensor, **kwargs) -> Tensor:
        if self.p == 1:  # pass all
            return x
        else:
            if self.dropout_noise == 'bernoulli':
                u = x.data.new(x.size())
                u.uniform_()
                z = ((u <= self.p).float() / self.p).detach()  # bernoulli: 1/p with p, mean 1
                return x * z
            elif self.dropout_noise == 'normal':
                n = x.data.new(x.size())
                n.normal_()
                z = (n * math.sqrt(self.var) + 1).detach()  # Normal(1, var)
                return x * z
            else:
                raise ValueError()
