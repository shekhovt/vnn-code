from .layers import *

from torchvision import datasets, transforms
import time
import inspect
import os
import numpy as np
import argparse
from .data_loader import *
import torch.utils.data
import traceback
import sys
import math
from .utils import *
import scipy.optimize
import traceback, sys, code

class AddNoiseTransform(object):
    def __init__(self, var):
        self.var = var
    
    def __call__(self, x):
        if self.var > 0:
            return x + torch.randn(x.size()) * math.sqrt(self.var)
        else:
            return x

class TrainingState:
    def __init__(self, model_class, train_dataset, test_dataset, args, var_args):
        self.model_class = model_class
        self.model = None
        self.optimizer = None
        self.reg_optimizer = None
        self.normalization = None
        self.init = None
        self.inference = None
        # self.loss = None
        self.epoch = 0
        #
        var_args = var_args.copy()
        #
        # remove var_args values matching the default:
        for k, v in var_args.copy().items():
            if k in args and args[k] == v:
                del var_args[k]
        #
        # append var_args to args
        args = odict(args, **var_args)  # overwrite duplicates with var_args
        #
        if args.project and args.norm is None:
            args.project = False
            del var_args.project
        
        if 'epochs' in var_args:
            args.epochs = var_args.epochs
            del var_args.epochs

        if args.reg is None:
            if 'reg_c' in var_args:
                del var_args.reg_c
            del args.reg_c
        
        ##
        self.construct_args = args.copy()  # deep copy
        self.args = args.copy()  # deep copy
        self.var_args = var_args.copy()  # deep copy
        #
        self.train_dataset = train_dataset
        self.test_dataset = test_dataset
        #
        self.args_s = self.__args_to_s(self.args)
        self.var_args_s = self.__args_to_s(self.var_args)
        self.path = self.args.path + self.var_args_s + '/'
        #
        self.model_name = model_class.__name__
        self.model_var_name = model_class.__name__ + self.var_args_s
        #
        self.reset_()
        
    def upgrade(self, **kwargs):
        """ reinitialize new parameters """
        self.model.upgrade(**kwargs)
        if kwargs.get('new_optimizer'):
            self.new_optimizer()
    
    def reset_(self):
        self.loss_stat = None
        # history record
        self.t_loss = []
        self.t_acc = []
        self.v_loss = []
        self.v_acc = []
        self.loss_mean = []
        self.loss_var = []
        self.min_v_loss = 1e10
        self.min_v_loss_i = -1
        self.log_interval = None
        self.it_hist = odict()
        self.it_hist.loss = []
        self.it_hist.ll = [] # all losses (e.g KL, etc.)
        self.it_hist.llm = []
        self.info = odict()
        self.info.it_hist = self.it_hist
        self.info.construct_args = self.construct_args
        self.info.args = self.args
        self.info.var_args = self.var_args
        self.regfactor = 1.0
    
    def log(self, *args):
        force_path(self.path)
        if self.args.verbose:
            print(*args)
        with open(self.path + 'output.txt', 'a') as f:
            print(*args, file=f)
    
    def create_model(self):
        self.init = self.args.init
        self.normalization = self.args.norm
        self.inference = self.args.inference
        self.model = self.model_class(args = self.args, data_mean=self.data_mean, data_var=self.data_var, **self.args)
        self.model.reset_parameters(self.args.weight_init)
        if self.args.cuda:
            self.model.cuda()
    
    def init_model(self):
        (data, target) = next(iter(self.train_loader))
        while data.size()[0] < self.args.init_batch_size:
            x, _ = next(iter(self.train_loader))
            data = torch.cat((data, x), 0)
        if data.size()[0] > self.args.init_batch_size:
            data = data[0:self.args.init_batch_size-1]

        # insert normalization for init
        self.model.eval()
        if self.init is not None:
            self.log('Initializing with {}'.format(self.init))
            self.model.set_normalization(self.init, **self.args)  # going to insert norm layers and then remove them
            # init normalizaiton
            if self.init == 'AP2':
                self.model.init_normalization_AP2(data.size(),preserve_equivalence=False)
            elif self.init == 'BN':
                self.model.init_normalization_BN(data, preserve_equivalence=False)
            # # project out invariances
            
            # if self.args.project:
            #     self.model.normalization_valid = False
            #     # y1 = self.model.forward(data)
            #     # self.model.project() # not sure why we need this bit, does not go well with BN train / test somehow
            #     self.model.normalization_valid = False
            #     # y2 = self.model.forward(data)
            #     # check invariance
            #     # assert ((y1-y2).abs() < 1e-3).all()

        y1 = self.model.forward(data, method='AP1')

        if self.args.get('reg_like_BN'):
            self.log('With noises like BN')
            self.init_norm_fixed_noise(data)
        else:
            # remove normalization if was different from init and reinsert a new one
            if self.normalization != self.init:
                    # self.model.remove_normalization()
                    # self.model.normalization_valid = False
                    # y3 = self.model.forward(data)
                    # check invariance
                    # assert ((y1 - y3).abs() < 1e-3).all()
                    # add new normalization
                    self.model.set_normalization(self.normalization, **self.args)  # insert norm layers
                    # init
                    if self.normalization == 'AP2':
                        self.model.init_normalization_AP2(data.size(), preserve_equivalence=True)
                    elif self.normalization == 'BN':
                        self.model.init_normalization_BN(data, preserve_equivalence=True)
                    elif self.normalization == 'Weight':
                        self.model.forward(data, preserve_equivalence=True)
                        # this one does not need init, is equivalence preserving
                        pass
                    y2 = self.model.forward(data, method='AP1')
                    # check invariance
                    assert ((y1-y2).abs() < 1e-3).all()

        # project
        if self.normalization != None and self.args.project:
            self.model.project()
            self.model.normalization_valid = False

        
        if self.normalization is not None:
            self.log('Normalizing with {}'.format(self.normalization))
        self.log(self.model)
        if self.args.cuda:
            self.model.cuda()
    
    def new_model(self):
        # create model
        self.reset_()
        #
        torch.set_num_threads(1)  # don't mess up
        torch.manual_seed(self.args.seed)
        # loaders
        self.train_loader, self.val_loader = get_train_valid_loader(self.train_dataset, batch_size=self.args.batch_size, random_seed=self.args.val_seed,
                                                                    valid_size=self.args.valid_size, shuffle=True, num_workers=self.args.num_workers, pin_memory=False)
        self.test_loader = torch.utils.data.DataLoader(self.test_dataset, batch_size=self.args.batch_size, shuffle=True, num_workers=self.args.num_workers, pin_memory=False)
        
        # traiing dataset statistics
        self.data_stats()
        # create model
        self.args.softmax_simplified = (self.args.softmax == 'simplified')
        
        torch.manual_seed(self.args.seed)
        if self.args.cuda:
            torch.cuda.manual_seed(self.args.seed)
        
        self.create_model()
        self.init_model()
        self.new_optimizer()
        #
        # pr = set(self.model.reg_parameters())
        # p1 = set(self.model.parameters) - pr
        
        
        # self.reg_optimizer = torch.optim.Adam(pp, lr=1)
        
        # self.optimizer = AdamConstLength(self.model.parameters(), lr=self.args.lr, weight_decay=self.args.weight_decay)
        # self.optimizer = torch.optim.ASGD(self.model.parameters(), lr=self.args.lr)
        # self.optimizer = TestSGD(self.model.parameters(), lr=self.args.lr)

        # scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=[lambda1, lambda2])
        #
        # if self.args.lr_down == 'const':
        #     lr_lambda = lambda epoch: self.args.lr  # constant learning rate
        # elif self.args.lr_down == 'sqrt':
        #     lr_lambda = lambda epoch: self.args.lr / math.sqrt((self.epoch + 1))
        # elif self.args.lr_down == 'exp':
        #     lr_lambda = lambda epoch: self.args.lr * math.pow(self.args.lr_exp_base, self.epoch)  # factor 0.1 in 50 epochs and factor 0.01 in 100 epochs
        # else:
        #     raise ValueError('unknown args.lr_down {}'.format(self.args.lr_down))
        # self.log('Learning rate: {}'.format(lr))
        # #  set optimizer lr value
        # for group in self.optimizer.param_groups:
        #     group['lr'] = lr
        
    def new_optimizer(self):
        if self.args.optimizer == 'Adam':
            self.optimizer = torch.optim.Adam(self.model.parameters(), lr=1, weight_decay=self.args.weight_decay)
        elif self.args.optimizer == 'SGD':
            # groups = [dict(params=p1, lr=1, momentum=self.args.SGD_momentum, nesterov=self.args.SGD_Nesterov),
            #           dict(params=pr, lr=1, momentum=self.args.SGD_momentum, nesterov=self.args.SGD_Nesterov)]
            self.optimizer = torch.optim.SGD(self.model.parameters(), lr=1, weight_decay=self.args.weight_decay, momentum= self.args.SGD_momentum, nesterov=self.args.SGD_Nesterov)
        elif self.args.optimizer == 'SGD1':
            self.optimizer = vnn.optim.SGD1(self.model.parameters(), lr=1, weight_decay=self.args.weight_decay, momentum=self.args.SGD_momentum, nesterov=False)
        elif self.args.optimizer == 'SGD2':
            self.optimizer = vnn.optim.SGD2(self.model.parameters(), lr=1, weight_decay=self.args.weight_decay, momentum=self.args.SGD_momentum, nesterov=False)
        else:
            raise ValueError()
        # self.reg_optimizer = torch.optim.SGD(self.model.reg_parameters(), lr=1, weight_decay=0, momentum=0, nesterov=False)
        pp = list(self.model.approx_params())
        if len(pp) > 0:
            self.reg_optimizer = torch.optim.SGD(pp, lr=0.1, momentum=0.9, nesterov=True)
    
    def __args_to_s(self, args):
        ss = ''
        # for arg in vars(args):
        #      ss = ss + '--{}'.format(arg)
        #      ss = ss + '={}'.format(getattr(args, arg))
        # return ss
        # python 3.5 dict insert is instable, sort manually
        # ll = sorted(args.__dict__)
        if 'name' in args:
            return args['name']
        else:
            ll = sorted(args)
            if 'mtype' in set(ll):
                ll = ['mtype'] + sorted(set(ll) - {'mtype'})
            for key in ll:
                ss = ss + '--{}'.format(key)
                ss = ss + '={}'.format(args[key])
            return ss
    
    def args_to_s(self, args):
        return self.__args_to_s(args)
    
    def data_stats(self):
        if hasattr(self.train_loader.dataset, 'processed_folder'):
            f = self.train_loader.dataset.processed_folder
        elif hasattr(self.train_loader.dataset, 'base_folder'):
            f = self.train_loader.dataset.base_folder
        else:
            f = ''
        stat_file = self.train_loader.dataset.root + '/' + f + '/' + 'stats.npz'
        if os.path.exists(stat_file):
            self.log('Loading training dataset statisitcs')
            self.log(stat_file)
            data = np.load(stat_file)
            self.n_train_data = data['n_train_data']
            if isinstance(self.n_train_data, np.ndarray):
                self.n_train_data = int(self.n_train_data)
            train_mean = data['train_mean']
            train_var = data['train_var']
        else:
            self.log('Computing training dataset statisitcs')
            m1 = 0.0  # running mean
            m2 = 0.0  # running 2nd moment
            n = 0  # batches
            self.n_train_data = 0  # dataset size
            for batch_idx, (data, target) in enumerate(self.train_loader):
                self.n_train_data += data.size()[0]
                data = data.cpu().numpy()
                m1 += np.mean(data, axis=0, keepdims=True)
                m2 += np.mean(np.square(data), axis=0, keepdims=True)
                n += 1
            train_mean = m1 / n  # per pixel
            train_var = m2 / n - np.square(train_mean)
            #train_var = np.mean(train_var)  # avera over all pixesl
            # self.train_var = np.max(self.train_var)  # max over pixels
            # self.train_var = np.array([1])
            self.log("Training data statistics: mean = ", np.mean(train_mean), "var=", np.mean(train_var))
            np.savez(stat_file, train_mean=train_mean, train_var=train_var, n_train_data=self.n_train_data)
        
        self.data_mean = torch.from_numpy(train_mean)
        self.data_var = np.asscalar(np.mean(train_var))
        n_batches = (self.n_train_data // self.args.batch_size)
        self.log_interval = max(n_batches // 10, 1)
        self.loss_stat = RunningStat(pow(0.01, 1 / n_batches))
        self.ll_stat = []
    
    
    def train_BN_large_batch(self):
        iter = enumerate(self.train_loader)
        data_load_time = 0
        compute_time = 0
        final = False
        while True:
            self.model.train()  # training mode on
            start_time = time.time()
            DATA = []
            TARGET = []
            ID = []
            # read ahead many batches to usee all of them for normalization
            chunks = self.args.BN_batch_size // self.args.batch_size
            for local_batch_idx in range(chunks):
                try:
                    id, (data, target) = next(iter)
                    if self.args.cuda:
                        data, target = data.cuda(), target.cuda()
                    DATA.append(data)
                    TARGET.append(target)
                    ID.append(id)
                except StopIteration:
                    final = True
                    break
            data_load_time += time.time() - start_time
            if len(ID)==0:
                return
            # concatenate all data
            data = torch.cat(DATA, dim=0).detach()
            target = torch.cat(TARGET, dim=0).detach()
            # make steps over all batches sequentially
            for local_batch_idx in range(len(ID)):
                start_time = time.time()
                batch_idx = ID[local_batch_idx]
                #
                ll = self.model.train_forward(data, target, **self.args, sample_params=True, select_batch=local_batch_idx)
                #
                for x in ll:
                    if not (x == x).all():
                        raise NumericalProblem('NaN encountered in loss')
                    if not (x.abs() < float('inf')).all():
                        raise NumericalProblem('+-Inf encountered in loss')
                #
                for i, l in enumerate(ll):
                    if len(self.ll_stat) <= i:
                        n_batches = (self.n_train_data // self.args.batch_size)
                        self.ll_stat += [RunningStat(pow(0.01, 1 / n_batches))]
                    if len(self.it_hist.ll) <= i:
                        self.it_hist.ll += [[]]
                    if len(self.it_hist.llm) <= i:
                        self.it_hist.llm += [[]]
                    self.ll_stat[i].update(l.data.item())
                    self.it_hist.ll[i] += [l.data.item()]
                    self.it_hist.llm[i] += [self.ll_stat[i].mean]
                    
                t_loss = ll[0]
                #
                self.optimizer.zero_grad()
                t_loss.backward(retain_graph=False)
                self.optimizer.step()
                # loss running statistics
                l = ll[0].data.item()
                self.loss_stat.update(l)
                self.it_hist.loss += [l]
                #
                self.model.eval()
                if self.args.project:
                    self.model.project()
                #
                compute_time += time.time() - start_time
            
                if batch_idx % self.log_interval == 0:
                    batch_size = self.args.batch_size
                    if self.args.verbose:
                        s = 'Train Epoch: {} [{}/{} ({:.0f}%)]'.format(self.epoch, (batch_idx + 1) * batch_size, self.n_train_data,
                                                                       100. * (batch_idx + 1) * batch_size / self.n_train_data)
                        s += '\tLoss(es): '
                        s += ''.join([' {:.6f}'.format(l.mean) for l in self.ll_stat])
                        s += '  time compute: {:.6f}'.format(compute_time)
                        s += '  prepare: {:.6f}'.format(data_load_time)
                        s += '  full: {:.6f}'.format(compute_time + data_load_time)
                        self.log(s)
                        if len(self.ll_stat) > 1:
                            print('reg loss:{}'.format(self.ll_stat[1].mean * self.n_train_data))
                        data_load_time = 0
                        compute_time = 0
            if final:
                break
        return

    def train_BN_large_batch_reshuffle(self):
        it1 = enumerate(self.train_loader)
        it2 = iter(self.train_loader)
        data_load_time = 0
        compute_time = 0
        while True:
            self.model.train()  # training mode on
            start_time = time.time()
            DATA = []
            TARGET = []
            # read one batch from iter
            try:
                batch_idx, (data, target) = next(it1)
            except StopIteration:
                break
            if self.args.cuda:
                data, target = data.cuda(), target.cuda()
            DATA.append(data)
            TARGET.append(target)
            # read ahead many batches to usee all of them for normalization from it1
            chunks = self.args.BN_batch_size // self.args.batch_size
            for local_batch_idx in range(chunks-1):
                try:
                    data, target = next(it2)
                    if self.args.cuda:
                        data, target = data.cuda(), target.cuda()
                    DATA.append(data)
                    TARGET.append(target)
                except StopIteration:
                    it2 = iter(self.train_loader) # continue cycling
            data_load_time += time.time() - start_time
            # concatenate all data
            data = torch.cat(DATA, dim=0).detach()
            target = torch.cat(TARGET, dim=0).detach()
            # make step on batch 0 loss, normalized using all batches
            local_batch_idx = 0
            start_time = time.time()
            #
            ll = self.model.train_forward(data, target, **self.args, sample_params=True, select_batch=local_batch_idx)
            #
            for x in ll:
                if not (x == x).all():
                    raise NumericalProblem('NaN encountered in loss')
                if not (x.abs() < float('inf')).all():
                    raise NumericalProblem('+-Inf encountered in loss')
            #
            for i, l in enumerate(ll):
                if len(self.ll_stat) <= i:
                    n_batches = (self.n_train_data // self.args.batch_size)
                    self.ll_stat += [RunningStat(pow(0.01, 1 / n_batches))]
                if len(self.it_hist.ll) <= i:
                    self.it_hist.ll += [[]]
                if len(self.it_hist.llm) <= i:
                    self.it_hist.llm += [[]]
                self.ll_stat[i].update(l.data.item())
                self.it_hist.ll[i] += [l.data.item()]
                self.it_hist.llm[i] += [self.ll_stat[i].mean]
        
            t_loss = ll[0]
            #
            self.optimizer.zero_grad()
            t_loss.backward(retain_graph=False)
            self.optimizer.step()
            # loss running statistics
            l = ll[0].data.item()
            self.loss_stat.update(l)
            self.it_hist.loss += [l]
            #
            self.model.eval()
            if self.args.project:
                self.model.project()
            #
            compute_time += time.time() - start_time
        
            if batch_idx % self.log_interval == 0:
                batch_size = self.args.batch_size
                if self.args.verbose:
                    s = 'Train Epoch: {} [{}/{} ({:.0f}%)]'.format(self.epoch, (batch_idx + 1) * batch_size, self.n_train_data,
                                                                   100. * (batch_idx + 1) * batch_size / self.n_train_data)
                    s += '\tLoss(es): '
                    s += ''.join([' {:.6f}'.format(l.mean) for l in self.ll_stat])
                    s += '  time compute: {:.6f}'.format(compute_time)
                    s += '  prepare: {:.6f}'.format(data_load_time)
                    s += '  full: {:.6f}'.format(compute_time + data_load_time)
                    self.log(s)
                    if len(self.ll_stat) > 1:
                        print('reg loss:{}'.format(self.ll_stat[1].mean * self.n_train_data))
                    data_load_time = 0
                    compute_time = 0
        return
    
    def train_epoch(self, max_iters=None):
        if hasattr(self.optimizer,'train'):
            self.optimizer.train()
        self.model.train()  # training mode on
        # schedule down by 1/k  -- in 100 epochs by 2 orders, seems ok
        if self.args.lr_down == 'const':
            lr_factor = 1  # constant learning rate
        elif self.args.lr_down == 'sqrt':
            lr_factor = 1 / math.sqrt((self.epoch + 1))
        elif self.args.lr_down == 'exp':
            lr_factor = math.pow(self.args.lr_exp_base, self.epoch)  # factor 0.1 in 50 epochs and factor 0.01 in 100 epochs
        else:
            raise ValueError('unknown args.lr_down {}'.format(self.args.lr_down))
        self.log('Learning rate: {}'.format(lr_factor*self.args.lr))
        #  set optimizer lr value
        for group in self.optimizer.param_groups:
           group['lr'] = lr_factor * self.args.lr
        #
        if hasattr(self, 'reg_optimizer') and self.reg_optimizer is not None:
            for group in self.reg_optimizer.param_groups:
                group['lr'] = lr_factor * 0.1
        #
        min_time = 1e10
        data_load_time = 1e10
        if self.args.BN_batch_size:
            return self.train_BN_large_batch_reshuffle()
            # return self.train_BN_large_batch()
        #
        iter = enumerate(self.train_loader)
        # for batch_idx, (data, target) in enumerate(self.train_loader):
        data_load_time = 0
        compute_time = 0
        # l1m = vnn.RunningStatAvg()
        while True:
            try:
                self.model.train()  # training mode on
                start_time = time.time()
                batch_idx, (data, target) = next(iter)
                #iter_start_time = time.time()
                if self.args.cuda:
                    data, target = data.cuda(), target.cuda()
                #
                #data_load_time = min(data_load_time, time.time() - start_time)
                data_load_time += time.time() - start_time
                #
                start_time = time.time()
                ll = self.model.train_forward(data, target, **self.args, sample_params = True)
                #
                for x in ll[1:]:
                    if not (x == x).all():
                        raise NumericalProblem('NaN encountered in loss')
                    if not (x.abs() < float('inf')).all():
                        raise NumericalProblem('+-Inf encountered in loss')
                #
                if self.args.get('reg') == 'KL':
                    for l in ll[1:]:
                        l  *= (self.args.reg_c / self.n_train_data)
                if self.args.get('reg') == 'KL_pq':
                    for l in ll[1:]:
                        l  *= self.args.reg_c
                for i,l in enumerate(ll):
                    
                    if len(self.ll_stat) <= i:
                        n_batches = (self.n_train_data // self.args.batch_size)
                        self.ll_stat += [RunningStat(pow(0.01, 1 / n_batches))]
                    if len(self.it_hist.ll) <= i:
                        self.it_hist.ll += [[]]
                    if len(self.it_hist.llm) <= i:
                        self.it_hist.llm += [[]]
                    self.ll_stat[i].update(l.data.item())
                    self.it_hist.ll[i] += [l.data.item()]
                    self.it_hist.llm[i] += [self.ll_stat[i].mean]
                t_loss = ll[0].clone()
                # # t_loss *= 0
                #
                # debug
                # w = self.model[9].linear.S_weight
                # g = torch.autograd.grad(t_loss, w._var, retain_graph=True)[0]
                # print('batch_idx={} |var|={:3g}, grad=[{:3g}, {:3g}]'.format(batch_idx, w.var.max().item(), g.min().item(), g.max().item()))
                # # debug

                # self.model.zero_grad()
                # g = torch.autograd.grad(t_loss, self.model[0].norm.sb.B, retain_graph=True)[0]
                # g = torch.autograd.grad(t_loss, self.model[0].norm.sb.B, retain_graph=True)[0]
                # print('grad=[{:3g}, {:3g}]'.format(g.min().item(), g.max().item()))
                # g = torch.autograd.grad(t_loss, self.model[0].norm.sb.S, retain_graph=True)[0]
                # print('grad=[{:3g}, {:3g}]'.format(g.min().item(), g.max().item()))

                if self.args.get('reg') == 'KL_pq' and hasattr(self, 'reg_optimizer') and self.reg_optimizer is not None:
                    self.reg_optimizer.zero_grad()
                    ll[1].backward(retain_graph=True)
                    self.reg_optimizer.step()
                    if batch_idx == 0:
                        for m in self.model:
                            if isinstance(m, LinearNorm):
                                m = m.linear
                            if isinstance(m, ConvBase):
                                if m.approx_params.logs1 is not None:
                                    s1 = m.approx_params.logs1.exp()
                                    s2 = m.approx_params.logs2.exp()
                                    print("s1: [%4.4f,%4.4f]" % (s1.min().item(), s1.max().item()), end='')
                                    print(" s2: [%4.4f,%4.4f]" % (s2.min().item(), s2.max().item()))
                                elif m.approx_params.corr_logit is not None:
                                    v = F.sigmoid(m.approx_params.corr_logit - 2)
                                    print("corr: [%4.4f,%4.4f]" % (v.min().item(), v.max().item()))
                                elif m.approx_params.v_logit is not None:
                                    v = F.tanh(m.approx_params.v_logit)
                                    print("rank1: [%4.4f,%4.4f]" % (v.min().item(), v.max().item()))
                                else:
                                    v = m.approx_params.logscale.exp()
                                    print("scale: [%4.4f,%4.4f]" % (v.min().item(), v.max().item()))
                #
                # t_loss = 0
                for l in ll[1:]:
                    t_loss += l
                #
                self.optimizer.zero_grad()
                t_loss.backward(retain_graph=False)
                self.optimizer.step()
                # ll[0].backward(retain_graph=True)
                # for group in self.optimizer.param_groups:
                #     group['momentum'] = self.args.SGD_momentum
                # print(self.model.enc1[0].norm.sb.B.grad.norm().item())
                # print(self.model)
                # print(self.model[0].norm.sb.B.grad.norm().item())
                # print(self.model)
                # exit(0)

                # if len(ll) > 1:
                #     self.reg_optimizer.zero_grad()
                #     ll[1].backward()
                #     # self.reg_optimizer.step()
                #     for group in self.optimizer.param_groups:
                #         group['momentum'] = 0
                #     self.optimizer.step()
                #
                #
                # loss running statistics
                l = ll[0].data.item()
                self.loss_stat.update(l)
                self.it_hist.loss += [l]
                # l1m.update(ll[1])
                # l_m1 = beta1 * l_m1 + (1 - beta1) * l
                # l_m2 = beta2 * l_m2 + (1 - beta2) * square(l)
                # #
                # bias_correction1 = 1.0 - beta1 ** (batch_idx + 1)
                # bias_correction2 = 1.0 - beta2 ** (batch_idx + 1)
                # l_m1_u = l_m1 / bias_correction1
                # l_m2_u = l_m2 / bias_correction2
                # l_v = l_m2_u - square(l_m1_u)
                #
                #
                # invalidate normalization, project weights, etc
                #
                for m in self.model.modules():
                    if isinstance(m, VNorm):
                        m.invalidate()
                #
                self.model.eval()
                #
                if self.args.project:
                   self.model.project()
                #
                compute_time += time.time() - start_time
                #min_time = min(elapsed_time, min_time)
                #
                #full_time += time.time() - full_time
                
                if batch_idx % self.log_interval == 0:
                    batch_size = self.args.batch_size
                    if self.args.verbose:
                        s = 'Train Epoch: {} [{}/{} ({:.0f}%)]'.format(self.epoch, (batch_idx + 1) * batch_size, self.n_train_data, 100. * (batch_idx + 1) * batch_size / self.n_train_data)
                        s += '\tLoss(es): '
                        s += ''.join([' {:.6f}'.format(l.mean) for l in self.ll_stat])
                        s += '  time compute: {:.6f}'.format(compute_time)
                        s += '  prepare: {:.6f}'.format(data_load_time)
                        s += '  full: {:.6f}'.format(compute_time + data_load_time)
                        self.log(s)
                        if len(self.ll_stat)>1:
                            print('reg loss:{}'.format(self.ll_stat[1].mean * self.n_train_data))
                        # l1m = vnn.RunningStatAvg()
                        # self.log(('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss(es): ' + ''.join([' {:.6f}' for l in ll]) + '  time/batch: {:.6f}').format(
                        #     self.epoch, (batch_idx + 1) * batch_size, self.n_train_data,
                        #                 100. * (batch_idx + 1) * batch_size / self.n_train_data, *(l.item() for l in ll), min_time))
                        data_load_time = 0
                        compute_time = 0
                if max_iters is not None and batch_idx >= max_iters:
                    break
            except StopIteration:
                # if StopIteration is raised, break from loop
                break
        # self.log(str(self.model))
        # exit(0)
        return
    
    def evaluate(self, loader, std=0, method=None, **kwargs):
        if hasattr(self.optimizer, 'eval'):
            self.optimizer.eval()
        self.model.eval()
        (data, target) = next(iter(self.train_loader))
        sz = data.size()
        ## self.model._compute_normalization_AP2(sz, **kwargs, backprop_normalization = False) # unhack
        correct = 0
        n_total = 0
        odds = 0
        loss = 0
        # if self.args.eval_batch_size is not None:
        #     loader.batch_size = self.args.eval_batch_size
        #
        for data, target in loader:
            n_total += data.size()[0]
            if std > 0:
                data += torch.randn(data.shape) * std  # perturb in the original space
            if self.args.cuda:
                data, target = data.cuda(), target.cuda()
            #self.model[0].noise_var = square(std)
            output = self.model.forward(data, **kwargs, method=method).detach()  # log softmax of classes
            loss += self.model.avg_loss(self.model.map_target(target), output).data.item() * data.size()[0]  # total loss over the batch
            pval, pred = output.data.max(1, keepdim=True)  # get the index of the max log-probability
            # pval, pred = output.data.max(1, keepdim=False)  # get the index of the max log-probability
            n = reduce(operator.mul, list(target.size())[1:], 1)
            correct += pred.view(target.size()).eq(target).sum().item() / n
            pval = pval.cpu().numpy()
            ntarg = target.cpu().numpy()
            oval = output.data.cpu().numpy()
            
            # select output value on target class
            if len(oval.shape) ==2:
                tval = oval[np.arange(oval.shape[0]), ntarg]
                tval = np.squeeze(tval)
                pval = np.squeeze(pval)
                rel = pval - tval
                odds += np.sum(rel)
            
            #
        # loader.batch_size = self.args.batch_size
        acc = correct / n_total
        loss /= n_total
        odds /= n_total
        
        return loss, acc, odds, n_total

    def batch_statistics(self):
        net = self.model
        net.train()
        loader = self.train_loader
        kwargs = dict(method='sample', backprop_normalization=True)
        # for BN layers collect mean and variance of B and S
        m = [RunningStatAvg() for i in range(len(net))]
        v = [RunningStatAvg() for i in range(len(net))]
        Bstat = [RunningStatAvg() for i in range(len(net))]
        Sstat = [RunningStatAvg() for i in range(len(net))]
        # first, noise-free mean and variance, full training set
        batch = 0
        for x, target in loader:
            x = x.cuda()
            batch += 1
            if batch > 1000:
                break
            for (i, layer) in enumerate(net):
                if isinstance(layer, LinearNorm):
                    x1 = layer.linear.forward(x, **kwargs)
                    xt = x1.data.transpose(0, 1).contiguous().view([x1.size()[1], -1])  # all dimensions except channels in dim 1, channels moved to dim 0
                    M = xt.mean(dim=1).data  # channel mean
                    V = xt.var(dim=1, unbiased=False).data + layer.norm.eps  # channel variance
                    m[i].update(M.view([1, -1, 1, 1]))
                    v[i].update(V.view([1, -1, 1, 1]))
                x = layer.forward(x, **kwargs)
        for (i, layer) in enumerate(net):
            m[i] = m[i].mean
            v[i] = v[i].mean
        # now, normalized noises ( . + (M -m)/v**0.5 ) * v**0.5/S = (. + B)*S
        batch = 0
        for x, target in loader:
            x = x.cuda()
            batch += 1
            if batch > 1000:
                break
            for (i, layer) in enumerate(net):
                if isinstance(layer, LinearNorm):
                    x1 = layer.linear.forward(x, **kwargs)
                    # shape1 = [x1.size()[0], x1.size()[1], -1]
                    # shape2 = [x1.size()[0], x1.size()[1], 1, 1]
                    # M = x1.view(shape1).mean(dim=2).view(shape2).data # spatial dimensions
                    # V = x1.view(shape1).var(dim=2, unbiased=False).view(shape2).data + layer.norm.eps  # spatial dimensions
                    xt = x1.data.transpose(0, 1).contiguous().view([x1.size()[1], -1])  # all dimensions except channels in dim 1, channels moved to dim 0
                    M = xt.mean(dim=1).data.view([1, -1, 1, 1])  # channel mean
                    V = xt.var(dim=1, unbiased=False).data.view([1, -1, 1, 1]) + layer.norm.eps  # channel variance
                    #
                    B = (M - m[i]) / v[i].sqrt()
                    S = v[i].sqrt() / V.sqrt()
                    #
                    Bstat[i].update(B)
                    Sstat[i].update(S)
                    # for b in range(x1.size(0)):
                    #     Bstat[i].update(B[b].view([1, -1, 1, 1]))
                    #     Sstat[i].update(S[b].view([1, -1, 1, 1]))
                    #
                x = layer.forward(x, **kwargs)
        return Bstat, Sstat
    
    def print_batch_stats(self):
        Bstat, Sstat = self.batch_statistics()
        for (i, layer) in enumerate(self.model):
            if isinstance(layer, LinearNorm):
                print('Layer {}, Bstat.mean: {:.2g}, Bstat.std: {:.2g}, Sstat.mean: {:.2g}, Sstat.std: {:.2g}'.format(i, Bstat[i].mean.mean().item(), Bstat[i].var.sqrt().mean().item(), Sstat[i].mean.mean().item(), Sstat[i].var.sqrt().mean().item()))
                # %print('Max Sstat.std: {:.2g}'.format(Sstat[i].var.sqrt().max().item()))
    
    def init_norm_fixed_noise(self, data):
        # loop over training set, standard batches
        # Bstat, Sstat = self.batch_statistics()
        # use statistics of converged BN
        file = self.args.training_dir + 'CIFAR_S3/l5/BN SGD -m9 lr project new/state_last'
        state1 = torch.load(file)
        Bstat, Sstat = state1.batch_statistics()
        del state1
        #
        self.model.eval()
        # remove BN layers #
        self.model.remove_normalization()
        # introduce new norm layers
        self.model.set_normalization(self.normalization, **self.args)  # insert norm layers
        if self.normalization == 'AP2':
            self.model.init_normalization_AP2(data.size(), preserve_equivalence=False)
        elif self.normalization == 'BN':
            self.model.init_normalization_BN(data, preserve_equivalence=False)
        elif self.normalization == 'Weight':
            self.model.forward(data, preserve_equivalence=True)
        
        # set SSB variances
        for (i, layer) in enumerate(self.model):
            if isinstance(layer, LinearNorm):
                # move the affine part to the extra scale-bias
                layer.norm.sb.add_sb()
                w, b = layer.norm.sb.get_affine()
                layer.norm.sb.sb.set_affine(w, b)
                if self.args.SB_channelwise:
                    vB = Bstat[i].var
                    vS = Sstat[i].var
                else:
                    vB = Bstat[i].var.mean()
                    vS = Sstat[i].var.mean()
                layer.norm.sb.B.set_mean_var(torch.zeros_like(Bstat[i].mean), vB)
                # layer.norm.sb.B.set_mean_var(torch.zeros_like(Bstat[i].mean), torch.zeros_like(Bstat[i].var))
                layer.norm.sb.B.freeze()
                # layer.norm.sb.S.set_mean_var(torch.ones_like(Sstat[i].mean), torch.zeros_like(Sstat[i].var))
                layer.norm.sb.S.set_mean_var(torch.ones_like(Sstat[i].mean), vS)
                layer.norm.sb.S.freeze()
        
        pass
    
    # todo: moveout to test extensions
    def test_stability(self, **kwargs):
        # stability test
        stab = []
        std = 0.0
        while std <= 0.5:
            loss, acc, odds, _ = self.evaluate(self.test_loader, std, **kwargs)
            self.log("noise %g, test accuracy %g" % (std, 100 * acc))
            stab.append([std, acc, loss, odds])
            std += 0.05
        # np.savez(self.path + 'stab.npz', stab=stab)
        return stab
    
    # todo: moveout to test extensions
    def test_stability_known_noise(self):
        # stability test
        stab = []
        std = 0.0
        while std <= 0.5:
            # tell the known noise level to the model
            self.model.input_layer().noise_var = square(std)
            loss, acc, odds, _ = self.evaluate(self.test_loader, std)
            self.log("noise %g, test accuracy %g" % (std, 100 * acc))
            stab.append([std, acc, loss, odds])
            std += 0.1
        # restore model settings
        self.model.input_layer().noise_var = self.args.input_var
        # use adifferent save path
        path = self.path[:-1] + '--known_noise=True/'
        force_path(path)
        # np.savez(path + 'stab.npz', stab=stab)
        # return stab
    
    def save_history(self):
        np.savez(self.path + 'hist', t_loss=self.t_loss, t_acc=self.t_acc, v_loss=self.v_loss, v_acc=self.v_acc, loss_mean=self.loss_mean,
                 loss_var=self.loss_var)
        np.savez(self.path + 'it_hist', **self.it_hist)
        save_object(self.path + 'info.pkl', self.info)
    
    def valid_epoch(self):
        # train
        loss, acc, _, n_t = self.evaluate(self.train_loader)
        self.log('Train set: Average loss: {:.5f}, Accuracy: {:.1f}%'.format(loss, 100. * acc))
        self.t_loss.append(loss)
        self.t_acc.append(acc)
        tt_loss = loss*n_t
        #
        # validation
        loss, acc, _, n_v = self.evaluate(self.val_loader)
        self.log('Valid set: Average loss: {:.5f}, Accuracy: {:.1f}%'.format(loss, 100. * acc))
        self.v_loss.append(loss)
        self.v_acc.append(acc)
        tv_loss = loss*n_v
        
        if self.model.is_stochastic():
            # report also AP2 inference
            pass
            #loss, acc, _, n_v = self.evaluate(self.val_loader, method='AP2')
            #self.log('Valid AP2: Average loss: {:.5f}, Accuracy: {:.1f}%'.format(loss, 100. * acc))
            #self.v_loss.append(loss)
            #self.v_acc.append(acc)
            #tv_loss = loss * n_v
        #
        # best epoch
        if loss < self.min_v_loss:
            self.min_v_loss_i = self.epoch
            self.min_v_loss = loss
            # save this state as the currently best model
            # self.log('Saving the now-best model, at epoch{}'.format(self.epoch))
            # torch.save(self, self.path + 'state_v_best')
        # calibrate
        if len(self.v_loss) >= 2:
            if self.args.reg_calibrate == 'total_grad':
                delta_t = (self.t_loss[-1] - self.t_loss[-2]) * n_t
                delta_v = (self.v_loss[-1] - self.v_loss[-2]) * n_v
                # want delta_t + delta_v monotonous
                if delta_t + delta_v < 0:
                    # do nothing
                    pass
                    # if self.regfactor > 1 / 10:
                    #     self.regfactor /= 1.1
                else:
                    # regularize more
                    if self.regfactor < 1000:
                        self.regfactor *= 1.1
            elif self.args.reg_calibrate == 'v_monotone':
                delta_t = self.t_loss[-1] - self.t_loss[-2]
                delta_v = self.v_loss[-1] - self.v_loss[-2]
                # want delta_v monotonous
                if delta_v > 0:
                    if delta_t > 0:
                        # regularize less
                        if self.regfactor > 1 / 10:
                            self.regfactor /= 1.1
                    else:
                        # regularize more
                        if self.regfactor < 1000:
                            self.regfactor *= 1.1

        # calibrate
        # if tt_loss > tv_loss:
        #     # regularize less
        #     if self.regfactor > 1/10:
        #         self.regfactor /= 1.1
        # else:
        #     # regularize more
        #     if self.regfactor < 1000:
        #         self.regfactor *= 1.1
                
        # for m in self.model.modules():
        #     if isinstance(m, RandomScaleBias):
        #         m.var_mult = self.args.reg_like_BN/2 * self.regfactor

        #self.log('Reg factor: {}'.format(self.regfactor))
    
    def trial(self, lr):
        print('Evaluating lr: {}'.format(lr))
        self.args.lr = lr
        self.new_model()
        # self.train_epoch(max_iters=20)
        try:
            for i in range(self.args.lr_estimate_epochs):
                self.train_epoch()
                l = self.loss_stat.mean
                if l > 1e10:
                    return float('inf')
        except NumericalProblem as e:
            return float('inf')
        # result exponentially weighted sample mean
        l = self.loss_stat.mean
        print('Loss estimate: {}'.format(l))
        return l
    
    def select_lr(self):
        sargs = self.args.copy()
        self.args.verbose = False
        f = lambda x: self.trial(lr=pow(10, x))
        # llr = gss(f, -5, -2)
        # print(f(-3))
        # print(f(-3))
        # llr = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(-6, -2), options=dict(maxiter=10, disp=True)).x
        # llr = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(-6, -1), options=dict(maxiter=10, disp=True)).x
        llr = scipy.optimize.minimize_scalar(f, method='bounded', bounds=(-6, 0), options=dict(maxiter=10, disp=True)).x
        self.args = sargs
        self.args.lr = pow(10.0, llr)
        print('Selected lr: {}'.format(self.args.lr))
        return
    
    def continue_training(self):
        if self.epoch == 0:
            if self.args.lr is None:  # automatic search of lr
                self.select_lr()
                self.new_model()
        while self.epoch < self.args.epochs:
            # train next epoch
            self.train_epoch()
            self.log('Training model: ' + self.model_class.__name__ +'/' + self.var_args_s)
            self.loss_mean += [self.loss_stat.mean]
            self.loss_var += [self.loss_stat.var]
            #
            self.valid_epoch()
            #
            # exit(0)
            #
            # save
            self.save_history()
            # do more in checkpoints
            if self.epoch in [self.args.epochs - 1, 99]:
                # save checkpoint
                self.log('Saving checkpoint')
                checkpoint_name = self.path + 'state--epoch-{}'.format(self.epoch)
                # torch.save(self, checkpoint_name)
            # succesfull
            self.epoch += 1
            # save this
            torch.save(self, self.path + 'state_last')
        # validate last epoch
        self.valid_epoch()
        self.save_history()


class NoneException(BaseException):
    pass


class StateSaver:
    def __init__(self, state: TrainingState):
        # (self, Model, Optimizer, loss, args: odict, var_args: odict, train_loader, val_loader, test_loader):
        """args: fixed arguments, var_args: varied arguments"""
        self.state = state
        self.path = self.state.path
        self.model_name = self.state.model_class.__name__
        self.model_var_name = self.state.model_class.__name__ + self.state.var_args_s
        mkdir_recursive(os.path.dirname(self.state.args.path))
    
    def __train_protected(self):
        err_name = self.path + 'error'
        # check whether we have an up-to-date result or a failed run
        loaded_state = None
        l_epochs = None
        try: # try load current state
            if not getattr(self.state.args, 'train_anew') and os.path.exists(self.path + 'state_last'):
                loaded_state = torch.load(self.path + 'state_last')
                self.state.log('Load state at epoch: {}'.format(loaded_state.epoch))
                d1 = dict(loaded_state.construct_args.copy())
                d2 = dict(self.state.construct_args.copy())
                d1.pop('epochs', None)
                d2.pop('epochs', None)
                d1.pop('path', None)
                d2.pop('path', None)
                d1.pop('cuda', None)
                d1.pop('name', None)
                d2.pop('name', None)
                d2.pop('cuda', None)
                d1.pop('dataset_path', None)
                d2.pop('dataset_path', None)
                for key in set(d2) - set(d1): # ignore parameters that did not exist in d1 at all
                    print('new parameter: ' + key)
                    #if d2[key] != default_args[key]
                    d2.pop(key, None)
                if d1 != d2:
                    self.state.log('Parameters differ')
                    set1 = set(d1.items())
                    set2 = set(d2.items())
                    self.state.log('Old values:', set1 - set2)
                    self.state.log('New values:', set2 - set1)
                    c = query_yes_no('Continue the training using the new parameters (y) or start anew (n) and overwrite?', default="yes")
                    if c:
                        print('Answer Yes')
                    if not c:
                        print('Answer No')
                        loaded_state = None  # can't use it
                # update parameters
                #loaded_state.construct_args = self.state.construct_args.copy()
                loaded_state.construct_args = odict(self.state.construct_args.copy(), **d1)
                # d1 = dict(loaded_state.args.copy())
                # d1.pop('epochs', None); d1.pop('path', None); d1.pop('cuda', None); d1.pop('dataset_path', None)
                d2 = dict(self.state.construct_args.copy())
                d2.pop('epochs', None)
                d2.pop('path', None)
                d2.pop('cuda', None)
                d2.pop('dataset_path', None)
                if d2['lr'] is None:
                    d2.pop('lr', None)
                loaded_state.args = odict(loaded_state.args.copy(), **d2) # update to new
                # loaded_state.args = odict(self.state.args.copy(), **d1)
                #loaded_state.var_args = self.state.var_args.copy()
                # loaded_state.var_args = odict(self.state.var_args.copy(), **loaded_state.var_args)
                loaded_state.var_args = odict(loaded_state.var_args.copy(), **self.state.var_args) # update to new
                # update parameters
                loaded_state.inference = loaded_state.args.inference
                loaded_state.model.inference = loaded_state.args.inference
                # backward compatibility: copy all new attributes
                for a in dir(self.state):
                    if not hasattr(loaded_state, a):
                        print('updating attribute: ' + a)
                        setattr(loaded_state, a, getattr(self.state,a))
                #
                loaded_state.upgrade(**loaded_state.args)
                if self.state.args.cuda:
                    loaded_state.model.cuda()
                #
                # if not hasattr(loaded_state, 'regfactor'):
                #     loaded_state.regfactor = 1
                #
        except BaseException as e:
            self.state.log('Load failed: ' + str(e))
            loaded_state = None
        # check whether the was a failed run
        failed_run = False
        if os.path.exists(err_name):
            self.state.log('Found a failed run' + self.path)
            os.remove(err_name)
            failed_run = True
        if not failed_run and loaded_state is not None and loaded_state.epoch >= self.state.args.epochs:
            self.state.log('Up to date')
            self.state = loaded_state
            return
        if loaded_state is None:
            self.state.log('Starting anew')
            self.state.new_model()
        else:
            self.state.log('Restoring to loaded state')
            loaded_state.args.epochs = self.state.args.epochs
            d2 = self.state.construct_args.copy()
            path = self.state.path
            #
            self.state = loaded_state
            # fix parameters for old files
            # todo: use loaded location to fix paths
            self.state.path = path
            self.state.args.path = d2.path
            self.state.args.dataset_path = d2.dataset_path
            #self.state.var_args_s = self.state.args_to_s(self.state.var_args)
            #self.state.model_var_name = type(self.state.model).__name__ + self.state.var_args_s
            #
        # continue training
        force_path(self.path)
        force_path(self.state.path)
        self.state.log('Training started:  ' + self.model_var_name + '\n\n')
        self.state.log('Args:')
        self.state.log(self.state.args)
        self.state.continue_training()
        # training was succesfull
        self.state.log('Training completed:  ' + self.model_var_name + '\n\n')
    
    def train(self):
        #  initialize training
        self.state.log('Training model: ' + self.model_var_name)
        mkdir_recursive(os.path.dirname(self.path))
        #  lock the dir
        lock_name = self.path + 'training_running'
        err_name = self.path + 'error'
        if os.path.exists(lock_name) and not os.path.exists(self.path + 'state_last'):
            #  degugging, remove the lock
            os.remove(lock_name)
        if not os.path.exists(lock_name):
            # open(lock_name, 'w').close()
            # now it is locked
            try:
                self.__train_protected()
            # except BaseException as e:
            #     if isinstance(e, KeyboardInterrupt):  # rethrow KeyboardInterrupt
            #         raise e
            #     type, value, tb = sys.exc_info()
            #     traceback.print_exc()
            #     last_frame = lambda tb=tb: last_frame(tb.tb_next) if tb.tb_next else tb
            #     frame = last_frame().tb_frame
            #     ns = dict(frame.f_globals)
            #     ns.update(frame.f_locals)
            #     code.interact(local=ns)
            except NoneException as e:
                msg = 'ERROR: ' + str(e)
                msg += '\n When training model ' + self.model_var_name
                # for line in traceback.format_stack():
                #    msg = msg + line.strip() + '\n'
                self.state.log(msg)
                traceback.print_exc(file=sys.stdout)
                # log.write(msg)
                # create 'error file'
                errf = open(err_name, 'w')
                errf.write(msg)
                traceback.print_exc(file=errf)
                errf.close()
                if isinstance(e, KeyboardInterrupt):  # rethrow KeyboardInterrupt
                    raise e
                    # unlock
                    # os.remove(lock_name)
        else:
            raise Exception('path is locked (a training is running or was manually terminated)\n' + lock_name)


def run_configs(model_class, train_dataset, test_dataset, args, vargs):
    for k, l in vargs.items():
        if isinstance(l, list):
            vargs1 = vargs.copy()
            for v in l:
                vargs1[k] = v
                print("{}:{}".format(k, v)) #
                run_configs(model_class, train_dataset, test_dataset, args, vargs1)
            return
    # leaf of reccurent call
    print(vargs)
    state = vnn.TrainingState(model_class, train_dataset, test_dataset, args, vargs)
    saver = vnn.StateSaver(state)
    saver.train()
