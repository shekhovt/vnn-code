from .models import *


class MNIST_MLP(Model):
    def __init__(self, input_var=0.1, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        ll = self
        ll += InputNoise(input_var)
        ll += Flatten()
        ll += Linear(784, 100)
        ll += Sigmoid()
        ll += Linear(100, 10)
        ll += LogSoftmax(simplified=softmax_simplified)


class MNIST_FC(Model):
    def __init__(self, input_var=0.1, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        ll = self
        ll += InputNoise(input_var)
        ll += Linear(784, 100)
        ll += Sigmoid()
        ll += Linear(100, 10)
        ll += LogSoftmax(simplified=softmax_simplified)

class MNIST_MLPd(Model):
    def __init__(self, input_var=0.1, dropout=0.0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        ll = self
        ll += InputNoise(input_var)
        ll += Flatten()
        ll += Linear(784, 100)
        ll += ReLU()
        ll += Dropout(dropout)
        ll += Linear(100, 10)
        ll += LogSoftmax(simplified=softmax_simplified)

class MNIST_MLPd2(Model):
    def __init__(self, input_var=0.1, dropout=0.0, dropout_noise='bernoulli', softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        ll = self
        ll += InputNoise(input_var)
        ll += Flatten()
        ll += Linear(784, 1200)
        ll += ReLU()
        ll += Dropout(dropout, dropout_noise)
        ll += Linear(1200, 1200)
        ll += ReLU()
        ll += Dropout(dropout, dropout_noise)
        ll += Linear(1200, 10)
        ll += LogSoftmax(simplified=softmax_simplified)
        
class MNIST_MLP_Deep(Model):
    def __init__(self, input_var=0.1, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = Sigmoid(bernoulli=False)
        inner_units = 20
        ll = self
        ll += InputNoise(input_var)
        ll += Flatten()
        ll += Linear(784, inner_units)
        ll += activation
        for l in range(5):  # add a few linear-sigmoid layers
            ll += [Linear(inner_units, inner_units)]
            ll += activation
        ll += Linear(inner_units, 10)
        ll += LogSoftmax(simplified=softmax_simplified)


class LeNet(Model):
    def __init__(self, input_var=0.1, anoise_var=0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ModuleList([AddNoise(anoise_var), ReLU()])
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)
        
class LeNetPadd(Model):
    def __init__(self, input_var=0.1, anoise_var=0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ModuleList([AddNoise(anoise_var), ReLU()])
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=7, stride=2, padding=1)
        ll += activation
        ll += Conv2d(32, 64, kernel_size=3, stride=2, padding=1)
        ll += ConvTranspose2d(64, 32, kernel_size=3, stride=2, padding=1)
        ll += Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)


class LeNetPaddAvg(Model):
    def __init__(self, input_var=0.1, anoise_var=0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ModuleList([AddNoise(anoise_var), ReLU()])
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=7, stride=2, padding=1)
        ll += activation
        ll += Conv2d(32, 64, kernel_size=3, stride=1, padding=1)
        ll += AvgPool2d(2)
        ll += ConvTranspose2d(64, 32, kernel_size=3, stride=1, padding=1)
        ll += Conv2d(32, 64, kernel_size=3, stride=1)
        ll += activation
        ll += Conv2d(64, 50, kernel_size=3)
        ll += activation
        ll += AdaptiveAvgPool2d()
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)

        
class LeNetS(Model):
    def __init__(self, input_var=0.1, anoise_var=0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = Sigmoid(bernoulli=True)
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)

class LeNetD(Model):
    def __init__(self, input_var=0.1, anoise_var=0, dropout=0.0, dropout_noise='bernoulli', softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ModuleList([AddNoise(anoise_var), ReLU()])
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=5, stride=2)
        ll += activation
        ll += Dropout(dropout, dropout_noise)
        ll += Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += Dropout(dropout, dropout_noise)
        ll += Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += Dropout(dropout, dropout_noise)
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)


class CIFAR_Spingenberg(Model):
    def __init__(self, input_var=0.1, anoise_var=0.0, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition #
        activation = ModuleList([AddNoise(anoise_var), LeakyReLU(0.01)])
        ll = self
        ll += InputNoise(input_var)
        # conv layers
        ksize  = [3, 3, 3, 3, 3, 3, 3, 1, 1]
        #stride = [1, 1, 2, 1, 1, 2, 2, 1, 1]
        stride = [1, 1, 2, 1, 1, 2, 1, 1, 1]
        odepth = [96, 96, 96, 192, 192, 192, 192, 192, 10]
        idepth = 3  # expect color image
        for l in range(len(ksize)):
            #padding = ksize[l]//2
            padding = 0
            ll += Conv2d(idepth, odepth[l], kernel_size=ksize[l], stride=stride[l], padding=padding)
            ll += activation
            idepth = odepth[l]
        # average pooling 6
        ll += AdaptiveAvgPool2d()
        ll += LogSoftmax(simplified=softmax_simplified)


class ABlock(ModuleList):#
    def __init__(self, activation='LReLU', dropout=0.0, dropout_noise='bernoulli', input_channels=None, **kwargs):
        ModuleList.__init__(self)
        ll = self
        #
        if activation == 'LReLU':
            activation = LeakyReLU(0.01)
        elif activation == 'Sigmoid':
            activation = Sigmoid(False)
        elif activation == 'Bernoulli':
            activation = Sigmoid(True)
        else:
            raise KeyError()
        ll += [activation]
        #
        if dropout == 'Variational':
            ll +=[VariationalDropout(input_channels, **kwargs)]
        elif isinstance(dropout, numbers.Number)  and dropout > 0:
            ll +=[Dropout(dropout, dropout_noise)]
        #
        if kwargs.get('gain')  == 'fc':
            ll += [Gain(input_channels, **kwargs)]

class CIFAR_S1(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition #
        activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        # conv layers
        ksize  = [3, 3, 3, 3, 3, 3, 3, 1, 1]
        stride = [1, 1, 2, 1, 1, 2, 1, 1, 1]
        odepth = [96, 96, 96, 192, 192, 192, 192, 192, 10]
        idepth = 3  # expect color image
        for l in range(len(ksize)):
            #padding = ksize[l]//2
            padding = 0
            ll += Conv2d(idepth, odepth[l], kernel_size=ksize[l], stride=stride[l], padding=padding)
            ll += activation
            idepth = odepth[l]
        # average pooling 6
        ll += AdaptiveAvgPool2d()
        ll += LogSoftmax(simplified=softmax_simplified)

class CIFAR_S2(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition #
        #activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        # conv layers
        ksize  = [ 3,  3,  3,   3,   3,   3,   3,   1, 1]
        stride = [ 1,  1,  2,   1,   1,   2,   1,   1, 1]
        odepth = [96, 96, 96, 192, 192, 192, 192, 192, 10]
        idepth = 3  # expect color image
        for l in range(len(ksize)):
            #padding = ksize[l]//2
            padding = 0
            ll += Conv2d(idepth, odepth[l], kernel_size=ksize[l], stride=stride[l], padding=padding)
            if l < len(ksize)-1:
                #ll += activation # don't put activation after last layer
                ll += ABlock(activation, input_channels=odepth[l], **kwargs)
            idepth = odepth[l]
        # average pooling 6
        ll += AdaptiveAvgPool2d()
        #ll += AvgPool2d([2,2])
        ll += LogSoftmax(simplified=softmax_simplified)
        
class CIFAR_S3(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, pooling='stride', **kwargs):
        Model.__init__(self, **kwargs)
        self.eval()
        # model definition #
        if kwargs.get('SConv'):
            CC = SConv2d
        else:
            CC = Conv2d
        if kwargs.get('dropout') == 'Variational_Uncorrelated':
            CC = VariationalDropout_Conv2d
        elif kwargs.get('dropout') == 'Variational_Uncorrelated_RT':
            CC = VariationalDropoutRT_Conv2d
        #activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        # conv layers
        ksize  = [ 3,  3,  3,   3,   3,   3,   3,   1, 1]
        stride = [ 1,  1,  2,   1,   1,   2,   1,   1, 1]
        odepth = [96, 96, 96, 192, 192, 192, 192, 192, 10]
        padd   = [ 1,  1,  1,   1,   1,   1,   1,   0, 0]
        idepth = 3  # expect color image
        for l in range(len(ksize)):
            #padding = ksize[l]//2
            padding = 0
            if stride[l] == 1:
                st = 1
                MP = []
            else:
                if pooling == 'stride':
                    st = stride[l]
                    MP = []
                elif pooling == 'max':
                    st = 1
                    MP = MaxPool2d(kernel_size=stride[l], **kwargs)
                
            ll += CC(idepth, odepth[l], kernel_size=ksize[l], stride=st, padding=padd[l])
            if l < len(ksize)-1:
                #ll += activation # don't put activation after last layer
                ll += ABlock(activation, input_channels=odepth[l], **kwargs)
                ll += MP
            idepth = odepth[l]
        # average pooling 6
        if kwargs.get('softmax_dropout'):
            ll += [Dropout(kwargs.get('dropout'), kwargs.get('dropout_noise'))]
        ll += AdaptiveAvgPool2d()
        ll += LogSoftmax(simplified=softmax_simplified)
        # print(self)

class CIFAR_S4(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        self.eval()
        # model definition #
        if kwargs.get('SConv'):
            CC = SConv2d
        else:
            CC = Conv2d
        #activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        # conv layers
        ksize  = [ 3,  3,  3,   3,   3,   3,   3,   1, 1]
        stride = [ 1,  1,  2,   1,   1,   2,   1,   1, 1]
        odepth = [96, 96, 96, 192, 192, 192, 192, 192, 10]
        padd   = [ 1,  1,  1,   1,   1,   1,   1,   0, 0]
        idepth = 3  # expect color image
        for l in range(len(ksize)):
            #padding = ksize[l]//2
            padding = 0
            ll += CC(idepth, odepth[l], kernel_size=ksize[l], stride=1, padding=padd[l])
            if l < len(ksize)-1:
                #ll += activation # don't put activation after last layer
                ll += ABlock(activation, input_channels=odepth[l], **kwargs)
            idepth = odepth[l]
            if stride[l] > 1:
                ll += AvgPool2d(kernel_size=(stride[l],stride[l]))
        # average pooling 6
        ll += AdaptiveAvgPool2d()
        ll += LogSoftmax(simplified=softmax_simplified)
        # print(self)


class CIFAR_S2Reg(CIFAR_S2):
    def __init__(self, reg_like_BN, **kwargs):
        CIFAR_S2.__init__(self, **kwargs)
        for l, layer in enumerate(self):
            if isinstance(layer, Conv2d) or isinstance(layer, Linear):
                self.insert(l+1, RandomScaleBias(reg_like_BN/2, reg_like_BN, **kwargs))
        print(self)

class LeNet_S1(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += Conv2d(50, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)


class MNIST_MLP_S1(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 1200, kernel_size=28)
        ll += activation
        ll += Conv2d(1200, 1200, kernel_size=1)
        ll += activation
        ll += Conv2d(1200, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)
        
class MNIST_MLP_H1(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        activation = ABlock(activation, **kwargs)
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 100, kernel_size=28)
        ll += activation
        ll += Conv2d(100, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)
        
class CIFAR_MLP(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 1000, kernel_size=32)
        ll += ABlock(activation, **kwargs)
        ll += Conv2d(1000, 1000, kernel_size=1)
        ll += ABlock(activation, **kwargs)
        ll += Conv2d(1000, 10, kernel_size=1)
        ll += LogSoftmax(simplified=softmax_simplified)

class LeNet5(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, pooling='stride', **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        if pooling=='stride':
            stride = 2
            MP = []
        elif pooling == 'max':
            stride = 1
            MP = MaxPool2d(kernel_size=(2,2), **kwargs)
            
        ll = self
        ll += InputNoise(input_var)
        # Conv1
        ll += Conv2d(1, 6, kernel_size=5, stride=stride)
        ll += ABlock(activation, **kwargs)
        ll += MP
        # Conv2
        ll += Conv2d(6, 16, kernel_size=5, stride=stride)
        ll += ABlock(activation, **kwargs)
        ll += MP
        # FC1
        ll += Conv2d(16, 120, kernel_size=4) # this is equivalent to FC
        ll += ABlock(activation, **kwargs)
        # FC2
        ll += Conv2d(120, 84, kernel_size=1)  # this is equivalent to FC
        ll += ABlock(activation, **kwargs)
        # FC3-Softmax
        ll += Conv2d(84, 10, kernel_size=1)  # this is equivalent to FC
        ll += LogSoftmax(simplified=softmax_simplified)


class LeNet5torch(Model):
    def __init__(self, input_var, activation, softmax_simplified=True, pooling='stride', **kwargs):
        Model.__init__(self, **kwargs)
        # model definition
        if pooling == 'stride':
            stride = 2
            MP = []
        elif pooling == 'max':
            stride = 1
            MP = MaxPool2d(kernel_size=(2, 2), **kwargs)
        
        ll = self
        ll += InputNoise(input_var)
        ll += Conv2d(1, 32, kernel_size=5, stride=stride)
        ll += ABlock(activation, **kwargs)
        ll += MP
        # Conv2
        ll += Conv2d(32, 64, kernel_size=5, stride=stride)
        ll += ABlock(activation, **kwargs)
        ll += MP
        # FC1
        ll += Conv2d(64, 50, kernel_size=4)
        ll += ABlock(activation, **kwargs)
        # FC2-Softmax
        ll += Conv2d(50, 10, kernel_size=1)  # this is equivalent to FC
        ll += LogSoftmax(simplified=softmax_simplified)
