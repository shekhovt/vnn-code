import torch
import torch.nn as nn

import numpy as np

import torch.nn.functional as F
from torch.nn.parameter import Parameter

from .random_variable import *
from .functions import *
from .utils import *
from torch import Tensor

import operator
from functools import reduce

import math
from collections import OrderedDict
from torch.nn.modules.module import _addindent

# import threading
# from threading import current_thread
# threadLocal = threading.local()
# threadLocal.AttributeError = None



class Module(ModuleBase):
    """ augmenting nn.Module with extra functionality """
    
    def __init__(self):
        nn.Module.__init__(self)  # nothing bad if we call it twice
        self.training = False
        self.out_channels_ = None
    
    def reset_parameters(self):
        """ nn.Module does not have reset_parameters, but other parents may have, so a stub so that derived classes could always call it """
        if hasattr(super(), 'reset_parameters'):
            super().reset_parameters()
    
    def forward(self, *args, **kwargs):
        """
        forward pass with different propagation rules: method = 'AP1', 'AP2', 'sample'
        method == None dispatches to AP1 or AP2 based on type of first argument
        """
        method = kwargs.get('method')
        assert (method is not None), 'forward inference without method is deprecated'
        if method == 'AP1':
            if self.forward_AP1.__func__ == Module.forward_AP1:
                raise NotImplementedError('Module must override either forward forward_AP1')
            else:
                return self.forward_AP1(*args, **kwargs)
        elif method == 'AP2':
            if self.forward_AP2.__func__ == Module.forward_AP2:
                raise NotImplementedError('Module must override either forward or forward_AP2')
            else:
                return self.forward_AP2(*args, **kwargs)
        elif method == 'sample':
            # if self.forward_sample is Module.forward_sample:
            #     raise NotImplementedError('Module must override either forward or forward_sample')
            # else:
            return self.forward_sample(*args, **kwargs)
        else:
            raise ValueError('unknown forward method {}'.format(method))
    
    # def forward_(self, *args, method, **kwargs):
    #     """Subclasses shall override either this single method or forward_AP1, forward_AP2 and optionally forward_sample"""
    #     raise AssertionError('Not implemented')
    
    def forward_AP1(self, *args: Tensor, **kwargs) -> Tensor:  # virtual method to be overloaded by inheriting functions
        """
        propagate only the mean, ignore variance
        derived classes should implement this, **kwargs should be passed down to submodules
        """
        if self.forward.__func__ == Module.forward:
            raise NotImplementedError('Module must override either forward forward_AP1')
        else:
            return self.forward(*args, **odict(kwargs, method='AP1'))
    
    def forward_AP2(self, *args: RandomVar, **kwargs) -> RandomVar:  # virtual method to be overloaded by inheriting functions
        """
        propagate mean and variance
        derived classes should implement this, **kwargs should be passed down to submodules
        """
        if self.forward.__func__ == Module.forward:
            raise NotImplementedError('Module must override either forward forward_AP2')
        else:
            return self.forward(*args, **odict(kwargs, method='AP2'))
    
    # def forward_sample(self, *args: [Tensor, RandomVar], **kwargs) -> Tensor:  # virtual method to be overloaded by inheriting functions
    #     """
    #     propagate a sample
    #     """
    #     if self.forward is Module.forward:
    #         raise NotImplementedError('Module must override either forward forward_sample')
    #     else:
    #         return self.forward(*args, **odict(**kwargs, method='sample'))

    def forward_sample(self, *args: [Tensor, RandomVar], **kwargs) -> Tensor:  # virtual method to be overloaded by inheriting functions
        """default code here is for determenistic functions: to sample the input if it was a RandomVar and compute forward_AP1"""
        # if forward has override call that
        if self.forward.__func__ != Module.forward:
            return self.forward(*args, **odict(kwargs, method='sample'))
        # otherwise the default is the determenistic behaviour
        if self.forward_AP1.__func__ == Module.forward_AP1:
            raise NotImplementedError('Module must override forward_AP1')
        args = list(args)
        for i in range(len(args)):
            if isinstance(args[i], RandomVar):
                args[i] = args[i].sample()
        y = self.forward_AP1(*args, **odict(kwargs, method='sample'))
        return y
    
    def forward_MC(self, *args, MC_samples=10, method=None, **kwargs):
        yy = self.forward(*args, method='sample', **kwargs).detach()
        for s in range(MC_samples-1):  # draw several samples
            yy += self.forward(*args, method='sample', **kwargs).detach()
        yy /= MC_samples
        return yy
    
    def __call__(self, *args, **kwargs):
        return self.forward(*args, **kwargs)
    
    # @property
    def get_out_channels(self) -> int or None:
        """how many channels are there on the output"""
        m = self
        # nn.conv2d define out_channels, others something else
        if hasattr(m, 'out_channels'):  # linear
            return m.out_channels
        elif hasattr(m, 'out_features'):  # linear
            return m.out_features
        elif hasattr(m, 'num_channels'):
            return m.num_channels
        elif hasattr(m, 'num_features'):  # BN
            return m.num_features
        if hasattr(m, 'in_features'):
            return m.in_features
        elif hasattr(m, 'in_channels'):
            return m.in_channels
        return None
    
    def out_size(self, size):
        """ return output shape for a given input shape. default to output the same shape"""
        return size
    
    def is_stochastic(self):
        if self.forward_sample.__func__ != Module.forward_sample:
            return True
        for m in self.modules():
            if m is self:
                continue
            if m.is_stochastic():
                return True
        return False
    
    def can_reduce_input(self):
        return True
    
    # @out_channels.setter
    # def out_channels(self, value):
    #    self.out_channels_ = value
    
    def apply(self, fn, *args, parent=None, name=None, **kwargs):
        kv = OrderedDict(self.named_children())
        for n, m in kv.items():
            if isinstance(m, Module):
                m.apply(fn, *args, parent=self, name=n, **kwargs)
        fn(self, *args, parent=parent, name=name, **kwargs)
        return self
    
    def reg_parameters(self):
       for m in self.modules():
           if isinstance(m, StochasticParam):
               for p in m.parameters():
                   yield p
            
    
    def __repr__(self):
        tmpstr = self.__class__.__name__
        if len(self._modules) > 0:
            tmpstr += ' (\n'
            if hasattr(self, '__iter__'):
                kv = OrderedDict(enumerate(self))
            else:
                kv = OrderedDict(self.named_children())
            for key, module in kv.items():
                modstr = module.__repr__()
                modstr = _addindent(modstr, 2)
                tmpstr = tmpstr + '  (' + str(key) + '): ' + modstr + '\n'
            tmpstr = tmpstr + ')'
        return tmpstr