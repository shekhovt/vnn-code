import torch
import torch.nn as nn
import torch.nn.functional as F

import math

import vnn
from .random_variable import *
from torch import Tensor

from functools import reduce
import operator

V_S = (math.pi ** 2) / 3  # variance of standard logistic distribution
MIN_VAR = 1e-16
#MIN_VAR = 0.001

class NumericalProblem(BaseException):
    pass

def gauss_pdf(x):
    return 1 / math.sqrt(2 * math.pi) * torch.exp(-(x * x) / 2)


def gauss_cdf(x):
    # not used in the current version #
    return 0.5 + 0.5 * torch.erf(x / math.sqrt(2))

def logistic_cdf(x):
    if isinstance(x, numbers.Number):
        return 1 / (1 + math.exp(-x))
    else:
        return torch.sigmoid(x)

def logistic_icdf(x):
    if isinstance(x, numbers.Number):
        return math.log(x) - math.log(1 - x)
    else:
        eps = torch.finfo(x.dtype).eps
        x_clamped = x.clamp(min=eps, max=1 - eps)
        return torch.log(x_clamped) - torch.log1p(-x_clamped)

def logit(x):
    return logistic_icdf(x)

def phi(x):
    return gauss_pdf(x)


def Phi(x):
    return gauss_cdf(x)


def Phi_approx(x):
    """
    Approximate narmal_cdf with logistic
    """
    return torch.sigmoid(x * math.sqrt(vnn.V_S)) # by matching variance
    #return F.sigmoid(x * math.sqrt(8/math.pi)) # by matching derivative at 0


def R_approximation(x):
    """
    Approximation to x*phi(x)+(x^2+1)*Phi(x)-(x*Phi(x)+phi(x))^2, needed in ReLU and Max
    """
    #return logistic_cdf((x - 0.3766) * 1.8482)  # fit with logistic cdf
    #return logistic_cdf(x * (1/0.3729))  # max KL < 0.035 for LReLU(0.01), LReLU(0.1) in [-6 6]
    return logistic_cdf(x * (1 / 0.3758))  # max KL < 0.035 for LReLU(0.01), LReLU(0.1) in [-6 6]


def log1p_exp(x: Tensor) -> Tensor:
    """
    compute log(1+exp(a)) = log(exp(0)+exp(a))
    """
    m = torch.clamp(x, min=0).detach()  # max(x,1)
    return m + torch.log(torch.exp(x - m) + torch.exp(-m))

def log_sum_exp2(x: Tensor, y: Tensor) -> Tensor:
    """
    compute log(exp(x) + exp(y))
    """
    m = torch.max(x,y).detach()
    return m + torch.log(torch.exp(x - m) + torch.exp(y - m))

def log_sub_exp2(x: Tensor, y: Tensor) -> Tensor:
    """
    compute log(exp(x) - exp(y))
    """
    m = torch.max(x,y).detach()
    return m + torch.log(torch.exp(x - m) - torch.exp(y - m))



def log_sum_exp_1d(x: Tensor, dim, keepdim=False) -> Tensor:
    """
    Numerically more stable implementation of log_sum_exp, along one selected dimension
    """
    # method: using log_softmax
    # they promise a stable numerical implementation, so this should be good
    # but dim argument is currently unsopported and is unclear what it is doing by default
    # m = x - F.log_softmax(x, dim=dim)
    
    # method: subtract max before exponentiating
    m, _ = x.max(dim=dim, keepdim=True)
    m = m.detach()  # don't propagate gradient, it will cancel out
    m = m + torch.log(torch.exp(x - m).sum(dim=dim, keepdim=True))
    
    if keepdim is False:
        m = m.squeeze(dim)
    return m


def log_sum_exp_ex(x: Tensor, dim, keepdim=False) -> Tensor:
    """
    exclusive log_sum_exp
    """
    # method: full sum - element, subtract max before exponentiating
    m, _ = x.max(dim=dim, keepdim=True)
    m = m.detach()  # don't propagate gradient, it will cancel out #
    a = torch.exp(x - m)
    S = a.sum(dim=dim, keepdim=True)
    r = m + torch.log(S-a)
    if keepdim is False:
        r = r.squeeze(dim)
    return r


def log_sum_exp(x: Tensor, dims=None, keepdims=False) -> Tensor:
    """
    Log-sum-exp on all dimensions or a list of selected dimensions
    """
    if dims is None:
        m = torch.max(x).detach()
        r = m + torch.log(torch.sum(torch.exp(x - m)))
        if keepdims:
            r = r.expand([1] * len(x.size()))  # expand to dims of the input
        return r
    
    else:
        dims.sort(reverse=True)  # reorder descending so we can drop dims on the fly
        # todo: flatten on nearby dims
        for dim in dims:
            x = log_sum_exp_1d(x, dim, keepdim=keepdims)
        return x


def adaptive_log_sum_exp_2d(x: Tensor, keepdims=False):
    return log_sum_exp(x, [2, 3], keepdims=keepdims)


def check_var(x: Tensor):
    """
    Check variance values are elligible
    """
    # print(torch.min(x.data))
    if all_checks:
        if not (x.data >= 0).all():
            raise NumericalProblem('variance is negative or nan')
        
def check_real(x:Tensor):
    """
       Check for NaN / Inf
    """
    if all_checks:
        if not (x == x).all():
            raise NumericalProblem('NaN encountered')
        if not (x.abs() < float('inf')).all():
            raise NumericalProblem('+-Inf encountered')


def avg_pool2d(x, kernel_size, **kwargs):
    """
    Like F.avg_pool2d
    """
    y = RandomVar(F.avg_pool2d(x.mean, kernel_size=kernel_size))
    n = reduce(operator.mul, kernel_size, 1)  # calculate scaling used in averaging
    y.var = F.avg_pool2d(x.var, kernel_size=kernel_size) / n
    return y


def align_dims(x, y):
    d1 = x.dim()
    d2 = y.dim


def n_avg_cross_entropy_log(a, b):
    # assert a.dim() ==2 and b.dim() == 2
    sz = list(a.size())
    sz[1] = 1
    n = reduce(operator.mul, sz, 1)
    # return -torch.sum(torch.exp(a)*b)/a.size()[0]  # sum over channels and batch dimensions, average over batch
    return -torch.sum(torch.exp(a) * b) / n  # sum over all dimensions, average over all but channels
    # a = a[0,:,100,100]
    # # a[0] = 0
    # # a[1] = 1
    # # return -torch.sum(torch.exp(a[0,:,100,100]) * b[0,:,100,100])
    # return -b[0, 0, 100, 100]

def one_hot_log(y: Tensor, n_channels: int):
    sz = list(y.size())
    if len(sz) == 1:
        sz += [1]
    sz[1] = n_channels
    yl = y.new([]).float().new(size=sz)
    yl.fill_(-30.0)  # log(1e-13)
    sz[1] = 1
    yl.scatter_(1, y.long().view(sz), 0.0)  # log(1)
    return yl
