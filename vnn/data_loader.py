from torchvision import datasets, transforms
import torch.utils.data
import torch.utils
import numpy as np
from torch.utils.data.sampler import SubsetRandomSampler


def get_train_valid_loader(dataset,
                           batch_size,
                           random_seed,
                           valid_size=0.1,
                           shuffle=True,
                           num_workers=0,
                           pin_memory=False):
    assert ((valid_size >= 0) and (valid_size <= 1)), "[!] valid_size should be in the range [0, 1]."
    
    num_train = len(dataset)
    indices = list(range(num_train))
    split = int(np.ceil(valid_size * num_train))
    
    if shuffle:
        np.random.seed(random_seed)
        np.random.shuffle(indices)
    
    train_idx, valid_idx = indices[split:], indices[:split]
    
    # print(valid_idx)
    
    train_sampler = SubsetRandomSampler(train_idx)
    valid_sampler = SubsetRandomSampler(valid_idx)
    
    train_loader = torch.utils.data.DataLoader(dataset,
                                               batch_size=batch_size, sampler=train_sampler,
                                               num_workers=num_workers, pin_memory=pin_memory)
    
    valid_loader = torch.utils.data.DataLoader(dataset,
                                               batch_size=batch_size, sampler=valid_sampler,
                                               num_workers=num_workers, pin_memory=pin_memory)
    
    return train_loader, valid_loader
