import os
import sys

import matplotlib
matplotlib.use('Agg')

import numpy as np
from vnn.utils import *
import matplotlib.pyplot as plt
import pickle
from matplotlib.ticker import MaxNLocator


def save_pdf(file_name):
    force_path(file_name)
    plt.savefig(file_name, bbox_inches='tight', dpi=199, pad_inches=0)
