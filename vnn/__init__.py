import torch
import torch.nn as nn
from .random_variable import *
from .module import *
from .layers import *
from .functions import *
from .data_loader import *
from .training import *
from .models import *
from .utils import *

#todo organized unit tests
#todo plot results once training finishes
#todo regression problems
#todo merge Module with ModluleList - always keep a synchronized pair of list and dict
#todo minimize usage of cuda(), every model should know its placement?
#todo log all mesagges
#todo running statistics of all losses
#todo samppling inference (used in dropout) does not combine with sigmoid-Bernoulli - warn
#todo init normalization BN with equivalence - use whole training set (not possible, need to be recurrent)
#todo hooks are useful for calculating statistics
#todo superclass Linears: (Linear, Conv2d, TransposedConv2d, ScaleBias?, AdaptiveAvgPool2d?, AvgPool2d?) or NormLinears - by functionality
#todo reimplement AP2 normalization by extending forward to work on list of inputs, including kwargs
#todo make normalization modular: split normalization and affine. Use cases: norm + affine, norm + SSB. Easy control of regularizers
"""
normalization should know its preceeding linear layer and the subsequent scalebias. when initialized or removed -> adjust these two layers
parameters: defaults, feasible values, selected value. Distingish training and testing arguments (grids are independent). grid object.
#
Inspect saving only the model parameters / buffers

"""
