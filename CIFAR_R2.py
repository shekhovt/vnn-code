from config import *

if __name__ == '__main__':
    model_class = CIFAR_S3;
    args.path = training_dir + model_class.__name__ + '/R2/'
    #### varied arguments ####
    var_args = odict()
    #####
    var_args.noise_augment = 0.0
    #### datasets config ####
    transform = transforms.Compose(
        [transforms.RandomHorizontalFlip(), transforms.RandomCrop(32, 4), transforms.ToTensor(), AddNoiseTransform(var_args.noise_augment)])
    train_dataset = datasets.CIFAR10(root=args.dataset_path, train=True, download=True, transform=transform)
    test_dataset = datasets.CIFAR10(root=args.dataset_path, train=False, download=True, transform=transform)
    ######################################
    #
    # var_args.lr_exp_base = 0.97723 # decrease by 0.1 every 100 epochs
    # var_args.lr_exp_base = 0.98855  # decrease by 0.1 every 200 epochs
    # var_args.lr_exp_base = 0.99426  # by 0.1 in 400 epochs #
    var_args.lr_exp_base = 0.99616  # by 0.1 in 600 epochs
    args.batch_size = None  # no special default in this experiment
    #
    args.activation = 'LReLU'
    var_args.epochs = 1200
    args.batch_size = 32
    args.dropout = 0.2
    args.optimizer = 'SGD'
    args.SGD_Nesterov = True
    args.SGD_momentum = 0.9
    args.pooling = 'stride'
    #
    # var_args.weight_init = 'xavier'

    # AP2 #
    if True:
    # if False:
        args.path = training_dir + model_class.__name__ + '/E2/'
        # var_args.name = 'test'
        # var_args.optimizer = 'Adam'
        var_args.dropout = 0.2
        var_args.lr = None
        # var_args.lr = 0.02
        # var_args.init = None
        # var_args.weight_init = 'xavier'
        var_args.init = 'AP2'
        # var_args.norm = None
        var_args.norm = 'AP2'
        var_args.project = True  #
        var_args.train_inference = 'AP2'
        var_args.inference = 'AP2'
        # var_args.epochs = 1500
        args.train_anew = True
        run_configs(model_class, train_dataset, test_dataset, args, var_args)
        exit(0)

    # baseline #
    # if True:
    if False:
        args.path = training_dir + model_class.__name__ + '/E1/'
    #     var_args.optimizer = 'Adam'
        # var_args.pooling = 'max'
        var_args.dropout = 0.2
        var_args.lr = None
        # var_args.lr = 0
        # var_args.epochs = 1
        # var_args.init = None
        var_args.init = 'AP2'
        var_args.epochs = 100
        # var_args.AP2fix = 'True'
        # var_args.lr = 0.001
        # var_args.init = [None, 'AP2', 'BN']
        # var_args.init = None
        # var_args.weight_init = 'xavier'
        # var_args.init = 'BN'
        #
        var_args.norm = None
        # var_args.norm = 'AP2'
        # var_args.project = True
        # var_args.norm = 'BN'
        # var_args.dropout_noise = 'normal'
        var_args.train_inference = 'sample'
        var_args.inference = 'AP1'
        run_configs(model_class, train_dataset, test_dataset, args, var_args)
        exit(0)
    
    # Variational Dropout #
    
    # if True:
    if False:
        args.path = training_dir + model_class.__name__ + '/E1/'
    #     var_args.optimizer = 'Adam'
    #     var_args.dropout = 'Variational'
    #     var_args.dropout = 'Variational_Uncorrelated'
        var_args.dropout = 'Variational_Uncorrelated_RT'
        var_args.lr = None
        var_args.lr = 0.01
        var_args.init = 'AP2'
        # var_args.init = None
        var_args.norm = None
        var_args.train_inference = 'sample'
        var_args.inference = 'AP1'
        
        var_args.reg = 'KL'
        # var_args.reg_c = 1/10000
        var_args.reg_c = 1/100
        var_args.min_var = '2x 1e-6'
        # var_args.reg_c = 1
        # var_args.ScaleNormal = True
        # var_args.stochastic_S = True
        # var_args.name = 'Variational Dropout'
        
        run_configs(model_class, train_dataset, test_dataset, args, var_args)
        exit(0)


    # if True:
    # # if False:
    #     args.path = training_dir + model_class.__name__ + '/KL/'
    #     # var_args.optimizer = 'Adam'
    #     # var_args.dropout = 0.0
    #     var_args.dropout = 0.2
    #     var_args.epochs = 1200
    #     var_args.softmax_dropout = True
    #     args.input_var = 0.01
    #     # var_args.lr = None
    #     # var_args.lr = 0.01228
    #     var_args.lr = 0.016
    #     var_args.init = 'AP2'
    #     var_args.norm = 'AP2' # <-- this trigers invalid gradient
    #     var_args.project = True
    #     var_args.train_inference = 'AP2'
    #     var_args.inference = 'AP2'
    #     #var_args.reg = 'KL_pq'
    #     #var_args.reg_c = 0.001
    #     var_args.name = 'test'
    #     run_configs(model_class, train_dataset, test_dataset, args, var_args)
    #     exit(0)

    # AP2 # KL_pq
    if True:
    # if False:
        args.path = training_dir + model_class.__name__ + '/KL/'
        # var_args.optimizer = 'Adam'
        # var_args.dropout = 0.0
        var_args.dropout = 0.2
        var_args.epochs = 1200
        var_args.softmax_dropout = True
        args.input_var = 0.01
        # var_args.lr = None
        # var_args.lr = 0.01228
        var_args.lr = 0.016
        var_args.init = 'AP2'
        # var_args.norm = None
        var_args.norm = 'AP2'
        var_args.project = True
        var_args.train_inference = 'AP2'
        var_args.inference = 'AP2'
        var_args.reg = 'KL_pq'
        var_args.reg_c = 0.001
        # var_args.name = 'test KL ssample norm=AP2 reg_c=0.01'
        # var_args.name = 'test KL ssample norm=AP2 reg_c=0.001 dropout scale'
        # var_args.name = 'test KL ssample norm=AP2 reg_c=0.001 dropout scale1'
        var_args.name = 'test KL ssample norm=AP2 reg_c=0.001 dropout corr'
        # var_args.name = 'test KL ssample norm=AP2 reg_c=0.001 dropout rank1'
        # var_args.name = 'test KL ssample norm=AP2 reg_c=0.001 dropout s1s2'
        var_args.approx_params = True
        var_args.new_optimizer = True
        # var_args.train_anew = True
        run_configs(model_class, train_dataset, test_dataset, args, var_args)
        exit(0)