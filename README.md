vnn-code (Variance Neural Networks)

Contains implementation of the following methods:

Shekhovtsov and Flach (2019): "Feed-forward Propagation in Probabilistic Neural Networks with Categorical and Max Layers"

Shekhovtsov and Flach (2018): "Normalization of Neural Networks using Analytic Variance Propagation"

Shekhovtsov and Flach (2019): "Stochastic Normalizations as Bayesian Learning"
