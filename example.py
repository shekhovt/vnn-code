import sys
[print(p) for p in sys.path]
import torch
print(torch.__version__)
#
import vnn
import math

import vnn.tests

"""example Module definition that uses our extened layers """
class LeNet(vnn.Model):  # vnn.Model has functionality of a list of layers, of a Module and has some training facilities
    def __init__(self, **kwargs):
        vnn.Model.__init__(self, **kwargs)
        #activation = vnn.ReLU()  # submodule consisting of adding noise and ReLU
        activation = vnn.ModuleList([vnn.AddNoise(0.1), vnn.ReLU()]) # submodule consisting of adding noise and ReLU
        ll = self #  pointer to selfƒ
        ll += vnn.InputNoise(0.0) # InputNoise layer adapts input depending on inference type and adds noise to it. Operator += appends to self (as list)
        ll += vnn.Conv2d(1, 32, kernel_size=5, stride=2)
        ll += activation
        ll += vnn.Conv2d(32, 64, kernel_size=5, stride=2)
        ll += activation
        ll += vnn.Conv2d(64, 50, kernel_size=4)
        ll += activation
        ll += vnn.Conv2d(50, 10, kernel_size=1)
        ll += vnn.LogSoftmax()
        
        
torch.manual_seed(1)

net = LeNet(inference='AP1') # we set the default propagation method to the standard propagation, called AP1

#li = net[i].weight.data+0
print(net) # prints the network structure hierarchically
# random test input
x = torch.Tensor(128, 1, 28, 28)
x.uniform_()

if net.a.is_cuda:
    x = x.cuda(net.a.get_device())

"""This will ilustrate different propagation methods on the network that can be chosen dynamically"""
o1 = net.forward(x) # this calls forward as usual with the propagation method default for the network (method = net.inference)
o1a = net.forward(x, method='AP1') # use AP1 inference explicitly
assert (o1.data - o1a.data).norm() < 1e-5 # check the outputs are indeed equal

o2 = net.forward(x, method='sample') # 'sample' inference samples noises and stochastic layers but not the softmax layer
o2a = net.forward(x, method='sample') # sample again
assert not o2.data.eq(o2a.data).all()

o3 = net.forward(x, method='AP2') # 'AP2' inference - analytic estimation through noisy and stochastic layers
assert not o1.data.eq(o3.data).all()

# do something else, layer-by layer
y = x
for l in range(3):
    y = net[l].forward(y, method='AP2')
for l in range(3,len(net),1):
    y = net[l].forward(y, method ='sample')

# Difference between sampling and AP2
print("Comparing Sampling and AP2:")
#oe = (o2-o2).data
N = 1000
S = vnn.RunningStatAvg()
for s in range(N):
    o2 = net.forward(x, method='sample') # sample noises and stochastic layers but the softmax layer
    S.update(o2.data)
#oe = oe/N
print('Average Deviation: {}'.format((S.mean-o3.data).mean()))
print('Average Samples std: {}'.format(math.sqrt(S.var.mean())))
print('Average Sample mean accuracy: {}'.format(S.std_of_mean.mean())) #

