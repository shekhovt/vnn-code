import os
import sys
import inspect

import matplotlib
matplotlib.use('Agg') 

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import cumfreq
import argparse
import collections
from config import *
from matplotlib import colors as colors
from vnn.utils import *
import scipy.stats
import pickle
from matplotlib.ticker import MaxNLocator

#ymax=0.95
ymax=0.95
ymin=0.88
ymin=0.80

# lymax=0.9
# lymin=0.3

# figsize = (4.0, 3.0)
# figsize = (4.0, 1.5)
# figsize = (6.0, 4.5)
#figsize = (8.0, 6)
figsize = (16.0, 12)
# # figsize = (64.0, 48)

# fullscreen = False
fullscreen = True
#
# show_legend = False
show_legend = True

# show_title = True ##
show_title = False

stability = False

train_valid_loss = False
unimportant = {'num_workers', 'code_version_patch','ssb_v'}
unimportant = {'num_workers', 'code_version_patch', 'ssb_v', 'ScaleNormal', 'reg', 'reg_c' ,'stochastic_S', 'name', 'stochastic_B', 'init', 'SGD_Nesterov'} #'SGD_momentum'
unimportant = {'optimizer', 'SGD_momentum', 'num_workers', 'code_version_patch', 'ssb_v', 'ScaleNormal', 'reg', 'reg_c' ,'stochastic_S', 'name', 'stochastic_B', 'init', 'SGD_Nesterov'} #'SGD_momentum'
# unimportant = {'num_workers'}
# important = ['name', 'norm']
# important = ['project']
# important_only = False
important_only = True
important = ['name']
skip_exp = odict()
select_exp = [odict()]
loc = 1
max_epochs = 1200

if False:
# if True:
    ymax = 0.92
    # root_dir = training_dir + 'SegMod1/l1/'
    # root_dir = training_dir + 'SegMod1/net5/'
    # root_dir = training_dir + 'UNet/l1/'
    # root_dir = training_dir + 'SegMod/softmax_net/'
    # root_dir = training_dir + 'SegMod/softmax-selected/'
    root_dir = training_dir + 'SegMod/softmax_net-SGD/'
    # important_only = False
    # important = ['SGD_momentum', 'lr', '']
    unimportant = {'num_workers', 'code_version_patch', 'ssb_v', 'train_datadir', 'test_datadir', 'task', 'name', 'weighted_loss_factor', 'SGD_Nesterov', 'SGD_momentum'}
    important_only = True
    # important = ['name','lr','reg_c']
    important = ['name']

if False:
# if True:
    ymax = 0.95
    root_dir = training_dir + 'CIFAR_S3/R2-max+/'
    # important_only = False
    # important = ['init', 'norm', 'dropout', 'project']
    important_only = True
    important = ['name', 'lr']
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    figsize = (4.0, 3)
    fullscreen = True
    show_title = False
    # show_legend = False

if False:
# if True:
    unimportant = {'optimizer', 'SGD_momentum', 'num_workers', 'code_version_patch', 'ssb_v', 'ScaleNormal', 'reg', 'stochastic_S', 'name',
                   'stochastic_B', 'init', 'SGD_Nesterov','dropout_noise','weight_init'}  # 'SGD_momentum'
    ymax = 0.95
    root_dir = training_dir + 'CIFAR_S3/R2/'
    # root_dir = training_dir + 'CIFAR_S3/R2-init=AP2/'
    important_only = False
    important = ['init', 'norm', 'dropout']
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')

if False:
# if True:
    ymax = 0.95
    root_dir = training_dir + 'CIFAR_S3/R2-init=AP2/'
    important_only = True
    important = ['name','lr']
    figsize = (4.0*1.2, 3*1.2)
    fullscreen = False
    loc = 1
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')

# if False:
if True:
    ymax = 0.95
    root_dir = training_dir + 'CIFAR_S3/R2-norm=AP2++/'
    important_only = True
    important = ['name','lr']
    figsize = (4.0*1.2, 3*1.2)
    fullscreen = False
    loc = 0
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')

if False:
# if True:
    ymax = 1.0
    root_dir = training_dir + 'LeNet5torch/KL2/'
    important_only = False
    important = ['lr','reg_c','name']
    fullscreen = True
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')


if False:
# if True:
    # ymax = 0.95
    model_class = CIFAR_S3;
    root_dir = training_dir + model_class.__name__ + '/l7-seed/'
    important_only = True
    important = ['seed']
    # figsize = (4.0*1.2, 3*1.2)
    figsize = (8.0, 6.0)
    fullscreen = False
    loc = 0
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')


if False:
# if True:
    # ymax = 0.95
    model_class = CIFAR_S3;
    root_dir = training_dir + model_class.__name__ + '/l7-tests/'
    important_only = True
    important = ['name']
    figsize = (4.0, 3.0)
    fullscreen = False
    stability = True
    loc = 0


if False:
# if True:
    # ymax = 0.94
    # ymin = 0.92
    model_class = CIFAR_S3
    # root_dir = training_dir + model_class.__name__ + '/l7-batch-BN/'
    # root_dir = training_dir + model_class.__name__ + '/l7-fig2/'
    # root_dir = training_dir + model_class.__name__ + '/l7-batch-standard/'
    important_only = True
    important = ['name']
    # figsize = (4.0*1.2, 3*1.2)
    # figsize = (8.0, 6.0)
    figsize = (4.0, 3.0)
    fullscreen = False
    max_epochs = 600
    loc = 0
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')

if False:
# if True:
    ymax = 0.95
    model_class = CIFAR_S3
    root_dir = training_dir + model_class.__name__ + '/l7-fig1/'
    important_only = True
    important = ['name']
    figsize = (4.0, 3.0)
    fullscreen = False
    loc = 0


if False:
# if True:
    # ymax = 0.95
    model_class = CIFAR_S3;
    root_dir = training_dir + model_class.__name__ + '/KL/'
    # important_only = True
    # important = ['seed']
    # figsize = (4.0*1.2, 3*1.2)
    figsize = (8.0, 6.0)
    # fullscreen = False
    fullscreen = True
    loc = 0
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')


if False:
# if True:
    # ymax = 0.95
    model_class = CIFAR_S3;
    root_dir = training_dir + model_class.__name__ + '/l6-BN/'
    important_only = True
    important = ['seed']
    # figsize = (4.0*1.2, 3*1.2)
    figsize = (8.0, 6.0)
    fullscreen = False
    # select_exp = [odict(init=None, norm=None), odict(init='AP2', norm=None), odict(init='AP2', norm='AP2')]
    # select_exp = [odict(init='BN')]
    # select_exp = [odict(init='AP2', norm=None, optimizer='SGD')]
    # select_exp = [odict(optimizer='Adam')]
    # skip_exp = odict(pooling='max')

if False:
# if True:
    important_only = True
    important = ['name']
    # figsize = (4.0*1.2, 3*1.2)
    # figsize = (8.0, 6.0)
    figsize = (4.0, 3.0)
    fullscreen = False
    loc = 0
    ymax = 0.95
   
    #model_class = MNIST_MLP
    #model_class = MNIST_MLPd2
    #model_class = MNIST_MLP_Deep
    #model_class = LeNet
    #model_class = LeNetS
    #model_class = LeNetD
    #model_class = modelzoo.LeNetB
    #model_class = CIFAR_Spingenberg
    #model_class = LeNet_S1
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/n1b/'
    # model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/r3-nice/'
    # model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/l2/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l3/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-W-like/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-V-like/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l1-BN-project/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-BN-batch/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-W-project/'
    model_class = CIFAR_S3
    # root_dir = training_dir + model_class.__name__ + '/l5-V-project/'
    # root_dir = training_dir + model_class.__name__ + '/l7-BN-project/'
    # root_dir = training_dir + model_class.__name__ + '/l7-W-project/'
    
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-W-bayes/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-V-bayes/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l6-BN/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l6-vseed/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l5-bias/'
    # model_class = CIFAR_S4; root_dir = training_dir + model_class.__name__.
    #  + '/l1/'
    # model_class = CIFAR_S3; root_dir = training_dir + model_class.__name__ + '/l1-best/'
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/l1-nice/'
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/brendan'
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/r3-dropout/'; figsize = (4.0, 3.0); fullscreen = False
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/r3-BN/'; figsize = (4.0, 3.0); fullscreen = False
    #model_class = CIFAR_S2; root_dir = training_dir + model_class.__name__ + '/r3-standard/'; figsize = (4.0, 3.0); fullscreen = False
    #model_class = MNIST_MLP_S1
    #model_class = MNIST_MLP_H1

# root_dir = training_dir + 'LeNet/n1/'
# ymax=1.0

#root_dir = training_dir + model_class.__name__ + '/n2/'



#fullscreen = False

variant = ''

#skip_exp = odict(norm='BN')
#select_exp = [odict(dropout=0.2, activation='LReLU'), odict(activation='LReLU',norm=None, init='BN')]
#select_exp = [odict(activation='LReLU', init='BN')]
#select_exp = [odict(activation='LReLU')]
# [odict(init=None), odict(init='BN'), odict(init='AP2')]:
#select_exp = dict(init='AP2', norm='BN')
#skip_exp = dict(project=True, init='BN')
#skip_exp = dict(project=True, init='AP2')
#skip_exp = dict(project=True)
#select_exp = dict(init='AP2')
#select_exp = dict(init='BN')


variant = 'state_last'
#variant = 'state--epoch-99'

alpha = 0.1

name_colors = dict(colors.BASE_COLORS, **colors.CSS4_COLORS)


def plot(*args, fmt=None, elinewidth=None, capsize=None, capthick=None, **kwargs):
    plt.plot(*args, **kwargs)


# def errorbar_fency_log(x, y, yerr=None, color='#7EFF99', fmt=None, **kwargs):
#      yl = y - yerr
#      yu = y + yerr
#      plt.fill_between(x, np.exp(yl), np.exp(yu), alpha=0.1, facecolor=color) #, linewidth=0.5, edgecolor=color)
#      kwargs['markevery'] = 500
#      plot(x, np.exp(y), fmt, color=color, **kwargs)

def errorbar_fency(x, y, yerr=None, yl=None, yu=None, color='#7EFF99', fmt=None, **kwargs):
    if yl is None:
        yl = y - yerr
        yl = np.maximum(yl, 0.5*y)
    if yu is None:
        yu = y + yerr
    kwargs['markevery'] = int(math.ceil(len(x)/10))
    #kwargs['markevery'] = 10
    # plt.fill_between(x, yl, yu, alpha=0.1, facecolor=color)
    plt.fill_between(x, yl, yu, alpha=0.2, facecolor=color)
    plot(x, y, fmt, color=color, **kwargs)


def force_path(file_name):
    dir_name = os.path.dirname(file_name)
    if not os.path.exists(dir_name):
        os.mkdir(os.path.dirname(file_name))


def save_pdf(file_name):
    force_path(file_name)
    plt.savefig(file_name, bbox_inches='tight', dpi=199)


def list_diff(first, second):
    second = set(second)
    return [item for item in first if item not in second]

def mix_colors(a, alpha, b):
    return tuple(c1*alpha+c2*(1-alpha) for c1, c2 in zip(a, b))


def sort_keys(keys: dict) -> list:
    d = sorted(set(keys))
    d0 = ['inference', 'init', 'norm']
    for k in reversed(d0):
        if k in d:
            d.remove(k)
            d.insert(0, k)
    #print(d)
    return d

def llegend():
    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

scriptdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))  # script directory
cdw = os.getcwd()

if len(sys.argv) > 1:  # have command line arguments
    root_dir = sys.argv[1]
# else:  # no arguments
#    if cdw != scriptdir:  # running from the desired location
#        root_dir = cdw

if len(sys.argv) > 2:  # have at least 2 arguments
    variant = sys.argv[2]
    

for select_exp in select_exp: #[odict(init=None), odict(init='BN'), odict(init='AP2')]:
    
    c_i = 0
    plt.close("all")
    print('Select exp:{}'.format(select_exp))
    
    subd = [os.path.join(root_dir, o) for o in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir, o))]
    
    subd.sort()
    subd1 = []
    # prefilter
    ARGS = dict()
    for d,dir_name in reversed(list(enumerate(subd))):
        info_name = dir_name + '/info.pkl'
        if not os.path.exists(info_name):
            print('Skipping {}'.format(dir_name))
            del subd[d]
            continue
        info = pickle.load(open(info_name, "rb"))
        all_args = info.args
        for k,v in all_args.items():#
            if isinstance(v, list) or isinstance(v, tuple) and len(v)>0 and isinstance(v[0], list):
                all_args[k] = tuple(tuple(l) for l in v)
        apairs = set(all_args.items())
        if apairs & set(skip_exp.items()) != set() or not set(select_exp.items()) <= apairs or all_args.epochs < 2 or '-X' in dir_name:
            print('Skipping {} - epochs {}'.format(dir_name, all_args.epochs))
            del subd[d]
        ARGS[dir_name] = all_args
    
    # pref = os.path.commonprefix(subd)
    #print(subd)
    #exit(0)
  
    params = []
    subd1 = []
    common_params = None
    
    # take out common params
    common_params = set.intersection(*[set(arg.items()) for arg in ARGS.values()])
    # turn it into dictionary
    common_params = {key: value for (key, value) in common_params}
    print("Common params: ")
    print(common_params)

    # # parse params, take out common params
    # common_params = None
    # for loop in range(2):
    #     # loop dirs and fetch params
    #     for d in range(len(subd)):
    #         dir_name = subd[d]
    #         pstring = os.path.basename(dir_name)  # parse name into parameters
    #         pairs = pstring.split(sep='--')
    #         if loop == 1:
    #             pairs = list_diff(pairs, common_params)
    #         args = {}
    #         fail = False
    #         for p in pairs:
    #             if p != '':
    #                 kv = p.split(sep='=')
    #                 if len(kv) == 2:
    #                     args[kv[0]] = kv[1]
    #                 else:
    #                     fail = True
    #                     break
    #         if fail:
    #             continue
    #         # print(args)
    #         if loop == 0:
    #             s = set()
    #             for k, v in args.items():
    #                 if k == 'mtype':
    #                     continue
    #                 s = s | {k + '=' + v}
    #             if common_params is None:
    #                 common_params = s
    #             else:
    #                 common_params = common_params & s
    #             subd1.append(dir_name)
    #         if loop == 1:
    #             params.append(args)
    #     if loop == 0:
    #         if common_params is None:
    #             common_params = set()
    #         common_params = common_params - {''}
    #         if 'lr_down' not in ''.join(common_params):
    #             common_params = common_params - {'lr=0.001'}
    #         print("Common params: ")
    #         print(list(common_params))
    #
    # subd = subd1
    # print(params)
    # print(''.join(common_params))
    
    stab_records = ["Accuracy vs Gaussian Noise", "Loss vs Noise", "Confidence Odds vs Gaussian Noise"]
    stab_records1 = ["Accuracy vs Perturbation", "Loss vs Perturbation", "Confidence Odds vs Perturbation"]
    
    bar_idx = 0
    
    for d in range(len(subd)):
        dir_name = subd[d]
        # args = params[d]
        info_name = dir_name + '/info.pkl'
        if not os.path.exists(info_name):
            continue
        info = pickle.load(open(info_name, "rb"))
        all_args = info.args
        #
        all_args.net_struct = None
        #
        apairs = set(all_args.items())
        if apairs & set(skip_exp.items()) != set() or not set(select_exp.items()) <= apairs or all_args.epochs < 2 or '-X' in dir_name:
            print('Skipping {} - epochs {}'.format(dir_name, all_args.epochs))
            continue
        print('Plotting %s ' % dir_name)
        #print('Params:',all_args)
        args = {key: value for (key, value) in info.args.items() if key not in common_params and value is not None and value is not False}
        if 'path' in args:
            del args['path']
        if 'lr' in args:
            args['lr'] = '%s' % float('%.2g' % args['lr'])
        print('Uncommon params:', args)
        
        exp_name = ''
        
        # if 'norm' not in args and 'norm' not in common_params:
        #     args['norm'] = str(all_args.norm)
            
        #if 'dropout' not in args and 'dropout' not in common_params:
        #    args['dropout'] = str(all_args.norm)
            
        #if 'init' not in args:
        #    args['init'] = str(all_args.init)
        
        keys = sort_keys(args.keys())
    
        # if 'init' in args.keys:
        #     keys = sorted( set(keys) - {'init'})
        
    #        if 'V' not in args['mtype']:
    #            keys = sorted( set(keys) - {'anoise_var'})
    
    #        if 'mtype' in keys:
    #            keys = ['mtype'] + sorted(set(keys) - {'mtype'})
    
    #        if all_args.norm is None:
    #            exp_name = exp_name + '--norm=None'

        all_args['lr'] = '%s' % float('%.2g' % all_args['lr'])
        
        if 'name' in keys:
            keys = sorted(set(keys) - {'name'})
        
        if 'code_version_major' in keys:
            ver = 'V=' + str(args['code_version_major']) + '.' + str(args['code_version_minor']) + '.' + str(args['code_version_patch'])
            keys = sorted( set(keys) - {'code_version_major', 'code_version_minor', 'code_version_patch'})
        else:
            ver = ''
        
        # if 'name' in all_args:
        #     exp_name = exp_name + '' + str(all_args['name'])
        
        for k in important:
            if k == 'name':
                exp_name = exp_name + '' + str(os.path.basename(dir_name))
            elif k in all_args:
                    exp_name = exp_name + '--' + k + '=' + str(all_args[k])
                    keys = sorted(set(keys) - {k})
            # if important_only or k not in keys:
            #     if k == 'name':
            #         exp_name = exp_name + '' + str(os.path.basename(dir_name))
            #     elif k in all_args:
            #         exp_name = exp_name + '--' + k + '=' + str(all_args[k])
    
        if not important_only:
            for key in keys:
                if key != 'inference' and key != 'epochs' and key not in unimportant:
                    exp_name = exp_name + '--' + key + '=' + str(args[key])
        #
        
    
        # if 'lr=' not in exp_name and True:
        #     #exp_name = exp_name + '--lr={:.1E}'.format(all_args.lr)
        #     s = '{:.1E}'.format(all_args.lr)
        #     s = '--lr=${}\cdot 10^{{{}}}$'.format(*s.split('E'))
        #     exp_name = exp_name + s

        exp_name += ver

        if exp_name == '':
            exp_name = 'standard training'
        
        # parse exp_name for keys
        sname = exp_name
        line_s = '-'  # default line style
        linewidth = 1.0
        color_s = None  # no defualt color
        marker_s = ''
        #
        if all_args.inference == 'AP2':
            if '--input_var=0.001' in sname:
                line_s = ':'
                linewidth = 0.25
                #linewidth = 0.5
            elif '--input_var=0.01' in sname:
                line_s = '--'
                linewidth = 0.5
            elif '--input_var=0.1' in sname:
                linewidth = 1
                pass
            elif '--input_var=0.25' in sname:
                linewidth = 2
            elif '--input_var=1' in sname:
                linewidth = 3
        
        if 'avar' in root_dir:
            linewidth = 0.25
            if '--anoise_var=0.01' in sname:
                line_s = ':'
                linewidth = 0.25
                #linewidth = 0.5
            elif '--anoise_var=0.1' in sname:
                line_s = '--'
                linewidth = 0.5
            elif '--anoise_var=0.25' in sname:
                linewidth = 1
                pass
            elif '--anoise_var=1' in sname:
                linewidth = 2
        
        if '--lr_down=sqrt' in sname:
            line_s = ':'
        
        line_s = '-'
        
        #if '--lr=0.001' in sname:
        #    marker_s = '^'
        
        #if '--known_noise' in sname:
        #    marker_s = '*'
        if '--softmax=full' in sname:
            marker_s = '*'
    
        if '--noise_input=True' in sname:
            marker_s = '*'
    
        # color by norm
        #
        if all_args.norm is None:
            color_s = colors.to_rgb('green')
        elif all_args.norm == 'BN':
            color_s = colors.to_rgb('blue')
        elif all_args.norm == 'Weight':
            color_s = colors.to_rgb('red')
        elif all_args.norm == 'AP2':
            color_s = colors.to_rgb('darkorchid')
            
        #if all_args.init == 'AP2':
            #line_s = ':'
            #marker_s = '^'
            #color_s = mix_colors(color_s, 0.5, (1, 1, 1))
            
        if all_args.init is None:
            color_s = mix_colors(color_s, 0.6, (0, 0, 0))
            #print(color_s)
    
        # if all_args.project:
        #     marker_s = '^'
    
        # if args['mtype'] == 'S-BN' or args['mtype'] == 'S-BNnorm-BNinit' or 'BNnorm' in args['mtype']:
        #     color_s = 'blue'
        # elif args['mtype'] == 'V-Norm' or 'Vnorm' in args['mtype']:
        #     color_s = 'darkorchid'
        # elif args['mtype'] == 'V-Init':
        #     color_s = 'olive'
        # elif args['mtype'] == 'V':
        #     color_s = 'red'
        # elif args['mtype'] == 'S':
        #     color_s = 'green'
        #     marker_s = 'o'
        # elif args['mtype'] == 'S-BNinit':
        #     color_s = 'green'
        #     marker_s = 's'
        # elif args['mtype'][0] == 'S' and 'Wnorm' in args['mtype'] and 'BNinit' in args['mtype'] or 'Wnorm' in args['mtype']:
        #     color_s = 'yellow'
        #     marker_s = '^'
        # elif args['mtype'] == 'S-Vnorm':
        #     color_s = 'sienna'
        # elif args['mtype'] == 'S-BNinit':
        #     color_s = 'skyblue'
        # elif args['mtype'] == 'S-Vinit':
        #     color_s = 'green'
        
        if '0.98' in sname:
            marker_s = '^'
            
        if 'norm=AP2' in sname:
            marker_s = '*'
    
        if 'norm=None' in sname:
            marker_s = 'o'
            #line_s = '--'
        
        if 'tweak' in sname:
            color_s = colors.to_rgb('green')
            
    #        if 'anoise' in sname:
    #            color_s = 'black'
        
        exp_name = exp_name + ' '
        #exp_name = exp_name.replace('--mtype=', '')
        exp_name = exp_name.replace('--input_var=', '--var=')
        exp_name = exp_name.replace('--softmax=simplified', '')
        exp_name = exp_name.replace('=0.0001,', '=$10^{-4}$,')
        exp_name = exp_name.replace('=0.001,', '=$10^{-3}$,')
        exp_name = exp_name.replace('=0.01,', '=$10^{-2}$,')
        exp_name = exp_name.replace('--lr_down=const', '')
        exp_name = exp_name.replace('--lr_down=sqrt', '$/\sqrt{k}$')
        exp_name = exp_name.replace('--lr_down=exp', '$\cdot 0.96^k$')
        exp_name = exp_name.replace('--', ', ')
        if 'input_var' in args and args['input_var'] == '0':
            exp_name = exp_name.replace('var=0', '')
        exp_name = exp_name.replace('anoise_var=', 'avar=')
        exp_name = exp_name.replace('var=0.1', '')
        exp_name = exp_name.replace('reg_variant=0', '')
        exp_name = exp_name.replace('train_inference=', 'training=')
        exp_name = exp_name.replace(', ,', ',')
        exp_name = exp_name.replace('  ', ' ')
        
        #exp_name = exp_name.replace(', var=0.1', '')
    
        while len(exp_name)>0 and (exp_name[0] == ' ' or exp_name[0] == ','):
            exp_name = exp_name[1:len(exp_name)]
        
        ss = line_s + marker_s
        
        markersize = 3
        
        pargs = {'markersize': markersize, 'linewidth': linewidth, 'label': exp_name}
        
        print(pargs)

        #color_s = None
        #color_s = (tuple(np.random()))
        prop_cycle = plt.rcParams['axes.prop_cycle']
        ccolors = prop_cycle.by_key()['color']
        color_s = ccolors[c_i % len(ccolors)]
        c_i = c_i + 1
        
        
        if color_s is not None:
            pargs['color'] = color_s
    
        ebarargs = dict(elinewidth=1, capsize=3)
    
        # print(pargs)
        
        res_name = dir_name + '/hist.npz'
        it_res_name = dir_name + '/it_hist.npz'
        #
        if os.path.exists(res_name):
            # validation loss
            hist = np.load(res_name)
            hist = dict(hist)
            it_hist = None

            for k, v in hist.items():
                if v.size > max_epochs:
                    hist[k] = v[:max_epochs]
            
            vloss = RunningStatAdaptive(0.5)
            vloss_m = np.empty_like(hist['v_loss'])
            vloss_e = np.empty_like(hist['v_loss'])
            vacc = RunningStatAdaptive(0.5)
            vacc_m = np.empty_like(hist['v_acc'])
            vacc_e = np.empty_like(hist['v_acc'])
            for i, l in enumerate(hist['v_acc']):
                vloss.update(hist['v_loss'][i])
                vloss_m[i] = vloss.mean
                vloss_e[i] = math.sqrt(vloss.var)
                #
                vacc.update(hist['v_acc'][i])
                vacc_m[i] = vacc.mean
                vacc_e[i] = math.sqrt(vacc.var)
                
            try:
                if os.path.exists(it_res_name):
                    it_hist = np.load(it_res_name)
                    it_hist = dict(it_hist)
                    its = it_hist['loss'].size
                    t_epochs = hist['loss_mean'].size
                    t_iters = it_hist['loss'].size
                    #smooth = pow(0.05, 1/(its/50))
                    #smooth = 0.95
                    loss_stat = RunningStatAdaptive(alpha)
                    #loss_stat = RunningStat(0.999)
                    it_mean = np.empty_like(it_hist['loss'])
                    it_var = np.empty_like(it_hist['loss'])
                    it_s = np.empty_like(it_hist['loss'])
                    
                    for i,l in enumerate(it_hist['loss']):
                        loss_stat.update(l)
                        it_mean[i] = loss_stat.mean
                        it_s[i] = loss_stat.std_of_mean
                        it_var[i] = loss_stat.var
                        # if i > 0:
                        #     it_var[i] = loss_stat.var
                        # else:
                        #     it_var[i] = 0
        
                    y = np.empty(t_epochs)
                    y1 = np.empty(t_epochs)
                    y2 = np.empty(t_epochs)
                    its_epoch = t_iters/t_epochs
                    for t in range(1,t_epochs,1):
                        i1 = int(its_epoch * t)
                        i2 = int(its_epoch * (t + 1))
                        x = it_hist['loss'][i1:i2]
                        x = sorted(x)
                        n1 = int(0.25 * len(x))
                        n2 = int(0.975 * len(x))
                        #y[t] = np.mean(x[n1:n2+1]) #hist['t_loss'][t] #it_mean[i1]
                        #y1[t] = x[n1]
                        #y2[t] = x[n2]
                        y[t] = it_mean[i1]
                        s = it_s[i1]
                        y1[t] = it_mean[i1] - 3*s
                        y2[t] = it_mean[i1] + 3*s
                    y1[0] = y2[0] = y[0] = it_mean[0]
            except BaseException as e:
                print(str(e))
            plt.figure("Validation Loss", figsize=figsize)
            #plt.plot(hist['v_loss'], ss, **pargs)
            errorbar_fency(range(vloss_m.size), vloss_m, yerr=vloss_e, fmt=ss, **pargs, **ebarargs)
            #plt.semilogy(hist['v_loss'], ss, **pargs)
            # plot the selected model
            v_epoch = hist['v_loss'].argmin()
            #plt.plot(v_epoch, hist['v_loss'][v_epoch], 'og', label=None)
            #plt.semilogy(v_epoch, hist['v_loss'][v_epoch], 'og', label=None)
            plt.legend(loc=loc)
            if fullscreen:
                llegend()
            if not show_legend:
                ax = plt.gca()
                ax.legend_.remove()
            #plt.ylim(0.5, 2.5)
            if show_title:
                plt.title('Validation Loss')
            ax = plt.gca()
            ax.yaxis.grid(True)
            ax.set_yscale("log", nonposy='clip')
            #plt.xlabel('epochs')
            # ax.grid(True)
    
            # Train loss
            plt.figure("Train Loss", figsize=figsize)
            #plt.semilogy(hist['t_loss'], ss, **pargs)
            if 'loss_mean' in hist:
                if it_hist is not None:
                    #errorbar_fency(epochs, it_mean, yerr=np.sqrt(it_var), fmt=ss, **pargs, **ebarargs)
                    epochs = np.arange(t_epochs)
                    #errorbar_fency(epochs, y, yl=y1, yu=y2, fmt=ss, **pargs, **ebarargs)
                    #plt.fill_between(epochs, y1, y2, alpha=0.1, facecolor=pargs['color'])
                    #plt.plot(epochs, y, ss, **pargs)
                    #
                    epochs = (np.arange(len(it_mean))+1)/len(it_mean)*hist['loss_mean'].size
                    s = np.sqrt(it_var)
                    #plt.fill_between(epochs, np.maximum(it_mean - s, it_mean*0.2) , it_mean + s, alpha=0.1, facecolor=pargs['color'])
                    #plt.fill_between(epochs, it_mean - it_s, it_mean + it_s, facecolor=pargs['color'])
                    plt.plot(epochs, it_mean, ss, **pargs, markevery = int(math.ceil(len(epochs)/10)))
                    #plt.plot(epochs, it_hist['loss'], ss, **pargs)
                    
                else:
                    errorbar_fency(range(hist['loss_mean'].size), hist['loss_mean'], yerr=np.sqrt(hist['loss_var']), fmt=ss, **pargs, **ebarargs)
            #
            #if all_args.norm == 'BN':
            ss1 = ':' + marker_s
            if len(marker_s) == 0:
                ss2 = 's'
            else:
                ss2 = marker_s
            pargs1 = pargs.copy()
            pargs1['label'] = None
            #pargs1['linewidth'] = 0
            # if 't_loss' in hist: #all_args.norm == 'BN' or (all_args.train_inference == 'sample' and (all_args.dropout > 0 or all_args.reg_BN_k is not None)) or True:
            if 't_loss' in hist and (all_args.norm == 'BN' or all_args.ssb or all_args.SConv):
                plt.plot(np.arange(len(hist['t_loss'])), hist['t_loss'], ss2, **dict(pargs1,label=pargs['label'] + 'Eval', linewidth=0.25), markevery = int(math.ceil(len(hist['t_loss'])/10)))
            if train_valid_loss:
                #plt.plot(hist['v_loss'], ss1, **pargs1)
                errorbar_fency(range(vloss_m.size), vloss_m, yerr=vloss_e, fmt=ss1, **pargs1, **ebarargs)
                #plt.semilogy(hist['v_loss'], ss1, **pargs1)
                title = 'Train / Validation Loss'
            else:
                title = 'Train Loss'
            cparams = ''
            for (k,v) in common_params.items():
                if 'lr_down' in k or 'noise_augment' in k or 'path' in k or 'reg_variant' in k or v is None:
                    continue
                cparams = cparams + ', ' + k + '=' + str(v) + '\n'
            cparams = cparams.replace('lr_down=const', 'const lr')
            cparams = cparams.replace('lr_down=sqrt', 'lr_down: $1/\sqrt{k}$')
            cparams = cparams.replace('lr_down=exp', 'lr_down: $0.96^k$')
            cparams = cparams.replace(', inference=sample', '')
            cparams = cparams.replace(', inference=AP1', '')
            if 'init' not in cparams and 'init' not in exp_name:
                cparams = ', init=None' + cparams
            if len(cparams)>0:
                cparams = ' (' + cparams[2:len(cparams)] + ')'
            #title = title + cparams
            if show_title:
                plt.title(title)
            plt.legend(loc=loc)
            if fullscreen:
                llegend()
            if not show_legend:
                ax = plt.gca()
                ax.legend_.remove()
            ax = plt.gca()
            ax.yaxis.grid(True)
            ax.set_yscale("log", nonposy='clip')
            #plt.xlabel('epochs')
            ax.xaxis.set_major_locator(MaxNLocator(integer=True))
            #plt.ylim(0.5, 2.5)
            # plt.ylim(ymin=1e-4, ymax=1.0)
            # ax.set_ylim([ax.get_ylim()[0], 1.0])

            #plt.xticks(np.arange(0, t_epochs+1, 1.0))
            #ax.xaxis.grid(True)
    
            #### accuracies
            
            plt.figure("Train Accuracy", figsize=figsize)
            try:
                plt.plot(hist['t_acc'], ss, **pargs, markevery = int(math.ceil(len(hist['t_acc'])/10)))
                #plt.plot(v_epoch, hist['t_acc'][v_epoch], 'og', label=None)
                plt.legend(loc=4)
                if fullscreen:
                    llegend()
                if not show_legend:
                    ax = plt.gca()
                    ax.legend_.remove()
                if show_title:
                    plt.title('Train Accuracy')
                ax = plt.gca()
                ax.yaxis.grid(True)
                plt.ylim(ymin=ymin, ymax=1.0)
                #plt.xlabel('epochs')
            except:
                pass
            
            plt.figure("Validation Accuracy", figsize=figsize)
            #plt.plot(hist['v_acc'], ss, **pargs)
            if all_args.get('seed') == 1:
                errorbar_fency(range(vacc_m.size), vacc_m, yerr=vacc_e, fmt=ss, **pargs, **ebarargs)
            else:
                errorbar_fency(range(vacc_m.size), vacc_m, yerr=vacc_e*0, fmt=ss, **pargs, **ebarargs)
            #plt.plot(v_epoch, hist['v_acc'][v_epoch], 'og', label=None)
            plt.legend(loc=0)
            if fullscreen:
                llegend()
            if not show_legend:
                ax = plt.gca()
                ax.legend_.remove()
            if show_title:
                plt.title('Validation Accuracy')
            ax = plt.gca()
            #plt.ylim(0.7,1.0)
            ax.yaxis.grid(True)
            #plt.xlabel('epochs')
            #
            
        # test accuracy
        res_file = dir_name + '/' + variant + '-res/' + 'eval.pkl'
        if os.path.exists(res_file):
            eval = pickle.load(open(res_file, "rb"))
            plt.figure('Error Rate', figsize=figsize)
            #ax = plt.gca()
            b = plt.bar(bar_idx, (1-eval.acc.mean)*100, label = pargs['label'])


            def autolabel(rects):
                """
                Attach a text label above each bar displaying its height
                """
                for rect in rects:
                    height = rect.get_height()
                    plt.gca().text(rect.get_x() + rect.get_width() / 2., height, '%3.2f' % height, ha='center', va='bottom')
            autolabel(b)
            bar_idx += 1
            #ax.legend()
            
        
        
        for t in range(2):
            if t == 0:
                records = stab_records
                res_file = dir_name + '/' + variant + '-res/' + 'stab.npz'
            else:
                records = stab_records1
                res_file = dir_name + '/' + variant + '-res/' + 'stab_adv1a.npz'
                # res_file = dir_name + '/stab.npz'
            if os.path.exists(res_file):
                stab = np.load(res_file)
                D = stab['stab']
                X = D[:, 0]
                for r in range(len(records)):
                    plt.figure(records[r], figsize=figsize)
                    Y = D[:, r + 1]
                    pargsc = pargs.copy()
                    if r == 0:  # acc
                        pargsc['label'] += ' ({:4.3f})'.format(Y[0])  # at zero noise
                        acc0 = Y[0]
                    if r == 2:  # log odds
                        Y = np.exp(Y)
                        plt.semilogy(X, Y, ss, **pargsc)
                    else:
                        plt.plot(X, Y, ss, **pargsc)
                    plt.legend(loc=3)
                    if fullscreen:
                        llegend()
                    plt.title(records[r])
                    plt.xlabel('noise std')
                    
                    # plt.figure("stability", figsize=figsize)
                    # if stab is not None:
                    #     plt.plot(stab[:, 1], stab[:, 0], ss, label=exp_name)
                    #     plt.legend(loc=3)
                    #     plt.title('Stability')
        
        res_file = dir_name + '/' + variant + 'stab_adv1.npz'
        if os.path.exists(res_file):
            stab = np.load(res_file)
            stab = stab['stab']
            # print(stab.shape)
            dist = np.sqrt(stab[0, :])
            # patch the bug, none was computed as not adverarial
            # dist[dist == 1.0] = 0
            odds = stab[1, :]
            
            # reorder by increasing distance
            ind = np.argsort(dist)
            dist = dist[ind]
            odds = odds[ind]
            #acc = 1 - np.arange(len(dist)) / len(dist)
            acc = acc0 - np.arange(len(dist)) / len(dist)
            
            lo = np.where(dist > 0)[0][0]
            up = int(len(dist) * (1 - 0.2))
            
            #lo = max(0, lo-1)
            #print(lo)
            #lo = 0
            
            dist = dist[lo:up]
            acc = acc[lo:up]
            odds = odds[lo:up]
            
            dist = [0] + list(dist)
            acc = [acc0] + list(acc)
            
            plt.figure("Accuracy vs Perturbation", figsize=figsize)
            plt.plot(dist, acc, ss, **pargs)
            plt.legend(loc=loc)
            if fullscreen:
                llegend()
            if show_title:
                plt.title("Accuracy vs Adversarial Attack")
            plt.xlabel('perturbation norm')
            #plt.ylabel('accuracy')
            
            # plt.figure("Odds vs Perturbation", figsize=figsize)
            # plt.semilogy(dist, np.exp(odds), ss, **pargs)
            # plt.legend(loc=2)
            # plt.title("Odds vs Perturbation")
            # plt.xlabel('distance')
            # plt.ylabel('odds')
    
    # plt.show()
    # save all plots
    plt_path = root_dir + '/plots/'
    if len(select_exp)>0:
        suffix = ''.join('--' + str(k) + '=' + str(v) for k,v in sorted(select_exp.items()))
    else:
        suffix=''
    
    plt.figure("Validation Loss")
    try:
        plt.ylim(ymin=lymin, ymax=lymax)
    except NameError:
        pass
    save_pdf(plt_path + 'v_loss' + suffix + '.pdf')
    
    plt.figure("Train Loss")
    # plt.ylim(ymax=1.0)
    save_pdf(plt_path + 't_loss' + suffix + '.pdf')
    
    plt.figure("Train Accuracy")
    plt.ylim(ymin=ymin)
    save_pdf(plt_path + 't_acc' + suffix + '.pdf')
    
    plt.figure("Validation Accuracy")
    # plt.ylim(ymin=0.8, ymax = 0.92)
    plt.ylim(ymin = ymin, ymax = ymax)
    save_pdf(plt_path + 'v_acc' + suffix + '.pdf')
    
    # Test Accuracy
    
    plt.figure('Error Rate', figsize=figsize)
    plt.legend(loc=1)
    if fullscreen:
        llegend()
    save_pdf(plt_path + 'test_acc' + suffix + '.pdf')
    
    
    plt_path = root_dir + '/plots/' + variant
    # stability plots
    if stability:
        plt.figure(stab_records[0])
        plt.ylabel('Accuray')
        save_pdf(plt_path + 'stability_acc' + suffix + '.pdf')
        
        plt.figure(stab_records[1])
        plt.ylabel('Loss')
        plt.legend(loc=2)
        save_pdf(plt_path + 'stability_loss' + suffix + '.pdf')
        
        plt.figure(stab_records[2])
        plt.ylabel('Odds')
        plt.legend(loc=2)
        save_pdf(plt_path + 'stability_lodds' + suffix + '.pdf')
        
        #plt.figure("Accuracy vs Perturbation")
        #save_pdf(plt_path + 'stability1_acc' + suffix + '.pdf')
        
        #plt.figure("Odds vs Perturbation")
        #save_pdf(plt_path + 'stability1_odds' + suffix + '.pdf')
    
    # my GradientSignAttack test
    
    if stability:
        plt.figure(stab_records1[0])
        plt.ylabel('Accuray')
        plt.xlabel('perturbation norm')
        save_pdf(plt_path + 'stability1a_acc' + suffix + '.pdf')
        
        plt.figure(stab_records1[1])
        plt.ylabel('Loss')
        plt.xlabel('perturbation norm')
        plt.legend(loc=2)
        save_pdf(plt_path + 'stability1a_loss' + suffix + '.pdf')
        
        plt.figure(stab_records1[2])
        plt.ylabel('Odds')
        plt.xlabel('perturbation norm')
        plt.legend(loc=2)
        save_pdf(plt_path + 'stability1a_lodds' + suffix + '.pdf')


