import os
import sys
import torch
import torch.nn as nn
import torch.optim as optim
import vnn
from vnn.training import *
import argparse
from torchvision import datasets, transforms
import numpy as np
import math
import time
from collections import OrderedDict
import inspect
import os
from vnn.modelzoo import *

torch.nn.Module.dump_patches = True

training_dir = '../neuro-var-data/training/'
# training_dir = '../../neuro-var-data/training/'

# list of config options and their default values
args = odict()
args.code_version_major = 0
args.code_version_minor = 1
args.code_version_patch = 1
args.batch_size = 128
args.init_batch_size = 128
# args.eval_batch_size = 128
args.valid_size = 0.1
args.optimizer = 'Adam' #'SGD'
args.SGD_Nesterov = False
args.SGD_momentum = 0
args.epochs = 100
args.num_workers = 0 # means _extra_ workers
args.weight_decay = 0
args.seed = 1
args.val_seed = 1
args.cuda = torch.cuda.is_available()
# args.dataset_path = '../neuro-var-data/data/'
args.dataset_path = training_dir + '../data/'
args.training_dir = training_dir
args.path = None # path to save training results
args.softmax = 'simplified'
args.reg = None # None, 'sample'
args.weight_init = 'uniform' # 'orthogonal'
args.init = None  # BN, AP2
args.norm = None  # BN, AP2, Weight
args.noise_augment = 0.0
args.anoise_var = 0.0
args.input_var = 0.0
args.project = False
args.inference = 'AP1' # AP1, AP2
args.train_inference = 'AP1' # AP1, AP2
args.verbose = True
args.lr = None
args.lr_estimate_epochs = 5 # In case lr is None, will try to optimize lr for best performance in 5 epochs
args.reg_c = 1.0
args.dropout = 0.0
args.dropout_noise = 'bernoulli' # 'normal'
args.lr_down = 'exp'
args.lr_exp_base = 0.96 # = factor 0.1 in 50 epochs and factor 0.01 in 100 epochs
args.reg_BN_k = None
args.reg_BN_v = None
args.reg_calibrate = None # 'v_monotone', 'total_grad'
args.c_batch_noise = False
args.BN_v = None
args.reg_v = None
args.gain = None
args.str = None
args.ssb = False
args.initial_VB_factor = 32
args.target_epochs = 1200
args.prior_schedule = False
args.train_anew = False
# args.reg_lr = 10

default_args = args

torch.manual_seed(1)