from config import *

model_class = LeNet
args.path = training_dir + model_class.__name__ + '/n1/'
#### varied arguments ####
var_args = odict()
#####
var_args.noise_augment = 0.0
#### datasets config ####
transform = transforms.Compose([transforms.RandomCrop(28, 4), transforms.ToTensor(), AddNoiseTransform(var_args.noise_augment)])
train_dataset = datasets.MNIST(args.dataset_path, train=True, download=True, transform=transform)
test_dataset = datasets.MNIST(args.dataset_path, train=False, transform=transform)
######################################


#var_args.lr_exp_base = 0.97723  # decrease by 0.1 every 100 epochs
#var_args.epochs = 300
var_args.dropout = 0.0

var_args.lr_exp_base = 0.99616
var_args.epochs = 1200
var_args.train_inference = 'sample'
var_args.inference = 'AP1'
var_args.optimizer = 'SGD'
var_args.SGD_Nesterov = True
var_args.SGD_momentum = 0.9

#####
var_args.lr = None
var_args.init = 'AP2'
var_args.norm = 'AP2'
var_args.train_inference = 'sample'
var_args.inference = 'AP1'
var_args.batch_size = 32
var_args.project = True

# var_args.reg = 'KL'
# var_args.reg_c = 1
# var_args.stochastic_S = True
# var_args.ssb = 'SSB1'
# var_args.ScaleNormal = True


var_args.name = 'VN project'
#####

run_configs(model_class, train_dataset, test_dataset, args, var_args)